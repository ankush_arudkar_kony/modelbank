define("frmPFM", function() {
    return function(controller) {
        function addWidgetsfrmPFM() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0ec38ce9abb954e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": false,
                "onTouchEnd": controller.AS_Label_aac15a892b324686a236d7c21014295c,
                "right": "15dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0c7d42c5de9504c = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "CopyImage0c7d42c5de9504c",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_ic37779fba0b40c2a3f0daaece6ddfd9,
                "right": "15dp",
                "skin": "slImage",
                "src": "slider.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(Image0fe32af4553f34e, Label0dd464373550946, CopyImage0c7d42c5de9504c);
            var flxHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHead",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "62dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHead.setDefaultUnit(kony.flex.DP);
            var FlexGroup0d14a00d53c364f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "FlexGroup0d14a00d53c364f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e3746eec21724540b52bcd43c9eaeccf,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.33%"
            }, {}, {});
            FlexGroup0d14a00d53c364f.setDefaultUnit(kony.flex.DP);
            var imgBack = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "imgBack",
                "isVisible": true,
                "left": "15dp",
                "onTouchEnd": controller.AS_Image_he776d3827264b45b603ab9d0438c618,
                "skin": "slImage",
                "src": "back.png",
                "top": "1dp",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0d14a00d53c364f.add(imgBack);
            var CopyimgBack0d0166c8b606543 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "CopyimgBack0d0166c8b606543",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "slider.png",
                "top": "1dp",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHead.add(FlexGroup0d14a00d53c364f, CopyimgBack0d0166c8b606543);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Personal Finance",
                "textStyle": {},
                "top": "45dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxMonth = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxMonth",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "12dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMonth.setDefaultUnit(kony.flex.DP);
            var lblMonth = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblMonth",
                "isVisible": true,
                "left": "143dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "September",
                "textStyle": {},
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgPrevious = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "imgPrevious",
                "isVisible": true,
                "left": "95dp",
                "skin": "slImage",
                "src": "back.png",
                "top": "0dp",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgNext = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "imgNext",
                "isVisible": true,
                "right": "95dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "top": "0dp",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMonth.add(lblMonth, imgPrevious, imgNext);
            var donutchart = new com.konymp.donutchart({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "34%",
                "id": "donutchart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "140dp",
                "width": "100%",
                "overrides": {
                    "donutchart": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart.enableLegend = false;
            donutchart.bgColor = "#ffffff";
            donutchart.chartData = {
                "data": [{
                    "colorCode": "#2490c3",
                    "label": "Data1",
                    "value": "27"
                }, {
                    "colorCode": "#3bc016",
                    "label": "Data2",
                    "value": "20"
                }, {
                    "colorCode": "#ec1b7a",
                    "label": "Data3",
                    "value": "15"
                }, {
                    "colorCode": "#f48634",
                    "label": "Data4",
                    "value": "9"
                }, {
                    "colorCode": "#979797",
                    "label": "Data5",
                    "value": "6"
                }, {
                    "colorCode": "#ffffff",
                    "label": "Data6",
                    "value": "24"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart.titleFontSize = "12";
            donutchart.enableChartAnimation = true;
            donutchart.chartTitle = "Donut Chart";
            donutchart.legendFontSize = "8";
            donutchart.enableStaticPreview = true;
            donutchart.titleFontColor = "#ffffff";
            donutchart.legendFontColor = "#000000";
            var flxOverlay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35%",
                "id": "flxOverlay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "140dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOverlay.setDefaultUnit(kony.flex.DP);
            flxOverlay.add();
            var flxCatogories = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "275dp",
                "horizontalScrollIndicator": true,
                "id": "flxCatogories",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "420dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxCatogories.setDefaultUnit(kony.flex.DP);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_hd33cc0bb0274e6983364a1cc6c71890,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var flxSubAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSubAccounts.setDefaultUnit(kony.flex.DP);
            var imgArrow = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgArrow",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_01.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmount",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Food & Dining ",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmountValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountValue",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$762.12",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxSubAccounts.add(imgArrow, lblAmount, lblAmountValue, flxLine);
            flxAmount.add(flxSubAccounts);
            var flxEntertainment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxEntertainment",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEntertainment.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0f2169b9792e540 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0f2169b9792e540",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0f2169b9792e540.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0c505545b535d44 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0c505545b535d44",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_02.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0cfd971249b9c41 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0cfd971249b9c41",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Entertainment",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0e10c7bcf26944c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0e10c7bcf26944c",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$592.31",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0f2275f0299294e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0f2275f0299294e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0f2275f0299294e.setDefaultUnit(kony.flex.DP);
            CopyflxLine0f2275f0299294e.add();
            CopyflxSubAccounts0f2169b9792e540.add(CopyimgArrow0c505545b535d44, CopylblAmount0cfd971249b9c41, CopylblAmountValue0e10c7bcf26944c, CopyflxLine0f2275f0299294e);
            flxEntertainment.add(CopyflxSubAccounts0f2169b9792e540);
            var flxTravel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxTravel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTravel.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0b6bca624499441 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0b6bca624499441",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0b6bca624499441.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0g48870d229a74f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0g48870d229a74f",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_03.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0f874e2a5965e42 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0f874e2a5965e42",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Travel",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0e9e727903c6d4b = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0e9e727903c6d4b",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$323.75",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0fd23d017616c43 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0fd23d017616c43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0fd23d017616c43.setDefaultUnit(kony.flex.DP);
            CopyflxLine0fd23d017616c43.add();
            CopyflxSubAccounts0b6bca624499441.add(CopyimgArrow0g48870d229a74f, CopylblAmount0f874e2a5965e42, CopylblAmountValue0e9e727903c6d4b, CopyflxLine0fd23d017616c43);
            flxTravel.add(CopyflxSubAccounts0b6bca624499441);
            var flxFinancial = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxFinancial",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFinancial.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0c54df2013aca4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0c54df2013aca4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0c54df2013aca4a.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0a6d4d9c87efd40 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0a6d4d9c87efd40",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_04.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0dc93b215c4ea4f = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0dc93b215c4ea4f",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Financial",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0fff39f64c6d84f = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0fff39f64c6d84f",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$102.02",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0gb53148b185b4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0gb53148b185b4f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0gb53148b185b4f.setDefaultUnit(kony.flex.DP);
            CopyflxLine0gb53148b185b4f.add();
            CopyflxSubAccounts0c54df2013aca4a.add(CopyimgArrow0a6d4d9c87efd40, CopylblAmount0dc93b215c4ea4f, CopylblAmountValue0fff39f64c6d84f, CopyflxLine0gb53148b185b4f);
            flxFinancial.add(CopyflxSubAccounts0c54df2013aca4a);
            var flxEtc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxEtc",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEtc.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0abf866b2526c4b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0abf866b2526c4b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0abf866b2526c4b.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0ia2d62ab93e642 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0ia2d62ab93e642",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_05.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0bba5ecdcf15b43 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0bba5ecdcf15b43",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Etc",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0ff0d7b957bfb45 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0ff0d7b957bfb45",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$49.59",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0gdd3d0b73cf14c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0gdd3d0b73cf14c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0gdd3d0b73cf14c.setDefaultUnit(kony.flex.DP);
            CopyflxLine0gdd3d0b73cf14c.add();
            CopyflxSubAccounts0abf866b2526c4b.add(CopyimgArrow0ia2d62ab93e642, CopylblAmount0bba5ecdcf15b43, CopylblAmountValue0ff0d7b957bfb45, CopyflxLine0gdd3d0b73cf14c);
            flxEtc.add(CopyflxSubAccounts0abf866b2526c4b);
            flxCatogories.add(flxAmount, flxEntertainment, flxTravel, flxFinancial, flxEtc);
            var donutFood = new com.konymp.donutchart({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "34%",
                "id": "donutFood",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "140dp",
                "width": "100%",
                "overrides": {
                    "donutchart": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutFood.enableLegend = false;
            donutFood.bgColor = "#ffffff";
            donutFood.chartData = {
                "data": [{
                    "colorCode": "#2490c3",
                    "label": "Data1",
                    "value": "35"
                }, {
                    "colorCode": "#1b6d94",
                    "label": "Data2",
                    "value": "25"
                }, {
                    "colorCode": "#124761",
                    "label": "Data3",
                    "value": "10"
                }, {
                    "colorCode": "#092533",
                    "label": "Data4",
                    "value": "5"
                }, {
                    "colorCode": "#ffffff",
                    "label": "Data6",
                    "value": "24"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutFood.titleFontSize = "12";
            donutFood.enableChartAnimation = true;
            donutFood.chartTitle = "Donut Chart";
            donutFood.legendFontSize = "8";
            donutFood.enableStaticPreview = true;
            donutFood.titleFontColor = "#ffffff";
            donutFood.legendFontColor = "#000000";
            var flxFoodandDining = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFoodandDining",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "420dp",
                "width": "100%"
            }, {}, {});
            flxFoodandDining.setDefaultUnit(kony.flex.DP);
            var flxGeneralFood = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxGeneralFood",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGeneralFood.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0d7673896123248 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0d7673896123248",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0d7673896123248.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0c8a42d73e14d4f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0c8a42d73e14d4f",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_01.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0f6e0c95d51e44a = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0f6e0c95d51e44a",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "General Food & Dining",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0hd12434ce86d4e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0hd12434ce86d4e",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$762.12",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0db64a5a7535a49 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0db64a5a7535a49",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0db64a5a7535a49.setDefaultUnit(kony.flex.DP);
            CopyflxLine0db64a5a7535a49.add();
            CopyflxSubAccounts0d7673896123248.add(CopyimgArrow0c8a42d73e14d4f, CopylblAmount0f6e0c95d51e44a, CopylblAmountValue0hd12434ce86d4e, CopyflxLine0db64a5a7535a49);
            flxGeneralFood.add(CopyflxSubAccounts0d7673896123248);
            var flxRestuarant = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxRestuarant",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRestuarant.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0g95a466c35224f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0g95a466c35224f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0g95a466c35224f.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0i74b5a296bc142 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0i74b5a296bc142",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_02.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0h8d73b05ce824e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0h8d73b05ce824e",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Restuarant",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0ba85da76a72042 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0ba85da76a72042",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$592.31",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0ffd5660df0864d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0ffd5660df0864d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0ffd5660df0864d.setDefaultUnit(kony.flex.DP);
            CopyflxLine0ffd5660df0864d.add();
            CopyflxSubAccounts0g95a466c35224f.add(CopyimgArrow0i74b5a296bc142, CopylblAmount0h8d73b05ce824e, CopylblAmountValue0ba85da76a72042, CopyflxLine0ffd5660df0864d);
            flxRestuarant.add(CopyflxSubAccounts0g95a466c35224f);
            var flxFastfood = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxFastfood",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFastfood.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0gdd9aaac6b9d42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0gdd9aaac6b9d42",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0gdd9aaac6b9d42.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0d225918ece1d45 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0d225918ece1d45",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_03.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0b015604b56a54a = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0b015604b56a54a",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Fast food",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0e2e8d0461d534f = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0e2e8d0461d534f",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$323.75",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0f774bbda57ee42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0f774bbda57ee42",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0f774bbda57ee42.setDefaultUnit(kony.flex.DP);
            CopyflxLine0f774bbda57ee42.add();
            CopyflxSubAccounts0gdd9aaac6b9d42.add(CopyimgArrow0d225918ece1d45, CopylblAmount0b015604b56a54a, CopylblAmountValue0e2e8d0461d534f, CopyflxLine0f774bbda57ee42);
            flxFastfood.add(CopyflxSubAccounts0gdd9aaac6b9d42);
            var flxBars = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxBars",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBars.setDefaultUnit(kony.flex.DP);
            var CopyflxSubAccounts0c9507d0696004f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "CopyflxSubAccounts0c9507d0696004f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0c9507d0696004f.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0fbe7a9da989d4e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgArrow0fbe7a9da989d4e",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "cir_05.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0cbcf31a2bbd342 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0cbcf31a2bbd342",
                "isVisible": true,
                "left": "50dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Alcohal & Bars",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0ac622aa1c9084c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0ac622aa1c9084c",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$102.02",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0h42058cd27c442 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0h42058cd27c442",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0h42058cd27c442.setDefaultUnit(kony.flex.DP);
            CopyflxLine0h42058cd27c442.add();
            CopyflxSubAccounts0c9507d0696004f.add(CopyimgArrow0fbe7a9da989d4e, CopylblAmount0cbcf31a2bbd342, CopylblAmountValue0ac622aa1c9084c, CopyflxLine0h42058cd27c442);
            flxBars.add(CopyflxSubAccounts0c9507d0696004f);
            flxFoodandDining.add(flxGeneralFood, flxRestuarant, flxFastfood, flxBars);
            var lblTotalSpending = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTotalSpending",
                "isVisible": true,
                "left": "145dp",
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "$1,829.79",
                "textStyle": {},
                "top": "212dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSpendingText = new kony.ui.Label({
                "centerX": "50%",
                "height": "20dp",
                "id": "lblSpendingText",
                "isVisible": true,
                "left": "143dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Total Spending in all categories",
                "textStyle": {},
                "top": "260dp",
                "width": "58%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxMain.add(flxHeader, flxHead, lblPersonalChecking, flxMonth, donutchart, flxOverlay, flxCatogories, donutFood, flxFoodandDining, lblTotalSpending, lblSpendingText);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmPFM,
            "enabledForIdleTimeout": false,
            "id": "frmPFM",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_gbe35e2bd762414caccef49871abb8c7(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});