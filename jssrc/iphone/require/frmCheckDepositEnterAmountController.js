define("userfrmCheckDepositEnterAmountController", {
    frm_preshow: function() {
        this.view.txtAmount.text = "";
        // alert(value.lblAccountName);
        //alert(gblSelectedFromAccountRow1.lblAccountName);
        //this.view.lblBalance.text=gblSelectedFromAccountRow1;
        // this.view.btnContinue.skin="sknBtnDisabled";
        // this.view.btnContinue.hoverSkin="sknBtnBlueN";
    },
    onEdit_fun: function() {
        this.view.btnContinue.skin = "sknBtnBlueN";
        this.view.btnContinue.focusSkin = "sknBtnFocus";
    },
    onClickKey: function(selectedValue) {
        var tempVal = null;
        tempVal = this.view.txtAmount.text;
        if (selectedValue == -1) {
            if (tempVal != "" && tempVal != null && tempVal != undefined) {
                if (tempVal == "$") {
                    this.view.txtAmount.text = "";
                    this.view.bnContinue.skin = "sknBtnDisabled";
                    return;
                }
                tempVal = tempVal.substring(0, tempVal.length - 1);
                this.view.txtAmount.text = tempVal.replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            return;
        } else if (selectedValue != -1) {
            if (tempVal != "" && tempVal != null && tempVal != undefined) {
                this.view.txtAmount.text = (tempVal + selectedValue).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            } else {
                this.view.txtAmount.text = "$" + selectedValue;
            }
            this.view.bnContinue.skin = "sknBtnBlueN";
            this.view.bnContinue.focusSkin = "sknBtnFocus";
        }
        this.view.bnContinue.setEnabled(true);
    },
    //Type your controller code here 
});
define("frmCheckDepositEnterAmountControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_a23533df9a354069a6b6ac00de05fdaf: function AS_Image_a23533df9a354069a6b6ac00de05fdaf(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_be805fb10fb84d28baafc6e75b4ebbf0: function AS_Label_be805fb10fb84d28baafc6e75b4ebbf0(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for bnContinue **/
    AS_Button_d855b62002f84a4ebc663d5e46d67b6d: function AS_Button_d855b62002f84a4ebc663d5e46d67b6d(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCapture");
        ntf.navigate();
        amount = this.view.txtAmount.text;
        //alert(amount);
    },
    /** onTouchStart defined for lbl1 **/
    AS_Label_b194076a19694b4a81a689473d3fb881: function AS_Label_b194076a19694b4a81a689473d3fb881(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 1);
    },
    /** onTouchStart defined for lbl2 **/
    AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c: function AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 2);
    },
    /** onTouchStart defined for lbl3 **/
    AS_Label_cd4bf4991d604d5c9235a4f2a0073419: function AS_Label_cd4bf4991d604d5c9235a4f2a0073419(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 3);
    },
    /** onTouchStart defined for lbl4 **/
    AS_Label_a8313943b7994b55bb104e23d0bea023: function AS_Label_a8313943b7994b55bb104e23d0bea023(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 4);
    },
    /** onTouchStart defined for lbl5 **/
    AS_Label_c1f47c8e66d242fc893d55c068c44a3e: function AS_Label_c1f47c8e66d242fc893d55c068c44a3e(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 5);
    },
    /** onTouchStart defined for lbl6 **/
    AS_Label_e7abd80904f64838a05de2d67f2692e8: function AS_Label_e7abd80904f64838a05de2d67f2692e8(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 6);
    },
    /** onTouchStart defined for lbl7 **/
    AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b: function AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 7);
    },
    /** onTouchStart defined for lbl8 **/
    AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6: function AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 8);
    },
    /** onTouchStart defined for lbl9 **/
    AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29: function AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 9);
    },
    /** onTouchStart defined for lbl0 **/
    AS_Label_ef5e880ef423498293b0652c5cb97192: function AS_Label_ef5e880ef423498293b0652c5cb97192(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 0);
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_bb00780057c2476db51097bfd8c94496: function AS_FlexContainer_bb00780057c2476db51097bfd8c94496(eventobject) {
        var self = this;
        return self.onClickKey.call(this, -1);
    },
    /** onTextChange defined for txtAmount **/
    AS_TextField_f3544fa501154bf799c7f3cd22dcfffe: function AS_TextField_f3544fa501154bf799c7f3cd22dcfffe(eventobject, changedtext) {
        var self = this;
        this.onEdit_fun();
        this.view.bnContinue.setEnabled(true);
    },
    /** onClick defined for flxAmount **/
    AS_FlexContainer_g913eb538f2942c299eec011b84c1cf4: function AS_FlexContainer_g913eb538f2942c299eec011b84c1cf4(eventobject) {
        var self = this;
        ///
    },
    /** preShow defined for frmCheckDepositEnterAmount **/
    AS_Form_dea6023c7a634ae896be28d08d5ff5b4: function AS_Form_dea6023c7a634ae896be28d08d5ff5b4(eventobject) {
        var self = this;
        this.view.lblBalance.text = gblSelectedFromAccountRow1.lblAccountName;
        this.view.lblAvailableBalance.text = "Available Balance: " + gblSelectedFromAccountRow1.lblBalance;
        this.view.bnContinue.setEnabled(false);
    }
});
define("frmCheckDepositEnterAmountController", ["userfrmCheckDepositEnterAmountController", "frmCheckDepositEnterAmountControllerActions"], function() {
    var controller = require("userfrmCheckDepositEnterAmountController");
    var controllerActions = ["frmCheckDepositEnterAmountControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
