define("frmTransferSuccess", function() {
    return function(controller) {
        function addWidgetsfrmTransferSuccess() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c846579cfa2b44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "FlexContainer0c846579cfa2b44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c846579cfa2b44.setDefaultUnit(kony.flex.DP);
            var Image0f7270bbd061d44 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "75dp",
                "id": "Image0f7270bbd061d44",
                "isVisible": true,
                "left": "133dp",
                "skin": "slImage",
                "src": "ok_03.png",
                "top": "20dp",
                "width": "75dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c846579cfa2b44.add(Image0f7270bbd061d44);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "We successfully scheduled your transfer",
                "textStyle": {},
                "top": "110dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxReferenceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxReferenceId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxReferenceId.setDefaultUnit(kony.flex.DP);
            var CopyLabel0g369a6beee8c49 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0g369a6beee8c49",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Reference ID",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFrequency = new kony.ui.Label({
                "height": "100%",
                "id": "lblFrequency",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "878566910255",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxReferenceId.add(CopyLabel0g369a6beee8c49, lblFrequency);
            var flxFrom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxFrom.setDefaultUnit(kony.flex.DP);
            var CopyLabel0g0a5cb637d7d49 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0g0a5cb637d7d49",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "From",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSelectedFromAccount = new kony.ui.Label({
                "height": "100%",
                "id": "lblSelectedFromAccount",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "Personal Checking....x4567",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxFrom.add(CopyLabel0g0a5cb637d7d49, lblSelectedFromAccount);
            var flxTo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "300dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTo.setDefaultUnit(kony.flex.DP);
            var CopyLabel0a38fce130e2f49 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0a38fce130e2f49",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "To",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSelectedToAccount = new kony.ui.Label({
                "height": "100%",
                "id": "lblSelectedToAccount",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "Linda's Savings....x7878",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTo.add(CopyLabel0a38fce130e2f49, lblSelectedToAccount);
            var flxFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "350dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxFrequency.setDefaultUnit(kony.flex.DP);
            var CopyLabel0e5686939f38646 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0e5686939f38646",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Frequency",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblFrequency0j9844ad6fb3d43 = new kony.ui.Label({
                "height": "100%",
                "id": "CopylblFrequency0j9844ad6fb3d43",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "Once",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0h819f8b02ebf4e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0h819f8b02ebf4e",
                "isVisible": false,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFrequency.add(CopyLabel0e5686939f38646, CopylblFrequency0j9844ad6fb3d43, CopyImage0h819f8b02ebf4e);
            var flxTransferDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTransferDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "400dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTransferDate.setDefaultUnit(kony.flex.DP);
            var CopyLabel0e7778035d2a647 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0e7778035d2a647",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Transfer Date",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblTransferDate = new kony.ui.Label({
                "height": "100%",
                "id": "lblTransferDate",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "09/12/19",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTransferDate.add(CopyLabel0e7778035d2a647, lblTransferDate);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "450dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var CopyLabel0d6a792cf51764a = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0d6a792cf51764a",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Amount",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmount = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "$1500.00",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxAmount.add(CopyLabel0d6a792cf51764a, lblAmount);
            var bnNewTransfer = new kony.ui.Button({
                "bottom": "80dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnNewTransfer",
                "isVisible": true,
                "onClick": controller.AS_Button_dbe02e714f9747ee8837527466a14169,
                "skin": "sknBtnBlueN",
                "text": "New Transfer",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var bnMyAccounts = new kony.ui.Button({
                "bottom": "15dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnMyAccounts",
                "isVisible": true,
                "onClick": controller.AS_Button_f133a38ddb784adc8dc8f18da726afcb,
                "skin": "sknBtnBlueN",
                "text": "My Accounts",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMain.add(FlexContainer0c846579cfa2b44, lblPersonalChecking, flxReferenceId, flxFrom, flxTo, flxFrequency, flxTransferDate, flxAmount, bnNewTransfer, bnMyAccounts);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmTransferSuccess,
            "enabledForIdleTimeout": false,
            "id": "frmTransferSuccess",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a258a14efc794c00a6feddab2c8863fb(eventobject);
            },
            "skin": "CopysknFromBGWhite0f6fa4324c3aa45"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});