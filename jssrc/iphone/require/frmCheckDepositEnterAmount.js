define("frmCheckDepositEnterAmount", function() {
    return function(controller) {
        function addWidgetsfrmCheckDepositEnterAmount() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxSkinMainWhite",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "5%",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "15dp",
                "onTouchEnd": controller.AS_Image_a23533df9a354069a6b6ac00de05fdaf,
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "onTouchEnd": controller.AS_Label_be805fb10fb84d28baafc6e75b4ebbf0,
                "right": "15dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxHeader.add(Image0fe32af4553f34e, Label0dd464373550946);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "5%",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "7%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var lblPersonalChecking = new kony.ui.Label({
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "15dp",
                "skin": "skinhundred",
                "text": "Amount",
                "textStyle": {},
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblPersonalChecking0j74bf47189c841 = new kony.ui.Label({
                "id": "CopylblPersonalChecking0j74bf47189c841",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknLblBlack95N",
                "text": "Select where you would like to deposit your check",
                "textStyle": {},
                "top": "2dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "5dp",
                "width": "91%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAccountNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtBxTrans",
                "height": "100%",
                "id": "txtAccountNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "placeholder": "Search by account name or number",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTxtBxTrans",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtAccountNumber);
            flxWelcome.add(lblPersonalChecking, CopylblPersonalChecking0j74bf47189c841, flxSearch);
            var flxAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10%",
                "id": "flxAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccount.setDefaultUnit(kony.flex.DP);
            var flxAccountswithImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccountswithImg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAccountswithImg.setDefaultUnit(kony.flex.DP);
            var imgBankLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "imgBankLogo",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccountName = new kony.ui.Label({
                "id": "lblAccountName",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Deposit Account",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBalance = new kony.ui.Label({
                "id": "lblBalance",
                "isVisible": true,
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Personal Checking...4567",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBankName = new kony.ui.Label({
                "bottom": "15dp",
                "id": "lblBankName",
                "isVisible": false,
                "left": "53dp",
                "skin": "sknlblBankName",
                "text": "Label",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAvailableBalance = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblAvailableBalance",
                "isVisible": true,
                "right": "55dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Available Balance: $10,456.32 ",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbllIne = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "lbllIne",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxAccountswithImg.add(imgBankLogo, lblAccountName, lblBalance, lblBankName, lblAvailableBalance, lbllIne);
            flxAccount.add(flxAccountswithImg);
            var flxEnterAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxEnterAmount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEnterAmount.setDefaultUnit(kony.flex.DP);
            var tbxEnterAmount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "CopydefTextBoxFocus0c3c4213dfeb14c",
                "height": "40dp",
                "id": "tbxEnterAmount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "13dp",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0e2e3fc6dc92e45",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "5dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "defTextBoxPlaceholder",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblSeperator = new kony.ui.Label({
                "bottom": "4dp",
                "centerX": "50%",
                "height": "2dp",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "23dp",
                "skin": "CopydefLabel0j4005d8657b544",
                "textStyle": {},
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxEnterAmount.add(tbxEnterAmount, lblSeperator);
            var bnContinue = new kony.ui.Button({
                "bottom": "230dp",
                "centerX": "50%",
                "focusSkin": "sknBtnDisabled",
                "height": "50dp",
                "id": "bnContinue",
                "isVisible": true,
                "left": "15dp",
                "onClick": controller.AS_Button_d855b62002f84a4ebc663d5e46d67b6d,
                "skin": "sknBtnDisabled",
                "text": "Continue",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var flxKeyPad = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "200dp",
                "id": "flxKeyPad",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxKeyPad.setDefaultUnit(kony.flex.DP);
            var flx123 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx123",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx123.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl1",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_b194076a19694b4a81a689473d3fb881,
                "skin": "sknLblKey",
                "text": "1",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl2 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl2",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c,
                "skin": "sknLblKey",
                "text": "2",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl3 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl3",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_cd4bf4991d604d5c9235a4f2a0073419,
                "skin": "sknLblKey",
                "text": "3",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx123.add(lbl1, lbl2, lbl3);
            var flx456 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx456",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx456.setDefaultUnit(kony.flex.DP);
            var lbl4 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl4",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_a8313943b7994b55bb104e23d0bea023,
                "skin": "sknLblKey",
                "text": "4",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl5 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl5",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_c1f47c8e66d242fc893d55c068c44a3e,
                "skin": "sknLblKey",
                "text": "5",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl6 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl6",
                "isVisible": true,
                "onTouchStart": controller.AS_Label_e7abd80904f64838a05de2d67f2692e8,
                "right": "0dp",
                "skin": "sknLblKey",
                "text": "6",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx456.add(lbl4, lbl5, lbl6);
            var flx789 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx789",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx789.setDefaultUnit(kony.flex.DP);
            var lbl7 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl7",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b,
                "skin": "sknLblKey",
                "text": "7",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl8 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl8",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6,
                "skin": "sknLblKey",
                "text": "8",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl9 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl9",
                "isVisible": true,
                "left": "66%",
                "onTouchStart": controller.AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29,
                "skin": "sknLblKey",
                "text": "9",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx789.add(lbl7, lbl8, lbl9);
            var flxRemove = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flxRemove",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRemove.setDefaultUnit(kony.flex.DP);
            var lbl0 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl0",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_ef5e880ef423498293b0652c5cb97192,
                "skin": "sknLblKey",
                "text": "0",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_bb00780057c2476db51097bfd8c94496,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxDelete.setDefaultUnit(kony.flex.DP);
            var Image0abd38ce5079440 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "Image0abd38ce5079440",
                "isVisible": true,
                "right": "0dp",
                "skin": "slImage",
                "src": "delete_icn.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(Image0abd38ce5079440);
            flxRemove.add(lbl0, flxDelete);
            flxKeyPad.add(flx123, flx456, flx789, flxRemove);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "7%",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_g913eb538f2942c299eec011b84c1cf4,
                "skin": "slFbox",
                "top": "28%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            var txtAmount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "CopysknTxtBxTransBG0f853508411f14f",
                "height": "88%",
                "id": "txtAmount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "onTextChange": controller.AS_TextField_f3544fa501154bf799c7f3cd22dcfffe,
                "placeholder": "$0.00",
                "secureTextEntry": false,
                "skin": "sknTxtBxTransBG150Bold",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "CopysknTxtBxTransBG0f853508411f14f",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxAmount.add(flxLine, txtAmount);
            flxMain.add(flxHeader, flxWelcome, flxAccount, flxEnterAmount, bnContinue, flxKeyPad, flxAmount);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCheckDepositEnterAmount,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmCheckDepositEnterAmount",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_dea6023c7a634ae896be28d08d5ff5b4(eventobject);
            },
            "skin": "sknFrmBgWhitef8f8f8",
            "statusBarHidden": false
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});