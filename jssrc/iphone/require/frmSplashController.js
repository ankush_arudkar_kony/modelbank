define("userfrmSplashController", {
    //Type your controller code here 
    showSplash: function() {
        kony.timer.schedule("splashTimer", this.navToWelcome, 2, false);
    },
    navToWelcome: function() {
        navToRespForm("frmLogin");
    }
});
define("frmSplashControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** postShow defined for frmSplash **/
    AS_Form_dff408d135e24aafa6dc59ad27bd39b9: function AS_Form_dff408d135e24aafa6dc59ad27bd39b9(eventobject) {
        var self = this;
        return self.showSplash.call(this);
    }
});
define("frmSplashController", ["userfrmSplashController", "frmSplashControllerActions"], function() {
    var controller = require("userfrmSplashController");
    var controllerActions = ["frmSplashControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
