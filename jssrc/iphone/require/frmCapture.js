define("frmCapture", function() {
    return function(controller) {
        function addWidgetsfrmCapture() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlWHiteBG",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "15dp",
                "onTouchEnd": controller.AS_Image_d72570b7b47841ce8cc64ff3c44d71ed,
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "onTouchEnd": controller.AS_Label_d123d4adb2754026b6f0f04f7188b793,
                "right": "15dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxHeader.add(Image0fe32af4553f34e, Label0dd464373550946);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "7%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var lblPersonalChecking = new kony.ui.Label({
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "15dp",
                "skin": "Copyskinhundred0eb768ee2f3804f",
                "text": "Get ready to take pictures of your check",
                "textStyle": {},
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblPersonalChecking0j74bf47189c841 = new kony.ui.Label({
                "id": "CopylblPersonalChecking0j74bf47189c841",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknLblBlack95N",
                "text": "Select where you would like to deposit your check",
                "textStyle": {},
                "top": "2dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "5dp",
                "width": "91%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAccountNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtBxTrans",
                "height": "100%",
                "id": "txtAccountNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "placeholder": "Search by account name or number",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTxtBxTrans",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtAccountNumber);
            flxWelcome.add(lblPersonalChecking, CopylblPersonalChecking0j74bf47189c841, flxSearch);
            var FlexContainer0d3053611c83c4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "25%",
                "id": "FlexContainer0d3053611c83c4f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0d3053611c83c4f.setDefaultUnit(kony.flex.DP);
            var imgPicture = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgPicture",
                "isVisible": true,
                "left": "100dp",
                "skin": "slImage",
                "src": "rotate.png",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0d3053611c83c4f.add(imgPicture);
            var lblCamText = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblCamText",
                "isVisible": true,
                "skin": "CopysknLblBlack0a28e241776354c",
                "text": "We’ll take a picture for you. Or you can take a picture of your check manually. ",
                "textStyle": {},
                "top": "55%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var btnAutoCapture = new kony.ui.Button({
                "focusSkin": "sknBtnBlueN",
                "height": "50dp",
                "id": "btnAutoCapture",
                "isVisible": true,
                "left": "29dp",
                "onClick": controller.AS_Button_cdc5e444de5c421592359eab33402151,
                "skin": "sknBtnBlueN",
                "text": "Auto Capture",
                "top": "75%",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var btnMaualCapture = new kony.ui.Button({
                "focusSkin": "sknBtnBlueN",
                "height": "50dp",
                "id": "btnMaualCapture",
                "isVisible": true,
                "left": "29dp",
                "skin": "sknBtnBlueN",
                "text": "Manual Capture",
                "top": "85%",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var CamManual = new kony.ui.Camera({
                "centerX": "50%",
                "focusSkin": "slCamera",
                "height": "50dp",
                "id": "CamManual",
                "isVisible": true,
                "left": "36dp",
                "onCapture": controller.AS_Camera_j656ce0f116646a7a1c4c3b151099e85,
                "skin": "slCamera",
                "top": "85%",
                "width": "85%",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_PUBLIC,
                "enableOverlay": false,
                "nativeUserInterface": true
            });
            flxMain.add(flxHeader, flxWelcome, FlexContainer0d3053611c83c4f, lblCamText, btnAutoCapture, btnMaualCapture, CamManual);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCapture,
            "enabledForIdleTimeout": false,
            "id": "frmCapture",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});