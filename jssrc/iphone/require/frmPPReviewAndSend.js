define("frmPPReviewAndSend", function() {
    return function(controller) {
        function addWidgetsfrmPPReviewAndSend() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_hc0e0db770b84cb783534edf953877ac,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a9942efe15264f53b66dd0dfae6fefa8,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Review And Send",
                "textStyle": {},
                "top": "50dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmount = new kony.ui.Label({
                "id": "lblAmount",
                "isVisible": true,
                "left": "0dp",
                "skin": "skn150SemiBold",
                "text": "$ 100.00",
                "textStyle": {},
                "top": "120dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAccountId = new kony.ui.Label({
                "id": "lblAccountId",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl110SemiBold222222",
                "text": "Mobile: (123)4567890",
                "textStyle": {},
                "top": "200dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxNotes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxNotes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "300dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxNotes.setDefaultUnit(kony.flex.DP);
            var txtNotes = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtBxTrans",
                "height": "88%",
                "id": "txtNotes",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Note (Optional)",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTxtBxTrans",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var CopyflxLine0b0f8085572564d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0b0f8085572564d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0b0f8085572564d.setDefaultUnit(kony.flex.DP);
            CopyflxLine0b0f8085572564d.add();
            flxNotes.add(txtNotes, CopyflxLine0b0f8085572564d);
            var flxFrom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "500dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxFrom.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b8d6266a3bf24e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0b8d6266a3bf24e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "88%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0b8d6266a3bf24e.setDefaultUnit(kony.flex.DP);
            var Label0c180603619a54f = new kony.ui.Label({
                "height": "100%",
                "id": "Label0c180603619a54f",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "From",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSelectedFromAccount = new kony.ui.Label({
                "height": "100%",
                "id": "lblSelectedFromAccount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl110SemiBold222222",
                "text": "Personal Checking....x4567",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0b8d6266a3bf24e.add(Label0c180603619a54f, lblSelectedFromAccount);
            var lblAvailableBalance = new kony.ui.Label({
                "bottom": "0dp",
                "height": "50%",
                "id": "lblAvailableBalance",
                "isVisible": true,
                "right": "12%",
                "skin": "sknlblBankName",
                "text": "Available Balance: $10,456.32",
                "textStyle": {},
                "width": "88%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Image0bc3225b2ebb148 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "Image0bc3225b2ebb148",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFrom.add(FlexContainer0b8d6266a3bf24e, lblAvailableBalance, Image0bc3225b2ebb148);
            var bnTransfer = new kony.ui.Button({
                "bottom": "30dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnTransfer",
                "isVisible": true,
                "onClick": controller.AS_Button_df46860a9b594ae4b5954f1e526ed24f,
                "skin": "sknBtnBlueN",
                "text": "Send Now",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMain.add(FlexContainer0eed5fe5dc1064e, lblPersonalChecking, lblAmount, lblAccountId, flxNotes, flxFrom, bnTransfer);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmPPReviewAndSend,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmPPReviewAndSend",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_gd2c5d1d0cb44dc584b6c69596269f97(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});