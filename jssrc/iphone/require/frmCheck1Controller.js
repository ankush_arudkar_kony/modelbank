define("userfrmCheck1Controller", {
    //Type your controller code here 
});
define("frmCheck1ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_hb15f584386845baaf089ddd4c7644fd: function AS_Image_hb15f584386845baaf089ddd4c7644fd(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCaptureBack");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_f1f373386535488480067028459fe741: function AS_Label_f1f373386535488480067028459fe741(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    }
});
define("frmCheck1Controller", ["userfrmCheck1Controller", "frmCheck1ControllerActions"], function() {
    var controller = require("userfrmCheck1Controller");
    var controllerActions = ["frmCheck1ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
