define("frmCardManagement", function() {
    return function(controller) {
        function addWidgetsfrmCardManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var FlexGroup0i067cb445f824d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexGroup0i067cb445f824d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d49863f4337447",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            FlexGroup0i067cb445f824d.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back_1.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": false,
                "right": "0dp",
                "skin": "CopysknLblBlackSemiBold",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a69c2137f78c4f34876d08c313fbb2f2,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e8fcde6c659e47409bdbe20d054d3fd3,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopyCopysknlblAccountName1",
                "text": "Card Management",
                "textStyle": {},
                "top": 0,
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            kony.mvc.registry.add('FBox0fd02d7bdefc541', 'FBox0fd02d7bdefc541', 'FBox0fd02d7bdefc541Controller');
            var Segment0e5c91d7cd37345 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "Image0g5db86ebdafe4b": "creditcard_marron.png"
                }, {
                    "Image0g5db86ebdafe4b": "creditcard_gold.png"
                }, {
                    "Image0g5db86ebdafe4b": "creditcard_grey.png"
                }],
                "groupCells": false,
                "height": "240dp",
                "id": "Segment0e5c91d7cd37345",
                "isVisible": true,
                "left": "2dp",
                "needPageIndicator": false,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "FBox0fd02d7bdefc541",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "Image0g5db86ebdafe4b": "Image0g5db86ebdafe4b"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_ROW_SELECT,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            var flxoutstanfingBal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "77dp",
                "id": "flxoutstanfingBal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxoutstanfingBal.setDefaultUnit(kony.flex.DP);
            var CopyLabel0g369a6beee8c49 = new kony.ui.Label({
                "centerY": "25%",
                "height": "43.29%",
                "id": "CopyLabel0g369a6beee8c49",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLblGrey0g70b64115e9745",
                "text": "Outstanding Balance",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyLabel0cc32d36c675540 = new kony.ui.Label({
                "centerY": "25%",
                "height": "43.29%",
                "id": "CopyLabel0cc32d36c675540",
                "isVisible": true,
                "right": 0,
                "skin": "CopysknLblGrey0g70b64115e9745",
                "text": "Points",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyLabel0c2845a14a0554c = new kony.ui.Label({
                "centerY": "60%",
                "height": "43.29%",
                "id": "CopyLabel0c2845a14a0554c",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLblGrey0i5ff95d3c87e4c",
                "text": "$8,307.01",
                "textStyle": {},
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyLabel0b40b5a12c3a34e = new kony.ui.Label({
                "centerY": "60%",
                "height": "43.29%",
                "id": "CopyLabel0b40b5a12c3a34e",
                "isVisible": true,
                "right": 0,
                "skin": "CopysknLblGrey0gff405d6e8d142",
                "text": "6,785",
                "textStyle": {},
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFrequency = new kony.ui.Label({
                "height": "100%",
                "id": "lblFrequency",
                "isVisible": false,
                "right": "12%",
                "skin": "CopysknLbl",
                "text": "Once",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0h819f8b02ebf4e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0h819f8b02ebf4e",
                "isVisible": false,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow_1.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxoutstanfingBal.add(CopyLabel0g369a6beee8c49, CopyLabel0cc32d36c675540, CopyLabel0c2845a14a0554c, CopyLabel0b40b5a12c3a34e, lblFrequency, CopyImage0h819f8b02ebf4e);
            var CopyflxFrequency0jc944d376b1341 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "CopyflxFrequency0jc944d376b1341",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxFrequency0jc944d376b1341.setDefaultUnit(kony.flex.DP);
            var CopyLabel0de51174491e540 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0de51174491e540",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLblGrey0g70b64115e9745",
                "text": "Lock Card",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgRememberMe = new kony.ui.Image2({
                "centerY": "50%",
                "height": "70%",
                "id": "imgRememberMe",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_jf27a24de72f41818eb308b82ecab205,
                "right": "2%",
                "skin": "slImage",
                "src": "switchon_1.png",
                "width": "18.16%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxFrequency0jc944d376b1341.add(CopyLabel0de51174491e540, imgRememberMe);
            var flxCardActions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxCardActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCardActions.setDefaultUnit(kony.flex.DP);
            var Segment0a2ccc7321a7a47 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "data": [{
                    "imgAccountType": "icon_8_1.png",
                    "lblAccount": "Change Pin",
                    "lbllIne": ""
                }, {
                    "imgAccountType": "icon_14_1.png",
                    "lblAccount": "Replace Card",
                    "lbllIne": ""
                }, {
                    "imgAccountType": "icon_15_1.png",
                    "lblAccount": "Report Lost or Stolen Card",
                    "lbllIne": ""
                }, {
                    "imgAccountType": "icon_10_1.png",
                    "lblAccount": "Manage Travel Plans",
                    "lbllIne": ""
                }, {
                    "imgAccountType": "icon_6_1.png",
                    "lblAccount": "Cancel Card",
                    "lbllIne": ""
                }],
                "groupCells": false,
                "height": "250dp",
                "id": "Segment0a2ccc7321a7a47",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "CopyflxHdrImageText",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CopyflxHdrImageText": "CopyflxHdrImageText",
                    "imgAccountType": "imgAccountType",
                    "lblAccount": "lblAccount",
                    "lbllIne": "lbllIne"
                },
                "width": "93%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_NONE,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            flxCardActions.add(Segment0a2ccc7321a7a47);
            FlexGroup0i067cb445f824d.add(FlexContainer0eed5fe5dc1064e, lblPersonalChecking, Segment0e5c91d7cd37345, flxoutstanfingBal, CopyflxFrequency0jc944d376b1341, flxCardActions);
            this.add(FlexGroup0i067cb445f824d);
        };
        return [{
            "addWidgets": addWidgetsfrmCardManagement,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmCardManagement",
            "layoutType": kony.flex.FLOW_VERTICAL,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_i6d4a3dbe8fb4007be5ef377b789337c(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});