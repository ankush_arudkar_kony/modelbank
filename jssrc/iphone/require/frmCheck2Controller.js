define("userfrmCheck2Controller", {
    //Type your controller code here 
});
define("frmCheck2ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_j3b0c519e6e54410bee5541a69ac760f: function AS_Image_j3b0c519e6e54410bee5541a69ac760f(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmScaning");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_bc7e5cc46e10476bbb6f790d4961d5ca: function AS_Label_bc7e5cc46e10476bbb6f790d4961d5ca(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    }
});
define("frmCheck2Controller", ["userfrmCheck2Controller", "frmCheck2ControllerActions"], function() {
    var controller = require("userfrmCheck2Controller");
    var controllerActions = ["frmCheck2ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
