define("userfrmSendPPPaymentsController", {
    //Type your controller code here 
    onClickKey: function(selectedValue) {
        var tempVal = null;
        tempVal = this.view.txtAmount.text;
        if (selectedValue == -1) {
            if (tempVal != "" && tempVal != null && tempVal != undefined) {
                if (tempVal == "$") {
                    this.view.txtAmount.text = "";
                    this.view.bnContinue.skin = "sknBtnDisabled";
                    this.view.bnContinue.focusSkin = "sknBtnDisabled";
                    return;
                }
                tempVal = tempVal.substring(0, tempVal.length - 1);
                this.view.txtAmount.text = tempVal.replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            return;
        } else if (selectedValue != -1) {
            if (tempVal != "" && tempVal != null && tempVal != undefined) {
                this.view.txtAmount.text = (tempVal + selectedValue).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            } else {
                this.view.txtAmount.text = "$" + selectedValue;
            }
            this.view.bnContinue.skin = "sknBtnBlueN";
            this.view.bnContinue.focusSkin = "sknBtnFocus";
        }
    },
    preShowFun: function() {
        this.view.bnContinue.skin = "sknBtnDisabled";
        this.view.bnContinue.focusSkin = "sknBtnDisabled";
        this.view.txtSelectedPerson.setEnabled(false);
        this.view.txtSelectedPerson.text = gblSelectedPPID;
        this.view.txtAmount.text = "";
    }
});
define("frmSendPPPaymentsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for lbl1 **/
    AS_Label_b194076a19694b4a81a689473d3fb881: function AS_Label_b194076a19694b4a81a689473d3fb881(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 1);
    },
    /** onTouchStart defined for lbl2 **/
    AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c: function AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 2);
    },
    /** onTouchStart defined for lbl3 **/
    AS_Label_cd4bf4991d604d5c9235a4f2a0073419: function AS_Label_cd4bf4991d604d5c9235a4f2a0073419(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 3);
    },
    /** onTouchStart defined for lbl4 **/
    AS_Label_a8313943b7994b55bb104e23d0bea023: function AS_Label_a8313943b7994b55bb104e23d0bea023(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 4);
    },
    /** onTouchStart defined for lbl5 **/
    AS_Label_c1f47c8e66d242fc893d55c068c44a3e: function AS_Label_c1f47c8e66d242fc893d55c068c44a3e(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 5);
    },
    /** onTouchStart defined for lbl6 **/
    AS_Label_e7abd80904f64838a05de2d67f2692e8: function AS_Label_e7abd80904f64838a05de2d67f2692e8(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 6);
    },
    /** onTouchStart defined for lbl7 **/
    AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b: function AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 7);
    },
    /** onTouchStart defined for lbl8 **/
    AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6: function AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 8);
    },
    /** onTouchStart defined for lbl9 **/
    AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29: function AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 9);
    },
    /** onTouchStart defined for lbl0 **/
    AS_Label_ef5e880ef423498293b0652c5cb97192: function AS_Label_ef5e880ef423498293b0652c5cb97192(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 0);
    },
    /** onClick defined for flxBack **/
    AS_FlexContainer_g2d7e1a136e14465b5e800f7437ed98d: function AS_FlexContainer_g2d7e1a136e14465b5e800f7437ed98d(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferTo");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_b4d2d7410fda4212929c7b7c638bdf05: function AS_FlexContainer_b4d2d7410fda4212929c7b7c638bdf05(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_b69f5b5c9be445369c82fb226bde1807: function AS_FlexContainer_b69f5b5c9be445369c82fb226bde1807(eventobject) {
        var self = this;
        return self.onClickKey.call(this, -1);
    },
    /** onClick defined for bnContinue **/
    AS_Button_h896e47f96464e30ac5654b013415b35: function AS_Button_h896e47f96464e30ac5654b013415b35(eventobject) {
        var self = this;
        if (this.view.bnContinue.skin == "sknBtnBlueN") {
            gblEnteredPPAmt = this.view.txtAmount.text;
            gblEnteredPPId = this.view.txtSelectedPerson.text;
            navToRespForm("frmPPReviewAndSend");
        }
    },
    /** preShow defined for frmSendPPPayments **/
    AS_Form_b784026d058340bea91e753d72cafb39: function AS_Form_b784026d058340bea91e753d72cafb39(eventobject) {
        var self = this;
        return self.preShowFun.call(this);
    }
});
define("frmSendPPPaymentsController", ["userfrmSendPPPaymentsController", "frmSendPPPaymentsControllerActions"], function() {
    var controller = require("userfrmSendPPPaymentsController");
    var controllerActions = ["frmSendPPPaymentsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
