define("userfrmGetstartedController", {
    //Type your controller code here 
});
define("frmGetstartedControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnGetStarted **/
    AS_Button_fad1332a28744e3f8104a0a3f0744a70: function AS_Button_fad1332a28744e3f8104a0a3f0744a70(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmChooseAgent");
        ntf.navigate();
    },
    /** onClick defined for btnSwitch **/
    AS_Button_ca922ffd0b17419b8da049bcec578b7e: function AS_Button_ca922ffd0b17419b8da049bcec578b7e(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmGetStarted2");
        ntf.navigate();
    },
    /** onClick defined for btnBack **/
    AS_Button_acabfaddb33a41f5ad531b6acac64f3c: function AS_Button_acabfaddb33a41f5ad531b6acac64f3c(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** preShow defined for frmGetstarted **/
    AS_Form_b7c8fdff57fd45428a3bd09ecbcd67d8: function AS_Form_b7c8fdff57fd45428a3bd09ecbcd67d8(eventobject) {
        var self = this;
        this.view.segIntro.pageSkin = "sknSegffffff";
    }
});
define("frmGetstartedController", ["userfrmGetstartedController", "frmGetstartedControllerActions"], function() {
    var controller = require("userfrmGetstartedController");
    var controllerActions = ["frmGetstartedControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
