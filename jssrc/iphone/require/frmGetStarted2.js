define("frmGetStarted2", function() {
    return function(controller) {
        function addWidgetsfrmGetStarted2() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var segIntro = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgBG": "bgimage4.png",
                    "lbl1": "A trusted partner",
                    "lbl2": "A human, financial expert - right in your pocket. With you from lost cards to home loans",
                    "lblSwipe": "Swipe to learn more",
                    "upparImg": "engagelogo.png"
                }, {
                    "imgBG": "bgimage5.png",
                    "lbl1": "It's all talk",
                    "lbl2": "Have a question or financial issue? Just ask your agent! Help is there for you whenever and wherever you need it.",
                    "lblSwipe": "",
                    "upparImg": ""
                }, {
                    "imgBG": "bgimage6.png",
                    "lbl1": "Just for Fun",
                    "lbl2": "The perfect agent - with you in mind. For whatever life throws at you, getting help is easy and secure.",
                    "lblSwipe": "",
                    "upparImg": ""
                }],
                "groupCells": false,
                "height": "82%",
                "id": "segIntro",
                "isVisible": true,
                "left": "0%",
                "needPageIndicator": true,
                "pageOffDotImage": "dotn_1.png",
                "pageOnDotImage": "dotf_1.png",
                "retainSelection": false,
                "rowSkin": "CopysknSegffffff",
                "rowTemplate": "flxMain",
                "scrollingEvents": {},
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0%",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "flxBG": "flxBG",
                    "flxMain": "flxMain",
                    "flxUpparImg": "flxUpparImg",
                    "imgBG": "imgBG",
                    "lbl1": "lbl1",
                    "lbl2": "lbl2",
                    "lblSwipe": "lblSwipe",
                    "upparImg": "upparImg"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": false,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_ROW_SELECT,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            var btnGetStarted = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "7%",
                "id": "btnGetStarted",
                "isVisible": true,
                "left": "50dp",
                "onClick": controller.AS_Button_c46027f31c784d0dad7021c2f4e60264,
                "skin": "sknBtnBlue",
                "text": "Get Started",
                "top": "88%",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            var btnSwitch = new kony.ui.Button({
                "focusSkin": "sknBtnF",
                "height": "15%",
                "id": "btnSwitch",
                "isVisible": true,
                "left": "85%",
                "onClick": controller.AS_Button_b1c34031ba684c82a18fd3a1a8ae218a,
                "skin": "CopybtnTrans",
                "top": "0%",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "8%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var imgBack = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "imgBack",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "backarrow_black.png",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBack = new kony.ui.Button({
                "focusSkin": "CopysknBtnTrans1",
                "height": "100%",
                "id": "btnBack",
                "isVisible": true,
                "left": "0dp",
                "onClick": controller.AS_Button_h91674ca8bfd4bdea96f21a742e828a5,
                "skin": "CopysknBtnTrans1",
                "text": "Button",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": false,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            flxBack.add(imgBack, btnBack);
            flxMain.add(segIntro, btnGetStarted, btnSwitch, flxBack);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmGetStarted2,
            "enabledForIdleTimeout": false,
            "id": "frmGetStarted2",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ded0fd613abd46ac9782da7db5b023ff(eventobject);
            },
            "skin": "CopysknFrmBG"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});