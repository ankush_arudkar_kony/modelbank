define("frmChooseAgent", function() {
    return function(controller) {
        function addWidgetsfrmChooseAgent() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxBGwhite",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "5.99%",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3419645b41947",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var imgsearch = new kony.ui.Image2({
                "centerY": "50%",
                "height": "47%",
                "id": "imgsearch",
                "isVisible": true,
                "right": "3%",
                "src": "search1.png",
                "width": "6%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "48%",
                "id": "flxBack",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f011c709e617469c97ca5bf91705b02e,
                "skin": "slFbox",
                "top": "0dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var imgBack = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "imgBack",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "backarrow.png",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBack = new kony.ui.Button({
                "focusSkin": "sknBtnTrans",
                "height": "100%",
                "id": "btnBack",
                "isVisible": true,
                "left": "0dp",
                "onClick": controller.AS_Button_d31f114a762f4cd6af54a94822b192f3,
                "skin": "sknBtnTrans",
                "text": "Button",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": false,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            flxBack.add(imgBack, btnBack);
            flxSearch.add(imgsearch, flxBack);
            var flxChooseAgent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChooseAgent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChooseAgent.setDefaultUnit(kony.flex.DP);
            var lblChooseAgent = new kony.ui.Label({
                "centerY": "49%",
                "height": "100%",
                "id": "lblChooseAgent",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLblWhiteSBold",
                "text": "Choose your Agent",
                "textStyle": {},
                "top": "10dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxChooseAgent.add(lblChooseAgent);
            flxHeader.add(flxSearch, flxChooseAgent);
            var flxBelow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBelow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxBGwhite",
                "top": "64dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBelow.setDefaultUnit(kony.flex.DP);
            var segAgent = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "data": [{
                    "Copylbl0a1a0b464f64140": "Mon, Tue, Fri: 9am -5pm",
                    "btnGetStarted": "Choose Kim",
                    "imgProfile": "chooseyouagent.png",
                    "imgagent": "agent.png",
                    "lbl1": "Home Lending",
                    "lbl2": "Wealth Management",
                    "lblArea": "Portland, OR",
                    "lblDes": "I have been in banking for the last two years and have had over ten years of experience in customer service. I love helping people with their finances and ensuring they get the best services.",
                    "lblExpertise": "Expertise : ",
                    "lblFav": "Traveller ✈  Foodie 🍕 Love my kids 👨‍👩‍👧‍👦",
                    "lblHrs": "Hours :",
                    "lblNMLS": "NMLS 192938",
                    "lblName": "Kim Conners"
                }, {
                    "Copylbl0a1a0b464f64140": "Mon, Tue, Fri: 9am -5pm",
                    "btnGetStarted": "Choose Jean",
                    "imgProfile": "chooseyouagent2.png",
                    "imgagent": "agent1.png",
                    "lbl1": "Home Lending",
                    "lbl2": "Wealth Management",
                    "lblArea": "Portland, OR",
                    "lblDes": "I have been in banking for the last two years and have had over ten years of experience in customer service. I love helping people with their finances and ensuring they get the best services.",
                    "lblExpertise": "Expertise : ",
                    "lblFav": "I love helping you save money 💸",
                    "lblHrs": "Hours :",
                    "lblNMLS": "NMLS 192938",
                    "lblName": "Jean Marcus"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAgent",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknSegffffff",
                "rowTemplate": "tempNew",
                "scrollingEvents": {},
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0%",
                "viewConfig": {
                    "linearConfig": {
                        "isCircular": false
                    }
                },
                "viewType": constants.SEGUI_VIEW_TYPE_LINEAR,
                "widgetDataMap": {
                    "Copylbl0a1a0b464f64140": "Copylbl0a1a0b464f64140",
                    "btnGetStarted": "btnGetStarted",
                    "flxExpertise": "flxExpertise",
                    "flxMain": "flxMain",
                    "flxTiming": "flxTiming",
                    "imgProfile": "imgProfile",
                    "lbl1": "lbl1",
                    "lbl2": "lbl2",
                    "lblArea": "lblArea",
                    "lblDes": "lblDes",
                    "lblExpertise": "lblExpertise",
                    "lblFav": "lblFav",
                    "lblHrs": "lblHrs",
                    "lblNMLS": "lblNMLS",
                    "lblName": "lblName",
                    "tempNew": "tempNew"
                },
                "width": "95%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_ROW_SELECT,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            var btnGetStarted = new kony.ui.Button({
                "centerX": "50%",
                "height": "40dp",
                "id": "btnGetStarted",
                "isVisible": false,
                "left": "50dp",
                "onClick": controller.AS_Button_b71c119c77834e9580feb35fe640f8dc,
                "skin": "CopyCopydefBtnNormal",
                "text": "Choose Kim",
                "top": "88%",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxBelow.add(segAgent, btnGetStarted);
            flxMain.add(flxHeader, flxBelow);
            var flxGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "116.32%",
                "id": "flxGrey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxGrey",
                "top": "-60dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxGrey.setDefaultUnit(kony.flex.DP);
            var flxUppar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "45%",
                "clipBounds": true,
                "height": "45%",
                "id": "flxUppar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "37dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0hb8fc437fe0d40",
                "width": "75%",
                "zIndex": 1
            }, {}, {});
            flxUppar.setDefaultUnit(kony.flex.DP);
            var flxImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "flxImg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxImg.setDefaultUnit(kony.flex.DP);
            var imgFe = new kony.ui.Image2({
                "height": "100%",
                "id": "imgFe",
                "isVisible": true,
                "left": "0%",
                "src": "pallet.png",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImg.add(imgFe);
            var flxMatter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "flxMatter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknflxWhitebg1",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMatter.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblName",
                "isVisible": true,
                "skin": "sknLblBlackName",
                "text": "Your Choice",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl2 = new kony.ui.Label({
                "centerX": "50%",
                "id": "lbl2",
                "isVisible": true,
                "skin": "CopydefLabel0j6806a077cbe4c",
                "text": "Your agent is yours for as long as you want - but you can always choose a new agent or pull in one with more specific expertise!",
                "textStyle": {},
                "top": "27%",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var btnChoose = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "CopydefBtnFocus0d415ca83cdee42",
                "height": "25%",
                "id": "btnChoose",
                "isVisible": true,
                "onClick": controller.AS_Button_aec71ab6d0ad494f8a6897aac8d853dc,
                "skin": "CopyCopydefBtnNormal",
                "text": "Choose an Agent",
                "top": "65%",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMatter.add(lblName, lbl2, btnChoose);
            flxUppar.add(flxImg, flxMatter);
            flxGrey.add(flxUppar);
            this.add(flxMain, flxGrey);
        };
        return [{
            "addWidgets": addWidgetsfrmChooseAgent,
            "bounces": false,
            "enableScrolling": true,
            "enabledForIdleTimeout": false,
            "id": "frmChooseAgent",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_d8f912337c6743e0a287984878900207,
            "preShow": function(eventobject) {
                controller.AS_Form_h5c4a4a580844bb3afc685da4fc40842(eventobject);
            },
            "skin": "sknFrmBlueBG"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": true,
            "configureExtendTop": true,
            "configureStatusBarStyle": true,
            "extendBottom": true,
            "extendTop": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});