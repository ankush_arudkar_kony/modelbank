define("userflxMainController", {
    //Type your controller code here 
});
define("flxMainControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSwitch **/
    AS_Button_b7c7f225e4fb4c2391fa8006af230035: function AS_Button_b7c7f225e4fb4c2391fa8006af230035(eventobject, context) {
        var self = this;
        NavToNxtFrm("frmGetstarted");
    }
});
define("flxMainController", ["userflxMainController", "flxMainControllerActions"], function() {
    var controller = require("userflxMainController");
    var controllerActions = ["flxMainControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
