define("flxMain", function() {
    return function(controller) {
        var flxMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "96%",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxBG = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80%",
            "id": "flxBG",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBG.setDefaultUnit(kony.flex.DP);
        var imgBG = new kony.ui.Image2({
            "height": "100%",
            "id": "imgBG",
            "isVisible": true,
            "left": "0dp",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxBG.add(imgBG);
        var flxUpparImg = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "30%",
            "id": "flxUpparImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "top": "19%",
            "width": "65%",
            "zIndex": 1
        }, {}, {});
        flxUpparImg.setDefaultUnit(kony.flex.DP);
        var upparImg = new kony.ui.Image2({
            "height": "100%",
            "id": "upparImg",
            "isVisible": true,
            "left": "0dp",
            "src": "imagedrag.png",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUpparImg.add(upparImg);
        var lbl1 = new kony.ui.Label({
            "centerX": "50%",
            "id": "lbl1",
            "isVisible": true,
            "skin": "CopyCopydefLabel5",
            "text": "Label",
            "textStyle": {},
            "top": "69%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbl2 = new kony.ui.Label({
            "centerX": "50%",
            "id": "lbl2",
            "isVisible": true,
            "skin": "CopyCopydefLabel0g6cfac94908246",
            "text": "Label",
            "textStyle": {},
            "top": "80%",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblSwipe = new kony.ui.Label({
            "bottom": "2%",
            "centerX": "50%",
            "id": "lblSwipe",
            "isVisible": true,
            "skin": "CopyCopydefLabel0a3f7c7fa9f544c",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxMain.add(flxBG, flxUpparImg, lbl1, lbl2, lblSwipe);
        return flxMain;
    }
})