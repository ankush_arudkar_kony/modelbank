define("frmSendPPPayments", function() {
    return function(controller) {
        function addWidgetsfrmSendPPPayments() {
            this.setDefaultUnit(kony.flex.DP);
            var flxSendPayments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSendPayments",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSendPayments.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_g2d7e1a136e14465b5e800f7437ed98d,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b4d2d7410fda4212929c7b7c638bdf05,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Send Payments",
                "textStyle": {},
                "top": "50dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "100dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": false,
                "left": "5dp",
                "skin": "slImage",
                "src": "search_02.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtSelectedPerson = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "sknTxtBxTrans",
                "height": "98%",
                "id": "txtSelectedPerson",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0%",
                "secureTextEntry": false,
                "skin": "sknTxtBxBlackFOnt",
                "text": "(123) 456-7890",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1,
                "blur": {
                    "enabled": false,
                    "value": 36
                }
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTxtBxTrans",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtSelectedPerson);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            var txtAmount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "CopysknTxtBxTransBG0f853508411f14f",
                "height": "88%",
                "id": "txtAmount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "$0.00",
                "secureTextEntry": false,
                "skin": "sknTxtBxTransBG150Bold",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "CopysknTxtBxTransBG0f853508411f14f",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxAmount.add(flxLine, txtAmount);
            var flxKeyPad = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "200dp",
                "id": "flxKeyPad",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxKeyPad.setDefaultUnit(kony.flex.DP);
            var flx123 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx123",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx123.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl1",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_b194076a19694b4a81a689473d3fb881,
                "skin": "sknLblKey",
                "text": "1",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl2 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl2",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c,
                "skin": "sknLblKey",
                "text": "2",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl3 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl3",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_cd4bf4991d604d5c9235a4f2a0073419,
                "skin": "sknLblKey",
                "text": "3",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx123.add(lbl1, lbl2, lbl3);
            var flx456 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx456",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx456.setDefaultUnit(kony.flex.DP);
            var lbl4 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl4",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_a8313943b7994b55bb104e23d0bea023,
                "skin": "sknLblKey",
                "text": "4",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl5 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl5",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_c1f47c8e66d242fc893d55c068c44a3e,
                "skin": "sknLblKey",
                "text": "5",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl6 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl6",
                "isVisible": true,
                "onTouchStart": controller.AS_Label_e7abd80904f64838a05de2d67f2692e8,
                "right": "0dp",
                "skin": "sknLblKey",
                "text": "6",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx456.add(lbl4, lbl5, lbl6);
            var flx789 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flx789",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flx789.setDefaultUnit(kony.flex.DP);
            var lbl7 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl7",
                "isVisible": true,
                "left": "0dp",
                "onTouchStart": controller.AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b,
                "skin": "sknLblKey",
                "text": "7",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl8 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl8",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6,
                "skin": "sknLblKey",
                "text": "8",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl9 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl9",
                "isVisible": true,
                "left": "66%",
                "onTouchStart": controller.AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29,
                "skin": "sknLblKey",
                "text": "9",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flx789.add(lbl7, lbl8, lbl9);
            var flxRemove = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flxRemove",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRemove.setDefaultUnit(kony.flex.DP);
            var lbl0 = new kony.ui.Label({
                "height": "100%",
                "id": "lbl0",
                "isVisible": true,
                "left": "33%",
                "onTouchStart": controller.AS_Label_ef5e880ef423498293b0652c5cb97192,
                "skin": "sknLblKey",
                "text": "0",
                "textStyle": {},
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b69f5b5c9be445369c82fb226bde1807,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxDelete.setDefaultUnit(kony.flex.DP);
            var Image0abd38ce5079440 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "Image0abd38ce5079440",
                "isVisible": true,
                "right": "0dp",
                "skin": "slImage",
                "src": "delete_icn.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(Image0abd38ce5079440);
            flxRemove.add(lbl0, flxDelete);
            flxKeyPad.add(flx123, flx456, flx789, flxRemove);
            var bnContinue = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnContinue",
                "isVisible": true,
                "onClick": controller.AS_Button_h896e47f96464e30ac5654b013415b35,
                "skin": "sknBtnDisabled",
                "text": "Continue",
                "top": "340dp",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxSendPayments.add(FlexContainer0eed5fe5dc1064e, lblPersonalChecking, flxSearch, flxAmount, flxKeyPad, bnContinue);
            this.add(flxSendPayments);
        };
        return [{
            "addWidgets": addWidgetsfrmSendPPPayments,
            "enabledForIdleTimeout": false,
            "id": "frmSendPPPayments",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_b784026d058340bea91e753d72cafb39(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});