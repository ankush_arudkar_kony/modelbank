define("CopyflxAccounts0b622ef8af97e41", function() {
    return function(controller) {
        var CopyflxAccounts0b622ef8af97e41 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "CopyflxAccounts0b622ef8af97e41",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        CopyflxAccounts0b622ef8af97e41.setDefaultUnit(kony.flex.DP);
        var imgBankLogo = new kony.ui.Image2({
            "centerY": "50%",
            "height": "30dp",
            "id": "imgBankLogo",
            "isVisible": false,
            "left": "10dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "30dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountName = new kony.ui.Label({
            "id": "lblAccountName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblAccountName",
            "text": "Label",
            "textStyle": {},
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblBalance = new kony.ui.Label({
            "id": "lblBalance",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknlblAccountName",
            "text": "Label",
            "textStyle": {},
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblBankName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblBankName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblBankName",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblAvailableBalance = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAvailableBalance",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknlblBankName",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbllIne = new kony.ui.Label({
            "bottom": 3,
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i1af17ef0c8348",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        CopyflxAccounts0b622ef8af97e41.add(imgBankLogo, lblAccountName, lblBalance, lblBankName, lblAvailableBalance, lbllIne);
        return CopyflxAccounts0b622ef8af97e41;
    }
})