define("frmNAOVerify", function() {
    return function(controller) {
        function addWidgetsfrmNAOVerify() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f1554a789da044009c66c22cb8dfc1e2,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            flxHeader.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Verify Product Selection",
                "textStyle": {},
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxProductSelection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "220dp",
                "id": "flxProductSelection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "4dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0dbe9bb4d3aef41",
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxProductSelection.setDefaultUnit(kony.flex.DP);
            var lblSectionHeader = new kony.ui.Label({
                "id": "lblSectionHeader",
                "isVisible": true,
                "left": "8%",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Product Selections",
                "textStyle": {},
                "top": "32dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknLineDarkGrey0aedb86f2964e4e",
                "top": "11dp",
                "width": "87%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            var flxAccountswithImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccountswithImg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "92%",
                "zIndex": 1
            }, {}, {});
            flxAccountswithImg.setDefaultUnit(kony.flex.DP);
            var imgBankLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "imgBankLogo",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccountName = new kony.ui.Label({
                "id": "lblAccountName",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Deposit Account",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBalance = new kony.ui.Label({
                "id": "lblBalance",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Pro Checking",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBankName = new kony.ui.Label({
                "bottom": "15dp",
                "id": "lblBankName",
                "isVisible": false,
                "left": "53dp",
                "skin": "sknlblBankName",
                "text": "Label",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAvailableBalance = new kony.ui.Label({
                "bottom": "15dp",
                "height": "30dp",
                "id": "lblAvailableBalance",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0i8f404985d5b41",
                "text": "Our most popular products offering, with high inter....",
                "textStyle": {},
                "top": 30,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbllIne = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "lbllIne",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxAccountswithImg.add(imgBankLogo, lblAccountName, lblBalance, lblBankName, lblAvailableBalance, lbllIne);
            var CopyflxAccountswithImg0acb12e9c2ba440 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0acb12e9c2ba440",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "92%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0acb12e9c2ba440.setDefaultUnit(kony.flex.DP);
            var CopyimgBankLogo0e06b5a8df63545 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgBankLogo0e06b5a8df63545",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAccountName0h2f21fe320cb4f = new kony.ui.Label({
                "id": "CopylblAccountName0h2f21fe320cb4f",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Deposit Account",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0ec98481e30d545 = new kony.ui.Label({
                "id": "CopylblBalance0ec98481e30d545",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Youth Checking",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBankName0f1e2c5c991b44a = new kony.ui.Label({
                "bottom": "15dp",
                "id": "CopylblBankName0f1e2c5c991b44a",
                "isVisible": false,
                "left": "53dp",
                "skin": "sknlblBankName",
                "text": "Label",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAvailableBalance0c8827c9efea44d = new kony.ui.Label({
                "bottom": "15dp",
                "height": "30dp",
                "id": "CopylblAvailableBalance0c8827c9efea44d",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0i8f404985d5b41",
                "text": "Our most popular products offering, with high inter....",
                "textStyle": {},
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0d92d096a91834c = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0d92d096a91834c",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0acb12e9c2ba440.add(CopyimgBankLogo0e06b5a8df63545, CopylblAccountName0h2f21fe320cb4f, CopylblBalance0ec98481e30d545, CopylblBankName0f1e2c5c991b44a, CopylblAvailableBalance0c8827c9efea44d, CopylbllIne0d92d096a91834c);
            flxProductSelection.add(lblSectionHeader, flxLine, flxAccountswithImg, CopyflxAccountswithImg0acb12e9c2ba440);
            var CopyflxProductSelection0cf465965407c46 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "540dp",
                "id": "CopyflxProductSelection0cf465965407c46",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "4dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0g6eb5c820e5d4c",
                "top": "0dp",
                "width": "86%",
                "zIndex": 1
            }, {}, {});
            CopyflxProductSelection0cf465965407c46.setDefaultUnit(kony.flex.DP);
            var CopylblSectionHeader0a6a3938eabd247 = new kony.ui.Label({
                "id": "CopylblSectionHeader0a6a3938eabd247",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Personal Information",
                "textStyle": {},
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxLine0f57ec24095314d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0f57ec24095314d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "top": "15dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0f57ec24095314d.setDefaultUnit(kony.flex.DP);
            CopyflxLine0f57ec24095314d.add();
            var CopyflxAccountswithImg0e533b168ffa742 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0e533b168ffa742",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0e533b168ffa742.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0jf247832d58142 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0jf247832d58142",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Name",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0c80baa6650eb4b = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0c80baa6650eb4b",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Veronica Mathers",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0ab32d46ada7e4c = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0ab32d46ada7e4c",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0e533b168ffa742.add(CopylblAvailableBalance0jf247832d58142, CopylblBalance0c80baa6650eb4b, CopylbllIne0ab32d46ada7e4c);
            var CopyflxAccountswithImg0h1e7303c531b4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0h1e7303c531b4f",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0h1e7303c531b4f.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0d70b7430e8f843 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0d70b7430e8f843",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Date of Birth",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0ae0bb6cbe35848 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0ae0bb6cbe35848",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "01/01/1990",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0j3252b0ca15e43 = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0j3252b0ca15e43",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0h1e7303c531b4f.add(CopylblAvailableBalance0d70b7430e8f843, CopylblBalance0ae0bb6cbe35848, CopylbllIne0j3252b0ca15e43);
            var CopyflxAccountswithImg0a210c35e0f0345 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0a210c35e0f0345",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0a210c35e0f0345.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0j57bfed582e34b = new kony.ui.Label({
                "id": "CopylblAvailableBalance0j57bfed582e34b",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Gender",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0h73aeb411c0c47 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0h73aeb411c0c47",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Female",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0cc6b61051ae540 = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0cc6b61051ae540",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0a210c35e0f0345.add(CopylblAvailableBalance0j57bfed582e34b, CopylblBalance0h73aeb411c0c47, CopylbllIne0cc6b61051ae540);
            var CopyflxAccountswithImg0f019620be7ba49 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0f019620be7ba49",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0f019620be7ba49.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0hb11d9612d8140 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0hb11d9612d8140",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Maritial Status",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0i6fd3dbf8fc649 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0i6fd3dbf8fc649",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "-",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0ed8ae83046da47 = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0ed8ae83046da47",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0f019620be7ba49.add(CopylblAvailableBalance0hb11d9612d8140, CopylblBalance0i6fd3dbf8fc649, CopylbllIne0ed8ae83046da47);
            var CopyflxAccountswithImg0b60fc205bbb247 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0b60fc205bbb247",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0b60fc205bbb247.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0a9f47b5b09bb44 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0a9f47b5b09bb44",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Dependents",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0c856a9e6577b45 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0c856a9e6577b45",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "-",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0eb9b77f6617c46 = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0eb9b77f6617c46",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0b60fc205bbb247.add(CopylblAvailableBalance0a9f47b5b09bb44, CopylblBalance0c856a9e6577b45, CopylbllIne0eb9b77f6617c46);
            var CopyflxAccountswithImg0h8cfff1bfe9344 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "80dp",
                "id": "CopyflxAccountswithImg0h8cfff1bfe9344",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0h8cfff1bfe9344.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0g8491f7e918a49 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0g8491f7e918a49",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Address",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0e3203edfcd904d = new kony.ui.Label({
                "id": "CopylblBalance0e3203edfcd904d",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "1234 Willow Street.st, SanFrancisco, CA 12345",
                "textStyle": {},
                "top": "2dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0jf403806dc0948 = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0jf403806dc0948",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0h8cfff1bfe9344.add(CopylblAvailableBalance0g8491f7e918a49, CopylblBalance0e3203edfcd904d, CopylbllIne0jf403806dc0948);
            var CopyflxAccountswithImg0e3387fe365af41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "CopyflxAccountswithImg0e3387fe365af41",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyflxAccountswithImg0e3387fe365af41.setDefaultUnit(kony.flex.DP);
            var CopylblAvailableBalance0c40db7c4af6a4f = new kony.ui.Label({
                "id": "CopylblAvailableBalance0c40db7c4af6a4f",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Social Security Number",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblBalance0jd7bceb582aa42 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblBalance0jd7bceb582aa42",
                "isVisible": true,
                "left": "5dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "xxx-xxx-1234",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylbllIne0f15caed341a44e = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "CopylbllIne0f15caed341a44e",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxAccountswithImg0e3387fe365af41.add(CopylblAvailableBalance0c40db7c4af6a4f, CopylblBalance0jd7bceb582aa42, CopylbllIne0f15caed341a44e);
            CopyflxProductSelection0cf465965407c46.add(CopylblSectionHeader0a6a3938eabd247, CopyflxLine0f57ec24095314d, CopyflxAccountswithImg0e533b168ffa742, CopyflxAccountswithImg0h1e7303c531b4f, CopyflxAccountswithImg0a210c35e0f0345, CopyflxAccountswithImg0f019620be7ba49, CopyflxAccountswithImg0b60fc205bbb247, CopyflxAccountswithImg0h8cfff1bfe9344, CopyflxAccountswithImg0e3387fe365af41);
            var flxInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "62dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxInfo.setDefaultUnit(kony.flex.DP);
            var imgInfo = new kony.ui.Image2({
                "centerX": "50%",
                "height": "25dp",
                "id": "imgInfo",
                "isVisible": true,
                "left": "124dp",
                "skin": "slImage",
                "src": "info.png",
                "top": "18dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAvailableBalance0ibfeb5d53f024c = new kony.ui.Label({
                "id": "CopylblAvailableBalance0ibfeb5d53f024c",
                "isVisible": true,
                "left": "23dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "You will not be able to edit this application beyond this step",
                "textStyle": {},
                "top": "20dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAvailableBalance0e7e35f3ce84341 = new kony.ui.Label({
                "id": "CopylblAvailableBalance0e7e35f3ce84341",
                "isVisible": true,
                "left": "23dp",
                "right": "55dp",
                "skin": "CopysknlblBankName0e6c07c48547c41",
                "text": "Once you submit, we'll perform a credit check to complete your application",
                "textStyle": {},
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxInfo.add(imgInfo, CopylblAvailableBalance0ibfeb5d53f024c, CopylblAvailableBalance0e7e35f3ce84341);
            var btnMaualCapture = new kony.ui.Button({
                "bottom": "20dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "btnMaualCapture",
                "isVisible": true,
                "left": "29dp",
                "onClick": controller.AS_Button_c634db93227548dcaf5838a8cea80406,
                "skin": "sknBtnBlueN",
                "text": "Submit",
                "top": "10dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMain.add(flxHeader, lblPersonalChecking, flxProductSelection, CopyflxProductSelection0cf465965407c46, flxInfo, btnMaualCapture);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmNAOVerify,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmNAOVerify",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});