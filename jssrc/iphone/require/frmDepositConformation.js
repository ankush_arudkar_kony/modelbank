define("frmDepositConformation", function() {
    return function(controller) {
        function addWidgetsfrmDepositConformation() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "149dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxWhiteBG0d280610fd58748",
                "top": "42dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var imgConfirmed = new kony.ui.Image2({
                "centerX": "50%",
                "height": "150dp",
                "id": "imgConfirmed",
                "isVisible": true,
                "skin": "slImage",
                "src": "ok_03.png",
                "top": "15dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "15dp",
                "skin": "skinhundred",
                "text": "We successfully deposited your check",
                "textStyle": {},
                "top": "7dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSubAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSubAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%"
            }, {}, {});
            flxSubAccounts.setDefaultUnit(kony.flex.DP);
            var imgArrow = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "imgArrow",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmount",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Reference ID",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmountValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountValue",
                "isVisible": true,
                "right": "25dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "878566910255",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxSubAccounts.add(imgArrow, lblAmount, lblAmountValue);
            var CopyflxSubAccounts0h08481135e5b4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "CopyflxSubAccounts0h08481135e5b4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0h08481135e5b4a.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0g1583a8e2e4e4f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0g1583a8e2e4e4f",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0a892ab22eeb94c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0a892ab22eeb94c",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Deposit Account",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0b0865b50dc5a41 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0b0865b50dc5a41",
                "isVisible": true,
                "right": "25dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "Personal Checking…x4567",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxSubAccounts0h08481135e5b4a.add(CopyimgArrow0g1583a8e2e4e4f, CopylblAmount0a892ab22eeb94c, CopylblAmountValue0b0865b50dc5a41);
            var CopyflxSubAccounts0f8d4f053a1b248 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "CopyflxSubAccounts0f8d4f053a1b248",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0f8d4f053a1b248.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0h6b8a2c004f244 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0h6b8a2c004f244",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0be2dba7e40aa42 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0be2dba7e40aa42",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmountEntered = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountEntered",
                "isVisible": true,
                "right": "25dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "$1,500.00",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxSubAccounts0f8d4f053a1b248.add(CopyimgArrow0h6b8a2c004f244, CopylblAmount0be2dba7e40aa42, lblAmountEntered);
            var CopyflxSubAccounts0f44cf973077140 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "CopyflxSubAccounts0f44cf973077140",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            CopyflxSubAccounts0f44cf973077140.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0ce8fc3b652194d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0ce8fc3b652194d",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "chevron_go.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0ab1f08889bdb4f = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0ab1f08889bdb4f",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Deposit Date",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0dbc048ae9b9740 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmountValue0dbc048ae9b9740",
                "isVisible": true,
                "right": "25dp",
                "skin": "CopysknlblAccountName0be10f542987848",
                "text": "09/12/19",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxSubAccounts0f44cf973077140.add(CopyimgArrow0ce8fc3b652194d, CopylblAmount0ab1f08889bdb4f, CopylblAmountValue0dbc048ae9b9740);
            var btnMaualCapture = new kony.ui.Button({
                "centerX": "50.04%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "btnMaualCapture",
                "isVisible": true,
                "left": "29dp",
                "onClick": controller.AS_Button_i0594f51f68a4b1f9a48151cbf73ae2a,
                "skin": "sknBtnBlueN",
                "text": "New Transfers",
                "top": "15%",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var CopybtnMaualCapture0c25aa22a5aba4e = new kony.ui.Button({
                "bottom": "20dp",
                "centerX": "50.04%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "CopybtnMaualCapture0c25aa22a5aba4e",
                "isVisible": true,
                "left": "29dp",
                "onClick": controller.AS_Button_a10acfb9a72f4ba389697cf1a996c584,
                "skin": "sknBtnBlueN",
                "text": "My Accounts",
                "top": "10dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMain.add(imgConfirmed, lblPersonalChecking, flxSubAccounts, CopyflxSubAccounts0h08481135e5b4a, CopyflxSubAccounts0f8d4f053a1b248, CopyflxSubAccounts0f44cf973077140, btnMaualCapture, CopybtnMaualCapture0c25aa22a5aba4e);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmDepositConformation,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmDepositConformation",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ed47d030412447ed82ae61e251cf33a1(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});