define("userfrmTransferToController", {
    //Type your controller code here 
});
define("frmTransferToControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_d8e47a200c14475dbc3df1139ff4625f: function AS_FlexContainer_d8e47a200c14475dbc3df1139ff4625f(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_e4a46b45c72546fd8d32da85ee26aa67: function AS_FlexContainer_e4a46b45c72546fd8d32da85ee26aa67(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_i0e3768eaf14456089711015dc38d905: function AS_Segment_i0e3768eaf14456089711015dc38d905(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedToAccountRow = this.view.flxCheckingAccounts.segAccRecords.selectedRowItems[0];
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_g48b2e5f71c2427ea85934097131de96: function AS_Segment_g48b2e5f71c2427ea85934097131de96(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedToAccountRow = this.view.flxSavingsAcounts.segAccRecords.selectedRowItems[0];
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_adb3459b02f649d1a14d53842fd38b58: function AS_Segment_adb3459b02f649d1a14d53842fd38b58(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedToAccountRow = this.view.EnternalRecipients.segAccRecords.selectedRowItems[0];
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onRowClick defined for segPersonToPerson **/
    AS_Segment_fd51a3b23f1842f8a5d52be76645c3e1: function AS_Segment_fd51a3b23f1842f8a5d52be76645c3e1(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedPPID = this.view.segPersonToPerson.selectedRowItems[0].lblBankName;
        if (this.view.segPersonToPerson.selectedRowItems[0].imgPayPal == "paypal_icon.png") {
            navToRespForm("frmSendPPPayments");
        }
    }
});
define("frmTransferToController", ["userfrmTransferToController", "frmTransferToControllerActions"], function() {
    var controller = require("userfrmTransferToController");
    var controllerActions = ["frmTransferToControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
