define("frmPPSendSuccess", function() {
    return function(controller) {
        function addWidgetsfrmPPSendSuccess() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c90e76f7a3cb4d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "FlexContainer0c90e76f7a3cb4d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c90e76f7a3cb4d.setDefaultUnit(kony.flex.DP);
            var Image0d9b2443b016b41 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "40dp",
                "id": "Image0d9b2443b016b41",
                "isVisible": true,
                "skin": "slImage",
                "src": "paypal_icon.png",
                "width": "40dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c90e76f7a3cb4d.add(Image0d9b2443b016b41);
            var FlexContainer0c846579cfa2b44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "FlexContainer0c846579cfa2b44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c846579cfa2b44.setDefaultUnit(kony.flex.DP);
            var Image0f7270bbd061d44 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "80dp",
                "id": "Image0f7270bbd061d44",
                "isVisible": true,
                "left": "133dp",
                "skin": "slImage",
                "src": "ok_03.png",
                "top": "20dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c846579cfa2b44.add(Image0f7270bbd061d44);
            var lblTransferredAmt = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTransferredAmt",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "You transferred $49.50",
                "textStyle": {},
                "top": "20dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxFrequency.setDefaultUnit(kony.flex.DP);
            var CopyLabel0g369a6beee8c49 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0g369a6beee8c49",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Estimated Arrival",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFrequency = new kony.ui.Label({
                "height": "100%",
                "id": "lblFrequency",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl110SemiBold222222",
                "text": "typically in minutes",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxFrequency.add(CopyLabel0g369a6beee8c49, lblFrequency);
            var CopyLabel0fd6aaa1db9ec4b = new kony.ui.Label({
                "centerX": "50%",
                "id": "CopyLabel0fd6aaa1db9ec4b",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "You transferred money to your bank using your bank debit card",
                "textStyle": {},
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var bnTransfer = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnTransfer",
                "isVisible": true,
                "onClick": controller.AS_Button_bda9546064cb4846aee025968d709575,
                "skin": "sknBtnBlueN",
                "text": "Done",
                "top": "10%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxMain.add(FlexContainer0c90e76f7a3cb4d, FlexContainer0c846579cfa2b44, lblTransferredAmt, flxFrequency, CopyLabel0fd6aaa1db9ec4b, bnTransfer);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmPPSendSuccess,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmPPSendSuccess",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_b0ab4ac6d80a4ff5a2db97be645c1bbd(eventobject);
            },
            "skin": "sknFromBGWhite"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});