define("frmScaning", function() {
    return function(controller) {
        function addWidgetsfrmScaning() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "138dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "94dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var Image0d599dcc0712349 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100dp",
                "id": "Image0d599dcc0712349",
                "isVisible": true,
                "left": "368dp",
                "skin": "slImage",
                "src": "loading.png",
                "top": "140dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0af544cbedd1d42 = new kony.ui.Label({
                "bottom": "50dp",
                "centerX": "50%",
                "id": "Label0af544cbedd1d42",
                "isVisible": true,
                "left": "331dp",
                "skin": "defLabel",
                "text": "Scanning your check information",
                "textStyle": {},
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxMain.add(Image0d599dcc0712349, Label0af544cbedd1d42);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmScaning,
            "enabledForIdleTimeout": false,
            "id": "frmScaning",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_gc05465ba7df451c915e28863482ccbd(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_LANDSCAPE,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});