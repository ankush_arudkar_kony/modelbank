define("frmGetstarted", function() {
    return function(controller) {
        function addWidgetsfrmGetstarted() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "-60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var segIntro = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgBG": "bgimage1.png",
                    "lbl1": "A trusted partner",
                    "lbl2": "A human, financial expert - right in your pocket. With you from lost cards to home loans",
                    "lblSwipe": "Swipe to learn more",
                    "upparImg": "engage1.png"
                }, {
                    "imgBG": "bgimage2.png",
                    "lbl1": "It's all talk",
                    "lbl2": "Have a question or financial issue? Just ask your agent! Help is there for you whenever and wherever you need it.",
                    "lblSwipe": "",
                    "upparImg": "engage2.png"
                }, {
                    "imgBG": "bgimage3.png",
                    "lbl1": "Just for Fun",
                    "lbl2": "The perfect agent - with you in mind. For whatever life throws at you, getting help is easy and secure.",
                    "lblSwipe": "",
                    "upparImg": "engage3.png"
                }],
                "groupCells": false,
                "height": "82%",
                "id": "segIntro",
                "isVisible": true,
                "left": "0%",
                "needPageIndicator": true,
                "pageOffDotImage": "dotn.png",
                "pageOnDotImage": "dotf.png",
                "retainSelection": false,
                "rowSkin": "segNormlSkn",
                "rowTemplate": "tempSwipe",
                "scrollingEvents": {},
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0%",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "flxBG": "flxBG",
                    "flxUpparImg": "flxUpparImg",
                    "imgBG": "imgBG",
                    "lbl1": "lbl1",
                    "lbl2": "lbl2",
                    "lblSwipe": "lblSwipe",
                    "tempSwipe": "tempSwipe",
                    "upparImg": "upparImg"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": false,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_ROW_SELECT,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            var btnGetStarted = new kony.ui.Button({
                "centerX": "51%",
                "focusSkin": "sknBtnFocus",
                "height": "7%",
                "id": "btnGetStarted",
                "isVisible": true,
                "left": "50dp",
                "onClick": controller.AS_Button_fad1332a28744e3f8104a0a3f0744a70,
                "skin": "sknBtnSignInF",
                "text": "Get Started",
                "top": "88%",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            var btnSwitch = new kony.ui.Button({
                "focusSkin": "sknHiddenBtnF",
                "height": "15%",
                "id": "btnSwitch",
                "isVisible": true,
                "left": "85%",
                "onClick": controller.AS_Button_ca922ffd0b17419b8da049bcec578b7e,
                "skin": "btnTrans",
                "top": "7%",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "8%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var imgBack = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "imgBack",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "backarrow_2.png",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnBack = new kony.ui.Button({
                "focusSkin": "CopysknBtnTrans",
                "height": "100%",
                "id": "btnBack",
                "isVisible": true,
                "left": "0dp",
                "onClick": controller.AS_Button_acabfaddb33a41f5ad531b6acac64f3c,
                "skin": "CopysknBtnTrans",
                "text": "Button",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": false,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": false
            });
            flxBack.add(imgBack, btnBack);
            flxMain.add(segIntro, btnGetStarted, btnSwitch, flxBack);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmGetstarted,
            "enabledForIdleTimeout": false,
            "id": "frmGetstarted",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_b7c8fdff57fd45428a3bd09ecbcd67d8(eventobject);
            },
            "skin": "CopysknFrmBlueBg0he826c286b214a"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": true,
            "configureStatusBarStyle": true,
            "extendTop": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});