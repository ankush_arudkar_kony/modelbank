define("userfrmAccountDetailsController", {
    data_PendingTransactions: [
        [{
                lblAccount: "Pending Transactions",
                template: "flxHeaderTran"
            },
            [{
                lblAccountName: {
                    skin: "sknLblPendingTransName",
                    text: "Samantha Simmons"
                },
                lblBalance: {
                    skin: "sknLblPendingTransAmount",
                    text: "$500.55"
                },
                lblBankName: {
                    skin: "sknLblBankNameIt",
                    text: "Sent Money"
                },
                imgBankLogo: {
                    "src": "acc_detailspic1.png",
                    "isVisible": false
                },
                lblAvailableBalance: {
                    skin: "sknLblBankNameIt",
                    text: "$55,360.11"
                },
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: {
                    skin: "sknLblPendingTransName",
                    text: "Home Depot"
                },
                lblBalance: {
                    skin: "sknLblPendingTransAmount",
                    text: "$25.55"
                },
                lblBankName: {
                    skin: "sknLblBankNameIt",
                    text: "Home Improvement"
                },
                imgBankLogo: "acc_detailspic2.png",
                lblAvailableBalance: {
                    skin: "sknLblBankNameIt",
                    text: "$55,360.11"
                },
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: {
                    skin: "sknLblPendingTransName",
                    text: "Bank of America Rewards"
                },
                lblBalance: {
                    skin: "sknLblPendingTransAmountGreen",
                    text: "+25.55"
                },
                lblBankName: {
                    skin: "sknLblBankNameIt",
                    text: "Credit Card Cash Back"
                },
                imgBankLogo: "acc_detailspic3.png",
                lblAvailableBalance: {
                    skin: "sknLblBankNameIt",
                    text: "$55,360.11"
                },
                template: "flxAccounts"
            }, ]
        ],
    ],
    data_PostedTransactions: [
        [{
                lblAccount: "Thursday,August 22,2019",
                template: "flxHeaderTran"
            },
            [{
                lblAccountName: "Wells Fargo Mortgage",
                lblBalance: "$500.55",
                lblBankName: "Mortgage & Rent",
                imgBankLogo: "acc_detailspic4.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Hewlett Packard",
                lblBalance: "$25.55",
                lblBankName: "Electronics",
                imgBankLogo: "acc_detailspic5.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Apple Card",
                lblBalance: {
                    skin: "sknLblPendingTransAmountGreen",
                    text: "+25.55"
                },
                lblBankName: "Daily Cash Back",
                imgBankLogo: "acc_detailspic6.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Instacart",
                lblBalance: "$25.55",
                lblBankName: "Groceries",
                imgBankLogo: "acc_detailspic7.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Lyft Pay",
                lblBalance: {
                    skin: "sknLblPendingTransAmountGreen",
                    text: "+500.55"
                },
                lblBankName: "Paycheck",
                imgBankLogo: "acc_detailspic8.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Instacart",
                lblBalance: "$25.55",
                lblBankName: "Groceries",
                imgBankLogo: "acc_detailspic7.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "McDonald's",
                lblBalance: "$25.55",
                lblBankName: "Quick Meal",
                imgBankLogo: "acc_detailspic10.png",
                lblAvailableBalance: "$55,360.11",
                lbllIne: "-",
                template: "flxAccounts"
            }, {
                lblAccountName: "Lyft Pay",
                lblBalance: {
                    skin: "sknLblPendingTransAmountGreen",
                    text: "+450.55"
                },
                lblBankName: "Paycheck",
                imgBankLogo: "acc_detailspic8.png",
                lblAvailableBalance: "$55,360.11",
                template: "flxAccounts"
            }, ]
        ],
    ],
    preShowFun: function() {
        this.view.flxSegPending.segPendingTransactions.setData([
            [{
                    lblAccount: "Pending Transactions",
                    template: "flxTransactionsHeader"
                },
                [{
                    lblAccountName: {
                        text: "Samantha Simmons"
                    },
                    lblBalance: {
                        skin: "sknLblPendingTransAmount",
                        text: "$500.55"
                    },
                    lblBankName: {
                        text: "Sent Money"
                    },
                    imgBankLogo: {
                        "src": "acc_detailspic1.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: {
                        text: "$55,360.11"
                    },
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: {
                        text: "Home Depot"
                    },
                    lblBalance: {
                        skin: "sknLblPendingTransAmount",
                        text: "$25.55"
                    },
                    lblBankName: {
                        text: "Home Improvement"
                    },
                    imgBankLogo: {
                        "src": "acc_detailspic2.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: {
                        text: "$55,360.11"
                    },
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: {
                        text: "Bank of America Rewards"
                    },
                    lblBalance: {
                        skin: "sknLblPendingTransAmountGreen",
                        text: "+25.55"
                    },
                    lblBankName: {
                        text: "Credit Card Cash Back"
                    },
                    imgBankLogo: {
                        "src": "acc_detailspic3.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: {
                        text: "$55,360.11"
                    },
                    template: "flxAccountswithImg"
                }, ]
            ]
        ]);
        this.view.flxSegPosted.segPostedTransactions.setData([
            [{
                    lblAccount: "Thursday, August 22, 2019",
                    template: "flxTransactionsHeader"
                },
                [{
                    lblAccountName: "Wells Fargo Mortgage",
                    lblBalance: "$500.55",
                    lblBankName: "Mortgage & Rent",
                    imgBankLogo: {
                        "src": "acc_detailspic4.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Hewlett Packard",
                    lblBalance: "$25.55",
                    lblBankName: "Electronics",
                    imgBankLogo: {
                        "src": "acc_detailspic5.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Apple Card",
                    lblBalance: {
                        skin: "sknLblPendingTransAmountGreen",
                        text: "+25.55"
                    },
                    lblBankName: "Daily Cash Back",
                    imgBankLogo: {
                        "src": "acc_detailspic6.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Instacart",
                    lblBalance: "$25.55",
                    lblBankName: "Groceries",
                    imgBankLogo: {
                        "src": "acc_detailspic7.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Lyft Pay",
                    lblBalance: {
                        skin: "sknLblPendingTransAmountGreen",
                        text: "+500.55"
                    },
                    lblBankName: "Paycheck",
                    imgBankLogo: {
                        "src": "acc_detailspic8.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Instacart",
                    lblBalance: "$25.55",
                    lblBankName: "Groceries",
                    imgBankLogo: {
                        "src": "acc_detailspic7.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "McDonald's",
                    lblBalance: "$25.55",
                    lblBankName: "Quick Meal",
                    imgBankLogo: {
                        "src": "acc_detailspic10.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    lbllIne: "-",
                    template: "flxAccountswithImg"
                }, {
                    lblAccountName: "Lyft Pay",
                    lblBalance: {
                        skin: "sknLblPendingTransAmountGreen",
                        text: "+450.55"
                    },
                    lblBankName: "Paycheck",
                    imgBankLogo: {
                        "src": "acc_detailspic8.png",
                        "isVisible": true
                    },
                    lblAvailableBalance: "$55,360.11",
                    template: "flxAccountswithImg"
                }, ]
            ],
        ]);
    },
    AnimationFun: function() {
        this.view.flxAccount.animate(kony.ui.createAnimation({
            "100": {
                "top": "100dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0.060,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        }, {
            // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
        });
        this.view.flxPendingTransactions.animate(kony.ui.createAnimation({
            "100": {
                "top": "310dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0.060,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        }, {
            // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
        });
        this.view.flxPostedTransactions.animate(kony.ui.createAnimation({
            "100": {
                "top": "600dp",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0.060,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.5
        }, {
            // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
        });
    },
    personalCheckingAni: function() {
        //this.view.lblPersonalCheckingnew.skin = "skinhundred";
        //this.view.lblPersonalCheckingnew.  ="50%";
        var controllerScope = this;

        function MOVE_ACTION____gdc376f5aedf4924b2d55f3f600e8f66_Callback() {
            // alert(controllerScope.view.flxWelcomenew.centerX) ;
        }
        //alert(controllerScope.view.flxWelcomenew.centerX) ;
        // widConst = kony.os.deviceInfo().deviceWidth/2;
        // alert( );
        this.view.flxWelcomenew.animate(kony.ui.createAnimation({
            "100": {
                "top": "15dp",
                "centerX": "" + 50 + "%", //"50%",
                //  "centerX": "50%",//
                //"width":"50%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____gdc376f5aedf4924b2d55f3f600e8f66_Callback
        });
    },
    personalcheckaniTwo: function() {
        //this.view.lblPersonalCheckingnew.skin = "skinhundred";
        //this.view.lblPersonalCheckingnew.  ="50%";
        var controllerScope = this;

        function MOVE_ACTION____gdc376f5aedf4924b2d55f3f600e8f66_Callback() {
            //  alert(JSON.stringify( this.view.flxWelcomenew.centerX  ));
        }
        // widConst = kony.os.deviceInfo().deviceWidth/2;
        // alert(this.view.flxWelcomenew.centerX  );
        this.view.flxWelcomenew.animate(kony.ui.createAnimation({
            "100": {
                "top": "65dp",
                "centerX": this.centrXOfflxWelcomenew, //"50%",
                //  "centerX": "50%",//
                //"width":"50%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": MOVE_ACTION____gdc376f5aedf4924b2d55f3f600e8f66_Callback
        });
    },
    getoccSet: function() {
        alert(this.view.flxContent.contentOffsetMeasured.y);
    },
    position: true,
    onScrolling: function() {
        var offset = this.view.flxContent.contentOffsetMeasured.y;
        if (offset > 30 && this.position) {
            this.personalCheckingAni();
            this.position = false;
        } else if (offset < 30 && !this.position) {
            this.personalcheckaniTwo();
            this.position = true;
        }
    }
});
define("frmAccountDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for yimgBacknew **/
    AS_Image_g4f49d62e5384d15b6e6840dd058f0af: function AS_Image_g4f49d62e5384d15b6e6840dd058f0af(eventobject, x, y) {
        var self = this;
        navToRespForm("frmDashboardAggregated");
    },
    /** onScrolling defined for flxContent **/
    AS_FlexScrollContainer_f15b02b2235442398caa3bb8ab56f859: function AS_FlexScrollContainer_f15b02b2235442398caa3bb8ab56f859(eventobject) {
        var self = this;
        return self.onScrolling.call(this);
    },
    /** preShow defined for frmAccountDetails **/
    AS_Form_a3ece9b58dc848b688af710cfb6e868e: function AS_Form_a3ece9b58dc848b688af710cfb6e868e(eventobject) {
        var self = this;
        this.view.flxBtns.top = "0dp";
        this.view.flxWelcome.top = "50dp";
        this.view.flxPendingTransactions.top = "1240dp";
        this.view.flxPostedTransactions.top = "1240dp";
        this.view.flxAccount.top = "1240dp";
        this.centrXOfflxWelcomenew = this.view.flxWelcomenew.centerX;
        this.view.lblPersonalCheckingnew.text = Account.lblAccountName;
        self.preShowFun.call(this);
    },
    /** postShow defined for frmAccountDetails **/
    AS_Form_e81921d0671040bd888eecc0d2f4e0d0: function AS_Form_e81921d0671040bd888eecc0d2f4e0d0(eventobject) {
        var self = this;
        return self.AnimationFun.call(this);
    }
});
define("frmAccountDetailsController", ["userfrmAccountDetailsController", "frmAccountDetailsControllerActions"], function() {
    var controller = require("userfrmAccountDetailsController");
    var controllerActions = ["frmAccountDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
