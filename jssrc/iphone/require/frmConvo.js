define("frmConvo", function() {
    return function(controller) {
        function addWidgetsfrmConvo() {
            this.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox4",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var imgsearch = new kony.ui.Image2({
                "centerY": "50%",
                "height": "60%",
                "id": "imgsearch",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_f3127fc8a1454829994528d1ae5fbd53,
                "right": "8%",
                "src": "users.png",
                "width": "8%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgBack = new kony.ui.Image2({
                "centerY": "50%",
                "height": "60%",
                "id": "imgBack",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_c1673b6f386846d5b2d2f5d9890078db,
                "right": "88%",
                "skin": "slImage",
                "src": "backarrow_1.png",
                "width": "8%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgChat = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "90%",
                "id": "imgChat",
                "isVisible": true,
                "src": "agent_1.png",
                "width": "20%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearch.add(imgsearch, imgBack, imgChat);
            flxHeader.add(flxSearch);
            var flxMAin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxMAin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "12.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMAin.setDefaultUnit(kony.flex.DP);
            var flxScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "70dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScroll",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0ccb1a1d262ed48",
                "top": "0%",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScroll.setDefaultUnit(kony.flex.DP);
            var flxBelow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBelow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBelow.setDefaultUnit(kony.flex.DP);
            var flxJoined = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "7%",
                "id": "flxJoined",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxJoined.setDefaultUnit(kony.flex.DP);
            var lblline1 = new kony.ui.Label({
                "height": "2%",
                "id": "lblline1",
                "isVisible": true,
                "left": "7%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblJoinedConvo = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblJoinedConvo",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Kim Conners Joined the conersation",
                "textStyle": {},
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblLine2 = new kony.ui.Label({
                "height": "2%",
                "id": "lblLine2",
                "isVisible": true,
                "left": "76%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxJoined.add(lblline1, lblJoinedConvo, lblLine2);
            var flxProfileDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "16%",
                "id": "flxProfileDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "sknflxGreyBorder",
                "top": "0%",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxProfileDetails.setDefaultUnit(kony.flex.DP);
            var imgagent = new kony.ui.Image2({
                "centerY": "50%",
                "height": "50%",
                "id": "imgagent",
                "isVisible": true,
                "left": "18dp",
                "src": "imagedrag.png",
                "top": "12dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "22%",
                "skin": "sknlblname",
                "text": "Kim Conners",
                "textStyle": {},
                "top": "18%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFav = new kony.ui.Label({
                "id": "lblFav",
                "isVisible": true,
                "left": "22%",
                "skin": "CopyCopyCopydefLabel1",
                "text": "Traveller ✈  Foodie 🍕 Love my kids 👨‍👩‍👧‍👦",
                "textStyle": {},
                "top": "50%",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxProfileDetails.add(imgagent, lblName, lblFav);
            var lblTime = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTime",
                "isVisible": true,
                "skin": "CopyCopyCopydefLabel1",
                "text": "time",
                "textStyle": {},
                "top": "1%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxAgentChat1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAgentChat1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1%",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxAgentChat1.setDefaultUnit(kony.flex.DP);
            var agentImg1 = new kony.ui.Image2({
                "bottom": "15dp",
                "height": "45dp",
                "id": "agentImg1",
                "isVisible": true,
                "left": "1%",
                "skin": "slImage",
                "src": "agent_1.png",
                "width": "25%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblKim = new kony.ui.Label({
                "centerX": "49%",
                "id": "lblKim",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Kim Conners ",
                "textStyle": {},
                "top": "5dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxChat0a20b8ee08ffb4f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "CopyflxChat0a20b8ee08ffb4f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "24%",
                "isModalContainer": false,
                "skin": "flxChatgrey",
                "top": "20dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            CopyflxChat0a20b8ee08ffb4f.setDefaultUnit(kony.flex.DP);
            var CopylblChat0g036531921da4a = new kony.ui.Label({
                "bottom": "5dp",
                "id": "CopylblChat0g036531921da4a",
                "isVisible": true,
                "left": "2%",
                "skin": "SknLBlGreyChat",
                "text": "Hey Dan! Thanks for picking me as your agent. I can help you with all of your financial questions and banking needs. Is there anything that i can help you with today?",
                "textStyle": {},
                "top": "5dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxChat0a20b8ee08ffb4f.add(CopylblChat0g036531921da4a);
            flxAgentChat1.add(agentImg1, lblKim, CopyflxChat0a20b8ee08ffb4f);
            var flxCustChat1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustChat1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "2%",
                "skin": "sknflxBluechat",
                "top": "3%",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxCustChat1.setDefaultUnit(kony.flex.DP);
            var lblCustChat1 = new kony.ui.Label({
                "bottom": "5dp",
                "centerX": "50%",
                "id": "lblCustChat1",
                "isVisible": true,
                "skin": "sknLblChatWhite",
                "text": "Awesome ! I can't wait. I'm really excited. I was wondering if you could help opening a new account?",
                "textStyle": {},
                "top": "5dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxCustChat1.add(lblCustChat1);
            var flxAgentChat2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAgentChat2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2%",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxAgentChat2.setDefaultUnit(kony.flex.DP);
            var imgAgent2 = new kony.ui.Image2({
                "bottom": "15dp",
                "height": "45dp",
                "id": "imgAgent2",
                "isVisible": true,
                "left": "1%",
                "skin": "slImage",
                "src": "agent_1.png",
                "width": "25%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgent2 = new kony.ui.Label({
                "centerX": "47%",
                "id": "lblAgent2",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Kim Conners ",
                "textStyle": {},
                "top": "5dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxChat = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxChat",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "24%",
                "isModalContainer": false,
                "skin": "flxChatgrey",
                "top": "20dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxChat.setDefaultUnit(kony.flex.DP);
            var lblChat2 = new kony.ui.Label({
                "bottom": "5dp",
                "id": "lblChat2",
                "isVisible": true,
                "left": "3%",
                "skin": "SknLBlGreyChat",
                "text": "Definetly! Lets get that started right away. What kind of account were you looking to open? Are there any specific goals that you're trying to achieve",
                "textStyle": {},
                "top": "5dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxChat.add(lblChat2);
            flxAgentChat2.add(imgAgent2, lblAgent2, flxChat);
            flxBelow.add(flxJoined, flxProfileDetails, lblTime, flxAgentChat1, flxCustChat1, flxAgentChat2);
            var flxJeanChat = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60%",
                "id": "flxJeanChat",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxJeanChat.setDefaultUnit(kony.flex.DP);
            var flxKimLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "8%",
                "id": "flxKimLeft",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "1%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKimLeft.setDefaultUnit(kony.flex.DP);
            var Copylblline0b0dd5f134fd540 = new kony.ui.Label({
                "height": "2%",
                "id": "Copylblline0b0dd5f134fd540",
                "isVisible": true,
                "left": "7%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAgentLeft = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "id": "lblAgentLeft",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Kim Conners left the conersation",
                "textStyle": {},
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblLine0f97c0f739ca945 = new kony.ui.Label({
                "height": "2%",
                "id": "CopylblLine0f97c0f739ca945",
                "isVisible": true,
                "left": "76%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxKimLeft.add(Copylblline0b0dd5f134fd540, lblAgentLeft, CopylblLine0f97c0f739ca945);
            var flxJeanJoinedConvo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "8%",
                "id": "flxJeanJoinedConvo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "top": "1%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxJeanJoinedConvo.setDefaultUnit(kony.flex.DP);
            var Copylblline0f989db597ffc43 = new kony.ui.Label({
                "height": "2%",
                "id": "Copylblline0f989db597ffc43",
                "isVisible": true,
                "left": "7%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAgentJoined = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "id": "lblAgentJoined",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Jean Marcus Joined the conversation",
                "textStyle": {},
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblLine0f4cf13fd2e4848 = new kony.ui.Label({
                "height": "2%",
                "id": "CopylblLine0f4cf13fd2e4848",
                "isVisible": true,
                "left": "76%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "50%",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxJeanJoinedConvo.add(Copylblline0f989db597ffc43, lblAgentJoined, CopylblLine0f4cf13fd2e4848);
            var flxJeanProfile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "22%",
                "id": "flxJeanProfile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "sknflxGreyBorder",
                "top": "1%",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxJeanProfile.setDefaultUnit(kony.flex.DP);
            var imgJoinednew = new kony.ui.Image2({
                "centerY": "49%",
                "height": "50%",
                "id": "imgJoinednew",
                "isVisible": true,
                "left": "18dp",
                "src": "agent1_1.png",
                "top": "12dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgentName = new kony.ui.Label({
                "centerX": "34%",
                "id": "lblAgentName",
                "isVisible": true,
                "skin": "sknlblname",
                "text": "Jean Marcus",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAgentFav = new kony.ui.Label({
                "centerX": "52%",
                "id": "lblAgentFav",
                "isVisible": true,
                "skin": "CopyCopyCopydefLabel1",
                "text": "Traveller ✈  Foodie 🍕 Love my kids 👨‍👩‍👧‍👦",
                "textStyle": {},
                "top": "51%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxJeanProfile.add(imgJoinednew, lblAgentName, lblAgentFav);
            var flxJeanChat1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25%",
                "id": "flxJeanChat1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4%",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxJeanChat1.setDefaultUnit(kony.flex.DP);
            var imgJoinedChat = new kony.ui.Image2({
                "height": "45%",
                "id": "imgJoinedChat",
                "isVisible": true,
                "left": "0%",
                "skin": "slImage",
                "src": "agent1_1.png",
                "top": "34%",
                "width": "25%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgentName1 = new kony.ui.Label({
                "centerX": "45%",
                "centerY": "7%",
                "id": "lblAgentName1",
                "isVisible": true,
                "left": "148dp",
                "skin": "sknLblGey",
                "text": "Jean Marcus ",
                "textStyle": {},
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyflxChat0d552fa9a794349 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70%",
                "id": "CopyflxChat0d552fa9a794349",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "flxChatgrey",
                "top": "19%",
                "width": "79%",
                "zIndex": 1
            }, {}, {});
            CopyflxChat0d552fa9a794349.setDefaultUnit(kony.flex.DP);
            var CopylblChat0a0b13c0a38054d = new kony.ui.Label({
                "id": "CopylblChat0a0b13c0a38054d",
                "isVisible": true,
                "left": "3%",
                "skin": "SknLBlGreyChat",
                "text": "Hi Dan! It looks like you're interested in opening a new account! How can i help you?",
                "textStyle": {},
                "top": "2%",
                "width": "88%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyflxChat0d552fa9a794349.add(CopylblChat0a0b13c0a38054d);
            flxJeanChat1.add(imgJoinedChat, lblAgentName1, CopyflxChat0d552fa9a794349);
            flxJeanChat.add(flxKimLeft, flxJeanJoinedConvo, flxJeanProfile, flxJeanChat1);
            flxScroll.add(flxBelow, flxJeanChat);
            flxMAin.add(flxScroll);
            var lblLine = new kony.ui.Label({
                "height": "0.50%",
                "id": "lblLine",
                "isVisible": false,
                "left": "0%",
                "skin": "sknLblLineBlack",
                "textStyle": {},
                "top": "90%",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxTextMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxTextMsg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-3%",
                "isModalContainer": false,
                "skin": "sknflxWhite",
                "width": "110%",
                "zIndex": 1
            }, {}, {});
            flxTextMsg.setDefaultUnit(kony.flex.DP);
            var txtbxMsg = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "45%",
                "focusSkin": "skntxtBxGrey",
                "height": "50%",
                "id": "txtbxMsg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "9%",
                "placeholder": "Type your message here...",
                "secureTextEntry": false,
                "skin": "skntxtBxGrey",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "60%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknPHgrey",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblSend = new kony.ui.Label({
                "centerY": "46%",
                "id": "lblSend",
                "isVisible": true,
                "left": "73%",
                "skin": "CopydefLabel0a0285393b04544",
                "text": "Send",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTextMsg.add(txtbxMsg, lblSend);
            var flxGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "115.56%",
                "id": "flxGrey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknFlxGrey",
                "top": "-5.99%",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxGrey.setDefaultUnit(kony.flex.DP);
            var flxMatter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "30%",
                "id": "flxMatter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknflxWhitebgRounded",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxMatter.setDefaultUnit(kony.flex.DP);
            var flxConvoDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "24%",
                "id": "flxConvoDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a438b4c2a2a344",
                "top": "-1%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxConvoDetails.setDefaultUnit(kony.flex.DP);
            var CopylblName0a9e8a44840a64c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblName0a9e8a44840a64c",
                "isVisible": true,
                "left": "5%",
                "skin": "CopysknLblBlackName0ee9279d9233d42",
                "text": "Conversation Details",
                "textStyle": {},
                "top": "10dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Image0f1bd344cca184f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "35%",
                "id": "Image0f1bd344cca184f",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_g27593d7408f4d9da68c54a9f74ac475,
                "right": "5%",
                "skin": "slImage",
                "src": "closeicon.png",
                "top": "556dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxConvoDetails.add(CopylblName0a9e8a44840a64c, Image0f1bd344cca184f);
            var flxKim = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "24%",
                "id": "flxKim",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "sknFlxNoBorder",
                "top": "0.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKim.setDefaultUnit(kony.flex.DP);
            var lblChoosename = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChoosename",
                "isVisible": true,
                "left": "22%",
                "skin": "CopyCopydefLabel3",
                "text": "Kim Connors",
                "textStyle": {},
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgProfileGrey = new kony.ui.Image2({
                "centerY": "50%",
                "height": "38%",
                "id": "imgProfileGrey",
                "isVisible": true,
                "right": "5%",
                "skin": "slImage",
                "src": "phoneicon.png",
                "top": "556dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgNxtProfile = new kony.ui.Image2({
                "centerY": "50%",
                "height": "70%",
                "id": "imgNxtProfile",
                "isVisible": true,
                "left": "2%",
                "skin": "slImage",
                "src": "agent_1.png",
                "top": "556dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxKim.add(lblChoosename, imgProfileGrey, imgNxtProfile);
            var flxChoosePrimary = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "24%",
                "id": "flxChoosePrimary",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_baf6aa140c084d69b6b4fb095a8bfa13,
                "skin": "CopyslFbox0a438b4c2a2a344",
                "top": "0.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChoosePrimary.setDefaultUnit(kony.flex.DP);
            var Copylbl0jbea42e929b94e = new kony.ui.Label({
                "centerY": "50%",
                "id": "Copylbl0jbea42e929b94e",
                "isVisible": true,
                "left": "22%",
                "skin": "CopyCopydefLabel3",
                "text": "Choose a new primary agent",
                "textStyle": {},
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0c359a33d31e04b = new kony.ui.Image2({
                "centerY": "50%",
                "height": "70%",
                "id": "CopyImage0c359a33d31e04b",
                "isVisible": true,
                "left": "2%",
                "skin": "slImage",
                "src": "primaryagent.png",
                "top": "556dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChoosePrimary.add(Copylbl0jbea42e929b94e, CopyImage0c359a33d31e04b);
            flxMatter.add(flxConvoDetails, flxKim, flxChoosePrimary);
            flxGrey.add(flxMatter);
            this.add(flxHeader, flxMAin, lblLine, flxTextMsg, flxGrey);
        };
        return [{
            "addWidgets": addWidgetsfrmConvo,
            "bounces": false,
            "enableScrolling": false,
            "enabledForIdleTimeout": false,
            "id": "frmConvo",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_c7213f103bc14a38b4eef305d86f0dab,
            "preShow": function(eventobject) {
                controller.AS_Form_c287733b202b48f3a4d56ce0d9892f11(eventobject);
            },
            "skin": "sknFrmBlueBG"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": true,
            "configureExtendTop": true,
            "configureStatusBarStyle": true,
            "extendBottom": false,
            "extendTop": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});