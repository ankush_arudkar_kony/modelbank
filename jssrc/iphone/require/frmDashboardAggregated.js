define("frmDashboardAggregated", function() {
    return function(controller) {
        function addWidgetsfrmDashboardAggregated() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0dc050ce8d47c46",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxWhite",
                "top": "-45dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var imgBg = new kony.ui.Image2({
                "height": "100%",
                "id": "imgBg",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "headerbg.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(imgBg);
            var flxContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "90%",
                "horizontalScrollIndicator": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "onScrollEnd": controller.AS_FlexScrollContainer_e249c48cb7a84dd48812ad58ae9386cd,
                "onScrollStart": controller.AS_FlexScrollContainer_a14ca97a1a8c4f88971a8b24fe38f085,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContentBg0ha8092d5921a4c",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var lblWelcome = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblWelcome",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopydefLabel0c8fedc10d00a43",
                "text": "Welcome back,",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgFilter = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40dp",
                "id": "imgFilter",
                "isVisible": false,
                "onTouchEnd": controller.AS_Image_c7d2c6b132a844ed818508646177a73c,
                "right": "20dp",
                "skin": "slImage",
                "src": "sliders.png",
                "width": "35dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWelcome.add(lblWelcome, imgFilter);
            var flxUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%"
            }, {}, {});
            flxUser.setDefaultUnit(kony.flex.DP);
            var imgLogo = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40dp",
                "id": "imgLogo",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "profile.png",
                "width": "40dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "height": "90%",
                "id": "lblUserName",
                "isVisible": true,
                "left": "70dp",
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Veronica Mathers",
                "textStyle": {},
                "top": "15dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxUser.add(imgLogo, lblUserName);
            var flxCheckingAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxCheckingAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b8850a79703c44",
                "top": "1340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxCheckingAccounts.setDefaultUnit(kony.flex.DP);
            var compSegHdrWithoutImgText = new com.demo.originbank.compSegHdrWithoutImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "compSegHdrWithoutImgText",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "95%",
                "overrides": {
                    "compSegHdrWithoutImgText": {
                        "isVisible": false
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "",
                                    "lblAccountName": "John's Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$10,456.32",
                                    "lblBankName": "Acct #5678",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "",
                                    "lblAccountName": "Personal Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23,456.77",
                                    "lblBankName": "Bank of America #5678",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "",
                                    "lblAccountName": "Sam's Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$12,345.45",
                                    "lblBankName": "Citi Bank #5678",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            compSegHdrWithoutImgText.segAccRecords.onRowClick = controller.AS_Segment_i56f7d264e1e46c08792d563b0d65350;
            var compSegHdrWithImgText = new com.demo.originbank.compSegHdrWithImgText({
                "centerX": "50%",
                "clipBounds": true,
                "height": "295dp",
                "id": "compSegHdrWithImgText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Account",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "boa.png",
                                    "lblAccountName": "John's Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$10,000.00",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Personal Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$7,965.65",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Sam's Checking",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2,419.00",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            compSegHdrWithImgText.segAccRecords.onRowClick = controller.AS_Segment_ba10c8a686f84e22a66cbc00e5765fb5;
            flxCheckingAccounts.add(compSegHdrWithoutImgText, compSegHdrWithImgText);
            var flxQuickLinks = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "120dp",
                "id": "flxQuickLinks",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1340dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxQuickLinks.setDefaultUnit(kony.flex.DP);
            var lblQuickLinks = new kony.ui.Label({
                "id": "lblQuickLinks",
                "isVisible": true,
                "left": "40dp",
                "skin": "CopydefLabel0ecdffd9bc8de41",
                "text": "Quick Links:",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxLinks = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "90dp",
                "id": "flxLinks",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFooterBg0ie06cae4889f49",
                "top": "25dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxLinks.setDefaultUnit(kony.flex.DP);
            var flxTransferFunds = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransferFunds",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_g0dac3250aac4f4b83053239c82841f5,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTransferFunds.setDefaultUnit(kony.flex.DP);
            var imgTransferFunds = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgTransferFunds",
                "isVisible": true,
                "skin": "slImage",
                "src": "transfer_fund.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTransferFunds = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblTransferFunds",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Transfer Funds",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTransferFunds.add(imgTransferFunds, lblTransferFunds);
            var flxRequestMoney = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRequestMoney",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxRequestMoney.setDefaultUnit(kony.flex.DP);
            var imgRequestMoney = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgRequestMoney",
                "isVisible": true,
                "skin": "slImage",
                "src": "request_money.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRequestMoney = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblRequestMoney",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Request Money",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxRequestMoney.add(imgRequestMoney, lblRequestMoney);
            var flxDepositCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDepositCheck",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fffba09644924215bb1249fe73edff62,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxDepositCheck.setDefaultUnit(kony.flex.DP);
            var imgDepositCheck = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgDepositCheck",
                "isVisible": true,
                "skin": "slImage",
                "src": "travel_notification.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDepositCheck = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblDepositCheck",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Deposit Check",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxDepositCheck.add(imgDepositCheck, lblDepositCheck);
            var flxTravel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTravel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTravel.setDefaultUnit(kony.flex.DP);
            var imgTravel = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgTravel",
                "isVisible": true,
                "skin": "slImage",
                "src": "travel_notification.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTravel = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblTravel",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Travel Notification",
                "textStyle": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTravel.add(imgTravel, lblTravel);
            flxLinks.add(flxTransferFunds, flxRequestMoney, flxDepositCheck, flxTravel);
            flxQuickLinks.add(lblQuickLinks, flxLinks);
            var flxSavingsAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxSavingsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b8850a79703c44",
                "top": "1340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSavingsAccounts.setDefaultUnit(kony.flex.DP);
            var flxSegmentSavings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "248dp",
                "id": "flxSegmentSavings",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxSegmentSavings.setDefaultUnit(kony.flex.DP);
            var segSavingsAccount = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [
                    [{
                            "imgLogo": "imagedrag.png",
                            "lblAccount": "Savings Accounts",
                            "lbllIne": ""
                        },
                        [{
                            "imgBankLogo": "",
                            "lblAccountName": "Linda's Savings",
                            "lblAvailableBalance": "Available Balance",
                            "lblBalance": "$2,346.67",
                            "lblBankName": "Chase Bank #7878",
                            "lbllIne": ""
                        }, {
                            "imgBankLogo": "",
                            "lblAccountName": "DBX Savings",
                            "lblAvailableBalance": "Available Balance",
                            "lblBalance": "$7,867.55",
                            "lblBankName": "DBX Bank #1234",
                            "lbllIne": ""
                        }, {
                            "imgBankLogo": "",
                            "lblAccountName": "John's Checking",
                            "lblAvailableBalance": "Available Balance",
                            "lblBalance": "$8,956.44",
                            "lblBankName": "Chase Bank #4567",
                            "lbllIne": ""
                        }]
                    ]
                ],
                "groupCells": false,
                "height": "100%",
                "id": "segSavingsAccount",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAccounts",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxSegHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ededed00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccounts": "flxAccounts",
                    "flxSegHeader": "flxSegHeader",
                    "imgBankLogo": "imgBankLogo",
                    "imgLogo": "imgLogo",
                    "lblAccount": "lblAccount",
                    "lblAccountName": "lblAccountName",
                    "lblAvailableBalance": "lblAvailableBalance",
                    "lblBalance": "lblBalance",
                    "lblBankName": "lblBankName",
                    "lbllIne": "lbllIne"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_NONE,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            flxSegmentSavings.add(segSavingsAccount);
            var segSavingsAccountComp = new com.comp.segComp({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "255dp",
                "id": "segSavingsAccountComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "94%",
                "zIndex": 1,
                "overrides": {
                    "segAccounts": {
                        "data": [
                            [{
                                    "imgLogo": "",
                                    "lblAccount": "",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "",
                                    "lblAccountName": "",
                                    "lblAvailableBalance": "",
                                    "lblBalance": "",
                                    "lblBankName": "",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    },
                    "segComp": {
                        "top": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var compSegHdrWithImgText1 = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "297dp",
                "id": "compSegHdrWithImgText1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "centerY": "50%",
                        "height": "297dp",
                        "isVisible": true,
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "centerY": "50%",
                        "data": [
                            [{
                                    "imgAccountType": "savings.png",
                                    "lblAccount": "Savings Account",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Personal Savings",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$4,500.00",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "boa.png",
                                    "lblAccountName": "John's Savings",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$6,634.67",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Linda's Savings",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$4,789.00",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "245dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            compSegHdrWithImgText1.segAccRecords.onRowClick = controller.AS_Segment_aabbdb42f9524c2e8efb4adffb2981e1;
            flxSavingsAccounts.add(flxSegmentSavings, segSavingsAccountComp, compSegHdrWithImgText1);
            var flxCreditCards = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "235dp",
                "id": "flxCreditCards",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b8850a79703c44",
                "top": "1340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxCreditCards.setDefaultUnit(kony.flex.DP);
            var flxSegmentCards = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "183dp",
                "id": "flxSegmentCards",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxSegmentCards.setDefaultUnit(kony.flex.DP);
            var segCreditCards = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [
                    [{
                            "imgLogo": "imagedrag.png",
                            "lblAccount": "Credit Cards",
                            "lbllIne": ""
                        },
                        [{
                            "imgBankLogo": "",
                            "lblAccountName": "Chase Airline Rewards...",
                            "lblAvailableBalance": "Available Balance",
                            "lblBalance": "$5,025.54",
                            "lblBankName": "Chase Bank #7788",
                            "lbllIne": ""
                        }, {
                            "imgBankLogo": "",
                            "lblAccountName": "Bank of America Credit ...",
                            "lblAvailableBalance": "Available Balance",
                            "lblBalance": "$4,543.00",
                            "lblBankName": "Bank of America #3344",
                            "lbllIne": ""
                        }]
                    ]
                ],
                "groupCells": false,
                "height": "100%",
                "id": "segCreditCards",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAccounts",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxSegHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ededed00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccounts": "flxAccounts",
                    "flxSegHeader": "flxSegHeader",
                    "imgBankLogo": "imgBankLogo",
                    "imgLogo": "imgLogo",
                    "lblAccount": "lblAccount",
                    "lblAccountName": "lblAccountName",
                    "lblAvailableBalance": "lblAvailableBalance",
                    "lblBalance": "lblBalance",
                    "lblBankName": "lblBankName",
                    "lbllIne": "lbllIne"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_NONE,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            flxSegmentCards.add(segCreditCards);
            var segCreditCardsComp = new com.comp.segComp({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "190dp",
                "id": "segCreditCardsComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "94%",
                "zIndex": 1,
                "overrides": {
                    "segAccounts": {
                        "data": [
                            [{
                                    "imgLogo": "",
                                    "lblAccount": "",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "",
                                    "lblAccountName": "",
                                    "lblAvailableBalance": "",
                                    "lblBalance": "",
                                    "lblBankName": "",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    },
                    "segComp": {
                        "top": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var compSegHdrWithImgText2 = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "210dp",
                "id": "compSegHdrWithImgText2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "centerY": "50%",
                        "height": "210dp",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "centerY": "50%",
                        "data": [
                            [{
                                    "imgAccountType": "credit_card.png",
                                    "lblAccount": "Credit Card",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "boa.png",
                                    "lblAccountName": "Sam's Credit",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2,419.64",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Linda's Credit",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$4,838.32",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "180dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            compSegHdrWithImgText2.segAccRecords.onRowClick = controller.AS_Segment_fa6c63eed15b4f34900644f0b2860c3b;
            flxCreditCards.add(flxSegmentCards, segCreditCardsComp, compSegHdrWithImgText2);
            var flxChart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "190dp",
                "id": "flxChart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0bed329b10ed04b",
                "top": "1340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxChart.setDefaultUnit(kony.flex.DP);
            var flxBarChart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "95%",
                "id": "flxBarChart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyslFbox0e47a7eefa00c4c",
                "top": "0dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxBarChart.setDefaultUnit(kony.flex.DP);
            var FlexGroup0g4a553a3865543 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "85%",
                "id": "FlexGroup0g4a553a3865543",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%"
            }, {}, {});
            FlexGroup0g4a553a3865543.setDefaultUnit(kony.flex.DP);
            var imgChart = new kony.ui.Image2({
                "centerX": "50%",
                "height": "80dp",
                "id": "imgChart",
                "isVisible": false,
                "left": "53dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "10dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAmount",
                "isVisible": true,
                "skin": "CopydefLabel0jd8496e2d7274f",
                "text": "$65,429.20",
                "textStyle": {},
                "top": "97dp",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblTotalAccount = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblTotalAccount",
                "isVisible": true,
                "skin": "CopydefLabel0h9826445240d48",
                "text": "TOTAL ACCOUNT BALANCE",
                "textStyle": {},
                "top": "125dp",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var verticalbar = new com.konymp.verticalbar({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "73.91%",
                "id": "verticalbar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-14%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "top": "15dp",
                "width": "118.95%",
                "overrides": {
                    "verticalbar": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            verticalbar.enableGrid = true;
            verticalbar.chartData = {
                "data": [{
                    "colorCode": "#001746",
                    "label": " ",
                    "value": "25"
                }, {
                    "colorCode": "#001746",
                    "label": "   ",
                    "value": "20"
                }, {
                    "colorCode": "#001746",
                    "label": "  ",
                    "value": "10"
                }, {
                    "colorCode": "#001746",
                    "label": "  ",
                    "value": "30"
                }, {
                    "colorCode": "#7F8AA2",
                    "label": "  ",
                    "value": "40"
                }, {
                    "colorCode": "#7F8AA2",
                    "label": "  ",
                    "value": "35"
                }, {
                    "colorCode": "#001746",
                    "label": "",
                    "value": "36"
                }, {
                    "colorCode": "#001746",
                    "label": " ",
                    "value": "24"
                }, {
                    "colorCode": "#001746",
                    "label": " ",
                    "value": "15"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "adb02a6b13a046f4861d9d64ef0c34ee"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f7ad02b6f3df4493bb1ef588ab70e008"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "b44c0b92196a4fefb857530ee6a68947"
                }]
            };
            verticalbar.chartTitle = "Vertical Bar";
            verticalbar.xAxisTitle = " ";
            verticalbar.lowValue = "0";
            verticalbar.enableGridAnimation = true;
            verticalbar.titleFontSize = "0";
            verticalbar.yAxisTitle = " ";
            verticalbar.titleFontColor = "#000000";
            verticalbar.highValue = "40";
            verticalbar.bgColor = "#FFFFFF";
            verticalbar.enableChartAnimation = true;
            verticalbar.enableStaticPreview = true;
            var lblAmountTotal = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAmountTotal",
                "isVisible": false,
                "left": "38dp",
                "skin": "CopydefLabel0e0df699e8c9c4e",
                "text": "$ 65,429.00",
                "textStyle": {},
                "top": "125dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountTotal0he5f2129122a43 = new kony.ui.Label({
                "centerX": "50%",
                "id": "CopylblAmountTotal0he5f2129122a43",
                "isVisible": false,
                "left": "38dp",
                "skin": "CopydefLabel0e0df699e8c9c4e",
                "text": "Total Account Balance",
                "textStyle": {},
                "top": "145dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexGroup0g4a553a3865543.add(imgChart, lblAmount, lblTotalAccount, verticalbar, lblAmountTotal, CopylblAmountTotal0he5f2129122a43);
            flxBarChart.add(FlexGroup0g4a553a3865543);
            var flxFicoScore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "95%",
                "id": "flxFicoScore",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "right": "3%",
                "skin": "CopyslFbox0dbe9bb4d3aef41",
                "top": "0dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxFicoScore.setDefaultUnit(kony.flex.DP);
            var FlexGroup0ce6f4b4493ff47 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "85%",
                "id": "FlexGroup0ce6f4b4493ff47",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%"
            }, {}, {});
            FlexGroup0ce6f4b4493ff47.setDefaultUnit(kony.flex.DP);
            var lblNumber = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "49.95%",
                "height": "20%",
                "id": "lblNumber",
                "isVisible": true,
                "skin": "CopydefLabel0a748a2fe62ca49",
                "text": "705",
                "textStyle": {},
                "top": "45dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblGood = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblGood",
                "isVisible": true,
                "skin": "CopydefLabel0f262a202c5b348",
                "text": "Good",
                "textStyle": {},
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFico = new kony.ui.Label({
                "bottom": "10dp",
                "centerX": "50%",
                "id": "lblFico",
                "isVisible": true,
                "skin": "CopydefLabel0e1fc0432261d4f",
                "text": "FICO SCORE",
                "textStyle": {},
                "width": "100%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var donutchart = new com.konymp.donutchart({
                "centerX": "50%",
                "clipBounds": true,
                "height": "85%",
                "id": "donutchart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0dbe9bb4d3aef41",
                "top": "0dp",
                "width": "120%",
                "overrides": {
                    "donutchart": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart.enableLegend = false;
            donutchart.bgColor = "#ffffff";
            donutchart.chartData = {
                "data": [{
                    "colorCode": "#e02020",
                    "label": "Data1",
                    "value": "21"
                }, {
                    "colorCode": "#f7b500",
                    "label": "Data2",
                    "value": "21"
                }, {
                    "colorCode": "#6dd400",
                    "label": "Data3",
                    "value": "15"
                }, {
                    "colorCode": "#e7e7e7",
                    "label": "Data4",
                    "value": "25"
                }, {
                    "colorCode": "#ffffff",
                    "label": "Data5",
                    "value": "25"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart.titleFontSize = "12";
            donutchart.enableChartAnimation = true;
            donutchart.chartTitle = "Donut Chart";
            donutchart.legendFontSize = "0";
            donutchart.enableStaticPreview = true;
            donutchart.titleFontColor = "#000000";
            donutchart.legendFontColor = "#000000";
            var lbl250 = new kony.ui.Label({
                "bottom": "36dp",
                "id": "lbl250",
                "isVisible": true,
                "left": "13%",
                "skin": "CopydefLabel0c12428825abc48",
                "text": "250",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl900 = new kony.ui.Label({
                "bottom": "36dp",
                "id": "lbl900",
                "isVisible": true,
                "right": "13%",
                "skin": "CopydefLabel0c12428825abc48",
                "text": "900",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexGroup0ce6f4b4493ff47.add(lblNumber, lblGood, lblFico, donutchart, lbl250, lbl900);
            var flxDummy1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "85%",
                "id": "flxDummy1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%",
                "zIndex": 3
            }, {}, {});
            flxDummy1.setDefaultUnit(kony.flex.DP);
            flxDummy1.add();
            flxFicoScore.add(FlexGroup0ce6f4b4493ff47, flxDummy1);
            flxChart.add(flxBarChart, flxFicoScore);
            var flxMonthlyBudget = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": true,
                "height": "205dp",
                "id": "flxMonthlyBudget",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "CopyslFbox0dbe9bb4d3aef41",
                "top": "1340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMonthlyBudget.setDefaultUnit(kony.flex.DP);
            var lblMonthlyBudget = new kony.ui.Label({
                "id": "lblMonthlyBudget",
                "isVisible": true,
                "left": "7%",
                "skin": "CopydefLabel0ecdffd9bc8de41",
                "text": "Monthly Budget",
                "textStyle": {},
                "top": "31dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSpending = new kony.ui.Label({
                "id": "lblSpending",
                "isVisible": true,
                "left": "7%",
                "skin": "CopydefLabel0j4736e3c35e24d",
                "text": "Spening Trends by Category",
                "textStyle": {},
                "top": "56dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblJuly = new kony.ui.Label({
                "id": "lblJuly",
                "isVisible": true,
                "right": "7%",
                "skin": "CopydefLabel0ecdffd9bc8de41",
                "text": "JULY 2019",
                "textStyle": {},
                "top": "31dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxMonthlyCharts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "100dp",
                "id": "flxMonthlyCharts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFooterBg0ie06cae4889f49",
                "top": "71dp",
                "width": "88%",
                "zIndex": 1
            }, {}, {});
            flxMonthlyCharts.setDefaultUnit(kony.flex.DP);
            var flxEntertainment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEntertainment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxEntertainment.setDefaultUnit(kony.flex.DP);
            var lblEntertainment = new kony.ui.Label({
                "bottom": "15dp",
                "centerX": "50%",
                "id": "lblEntertainment",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Entertainment",
                "textStyle": {},
                "width": "100%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblEntertainmentExp = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblEntertainmentExp",
                "isVisible": true,
                "skin": "CopydefLabel0j4736e3c35e24d",
                "text": "$125/mth",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblEntertainmentNumber = new kony.ui.Label({
                "centerX": "51%",
                "id": "lblEntertainmentNumber",
                "isVisible": true,
                "skin": "CopydefLabel0d624ee13663541",
                "text": "45%",
                "textStyle": {},
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var donutchartCopy = new com.konymp.donutchartCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "donutchartCopy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "0dp",
                "width": "120%",
                "overrides": {
                    "donutchartCopy": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchartCopy.enableLegend = false;
            donutchartCopy.bgColor = "#ffffff";
            donutchartCopy.chartData = {
                "data": [{
                    "colorCode": "#f7b500",
                    "label": "Data1",
                    "value": "45"
                }, {
                    "colorCode": "#e7e7e7",
                    "label": "Data2",
                    "value": "55"
                }, {
                    "colorCode": "#76C044",
                    "label": "Data3",
                    "value": "0"
                }, {
                    "colorCode": "#FFC522",
                    "label": "Data4",
                    "value": "0"
                }, {
                    "colorCode": "#97cded",
                    "label": "Data5",
                    "value": "0"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchartCopy.titleFontSize = "12";
            donutchartCopy.enableChartAnimation = true;
            donutchartCopy.chartTitle = "";
            donutchartCopy.legendFontSize = "0";
            donutchartCopy.enableStaticPreview = true;
            donutchartCopy.titleFontColor = "#000000";
            donutchartCopy.legendFontColor = "#000000";
            flxEntertainment.add(lblEntertainment, lblEntertainmentExp, lblEntertainmentNumber, donutchartCopy);
            var flxRestaurants = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRestaurants",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxRestaurants.setDefaultUnit(kony.flex.DP);
            var lblRestaurants = new kony.ui.Label({
                "bottom": "15dp",
                "centerX": "50%",
                "id": "lblRestaurants",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Restaurants",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblRestaurantsExp = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblRestaurantsExp",
                "isVisible": true,
                "skin": "CopydefLabel0j4736e3c35e24d",
                "text": "$75/mth",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblRestaurantsNumber = new kony.ui.Label({
                "centerX": "51%",
                "id": "lblRestaurantsNumber",
                "isVisible": true,
                "skin": "CopydefLabel0e7546846e62d4e",
                "text": "65%",
                "textStyle": {},
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var donutchart3 = new com.konymp.donutchart3({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "donutchart3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "0dp",
                "width": "120%",
                "overrides": {
                    "donutchart3": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart3.enableLegend = false;
            donutchart3.bgColor = "#ffffff";
            donutchart3.chartData = {
                "data": [{
                    "colorCode": "#fa6400",
                    "label": "Data1",
                    "value": "65"
                }, {
                    "colorCode": "#e7e7e7",
                    "label": "Data2",
                    "value": "35"
                }, {
                    "colorCode": "#76C044",
                    "label": "Data3",
                    "value": "0"
                }, {
                    "colorCode": "#FFC522",
                    "label": "Data4",
                    "value": "0"
                }, {
                    "colorCode": "#97cded",
                    "label": "Data5",
                    "value": "0"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart3.titleFontSize = "12";
            donutchart3.enableChartAnimation = true;
            donutchart3.chartTitle = "";
            donutchart3.legendFontSize = "8";
            donutchart3.enableStaticPreview = true;
            donutchart3.titleFontColor = "#000000";
            donutchart3.legendFontColor = "#000000";
            flxRestaurants.add(lblRestaurants, lblRestaurantsExp, lblRestaurantsNumber, donutchart3);
            var flxGroceries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroceries",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxGroceries.setDefaultUnit(kony.flex.DP);
            var lblGroceries = new kony.ui.Label({
                "bottom": "15dp",
                "centerX": "50%",
                "id": "lblGroceries",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Groceries",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblGroceriesExp = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblGroceriesExp",
                "isVisible": true,
                "skin": "CopydefLabel0j4736e3c35e24d",
                "text": "$200/mth",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblGroceriesNumber = new kony.ui.Label({
                "centerX": "51%",
                "id": "lblGroceriesNumber",
                "isVisible": true,
                "skin": "CopydefLabel0e7546846e62d4e",
                "text": "53%",
                "textStyle": {},
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var donutchart4 = new com.konymp.donutchart4({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "donutchart4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "0dp",
                "width": "120%",
                "overrides": {
                    "donutchart4": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart4.enableLegend = false;
            donutchart4.bgColor = "#ffffff";
            donutchart4.chartData = {
                "data": [{
                    "colorCode": "#f7b500",
                    "label": "Data1",
                    "value": "53"
                }, {
                    "colorCode": "#e7e7e7",
                    "label": "Data2",
                    "value": "47"
                }, {
                    "colorCode": "#76C044",
                    "label": "Data3",
                    "value": "0"
                }, {
                    "colorCode": "#FFC522",
                    "label": "Data4",
                    "value": "0"
                }, {
                    "colorCode": "#97cded",
                    "label": "Data5",
                    "value": "0"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart4.titleFontSize = "12";
            donutchart4.enableChartAnimation = true;
            donutchart4.chartTitle = "";
            donutchart4.legendFontSize = "0";
            donutchart4.enableStaticPreview = true;
            donutchart4.titleFontColor = "#000000";
            donutchart4.legendFontColor = "#000000";
            flxGroceries.add(lblGroceries, lblGroceriesExp, lblGroceriesNumber, donutchart4);
            var flxTravels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTravels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTravels.setDefaultUnit(kony.flex.DP);
            var lblTravels = new kony.ui.Label({
                "bottom": "15dp",
                "centerX": "50%",
                "id": "lblTravels",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Travel",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblTravelExp = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblTravelExp",
                "isVisible": true,
                "skin": "CopydefLabel0j4736e3c35e24d",
                "text": "$100/mth",
                "textStyle": {},
                "width": "95%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblTravelNumber = new kony.ui.Label({
                "centerX": "51%",
                "id": "lblTravelNumber",
                "isVisible": true,
                "skin": "CopydefLabel0e7546846e62d4e",
                "text": "25%",
                "textStyle": {},
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var donutchart5 = new com.konymp.donutchart5({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "donutchart5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "0dp",
                "width": "120%",
                "overrides": {
                    "donutchart5": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart5.enableLegend = false;
            donutchart5.bgColor = "#ffffff";
            donutchart5.chartData = {
                "data": [{
                    "colorCode": "#6dd400",
                    "label": "Data1",
                    "value": "25"
                }, {
                    "colorCode": "#e7e7e7",
                    "label": "Data2",
                    "value": "75"
                }, {
                    "colorCode": "#76C044",
                    "label": "Data3",
                    "value": "0"
                }, {
                    "colorCode": "#FFC522",
                    "label": "Data4",
                    "value": "0"
                }, {
                    "colorCode": "#97cded",
                    "label": "Data5",
                    "value": "0"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart5.titleFontSize = "12";
            donutchart5.enableChartAnimation = true;
            donutchart5.chartTitle = "";
            donutchart5.legendFontSize = "0";
            donutchart5.enableStaticPreview = true;
            donutchart5.titleFontColor = "#000000";
            donutchart5.legendFontColor = "#000000";
            flxTravels.add(lblTravels, lblTravelExp, lblTravelNumber, donutchart5);
            flxMonthlyCharts.add(flxEntertainment, flxRestaurants, flxGroceries, flxTravels);
            var flxDummy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxDummy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "71dp",
                "width": "88%",
                "zIndex": 3
            }, {}, {});
            flxDummy.setDefaultUnit(kony.flex.DP);
            flxDummy.add();
            flxMonthlyBudget.add(lblMonthlyBudget, lblSpending, lblJuly, flxMonthlyCharts, flxDummy);
            var FlexGroup0d6ab82fddd0448 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexGroup0d6ab82fddd0448",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1340dp",
                "width": "100%"
            }, {}, {});
            FlexGroup0d6ab82fddd0448.setDefaultUnit(kony.flex.DP);
            FlexGroup0d6ab82fddd0448.add();
            flxContent.add(flxWelcome, flxUser, flxCheckingAccounts, flxQuickLinks, flxSavingsAccounts, flxCreditCards, flxChart, flxMonthlyBudget, FlexGroup0d6ab82fddd0448);
            var flxFooter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "65dp",
                "id": "flxFooter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFooterBg0a1c9ed972ab54f",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxFooter.setDefaultUnit(kony.flex.DP);
            var flxAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAccounts.setDefaultUnit(kony.flex.DP);
            var imgAccount = new kony.ui.Image2({
                "centerX": "50%",
                "height": "50dp",
                "id": "imgAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "icon_01.png",
                "top": "5dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccount = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblAccount",
                "isVisible": true,
                "skin": "CopydefLabel0f19088f695b640",
                "text": "Accounts",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxAccounts.add(imgAccount, lblAccount);
            var flxTransfer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransfer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b0c87070a89245b28db29d679ab5d35f,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTransfer.setDefaultUnit(kony.flex.DP);
            var imgTransfer = new kony.ui.Image2({
                "centerX": "50%",
                "height": "50dp",
                "id": "imgTransfer",
                "isVisible": true,
                "skin": "slImage",
                "src": "transfer_inactive.png",
                "top": "5dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTransfer = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblTransfer",
                "isVisible": true,
                "skin": "CopydefLabel0b667d2cb3a924b",
                "text": "Transfer",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTransfer.add(imgTransfer, lblTransfer);
            var flxBillPay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBillPay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxBillPay.setDefaultUnit(kony.flex.DP);
            var imgBillPay = new kony.ui.Image2({
                "centerX": "50%",
                "height": "50dp",
                "id": "imgBillPay",
                "isVisible": true,
                "skin": "slImage",
                "src": "billpay_inactive.png",
                "top": "5dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBillPay = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblBillPay",
                "isVisible": true,
                "skin": "CopydefLabel0b667d2cb3a924b",
                "text": "Bill Pay",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxBillPay.add(imgBillPay, lblBillPay);
            var flxMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMore",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e10828c230cd4e0f98901d966cf2b52d,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxMore.setDefaultUnit(kony.flex.DP);
            var imgMore = new kony.ui.Image2({
                "centerX": "50%",
                "height": "50dp",
                "id": "imgMore",
                "isVisible": true,
                "skin": "slImage",
                "src": "more_inactive.png",
                "top": "5dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMore = new kony.ui.Label({
                "bottom": "0dp",
                "centerX": "50%",
                "id": "lblMore",
                "isVisible": true,
                "skin": "CopydefLabel0b667d2cb3a924b",
                "text": "More",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxMore.add(imgMore, lblMore);
            flxFooter.add(flxAccounts, flxTransfer, flxBillPay, flxMore);
            var flxHamburger = new kony.ui.FlexContainer({
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxHamburger",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-100%",
                "isModalContainer": false,
                "skin": "sknFlxHamburger",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxHamburger.setDefaultUnit(kony.flex.DP);
            var flxTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20%",
                "id": "flxTop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTop.setDefaultUnit(kony.flex.DP);
            var imgClose = new kony.ui.Image2({
                "height": "25dp",
                "id": "imgClose",
                "isVisible": false,
                "onTouchEnd": controller.AS_Image_b208e4fcce68443bae917d4797623900,
                "right": "20dp",
                "skin": "slImage",
                "src": "back.png",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgProfile = new kony.ui.Image2({
                "height": "45dp",
                "id": "imgProfile",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "profile.png",
                "top": "5%",
                "width": "45dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0j8b94ad343be4e",
                "text": "Veronica Mathers",
                "textStyle": {},
                "top": "10dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblGmail = new kony.ui.Label({
                "id": "lblGmail",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0d03efd18b60e49",
                "text": "veronicamathers@gmail.com",
                "textStyle": {},
                "top": "5dp",
                "width": "350dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTop.add(imgClose, imgProfile, lblName, lblGmail);
            var flxMenu = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "73%",
                "horizontalScrollIndicator": true,
                "id": "flxMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlWHiteBG",
                "top": "20%",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMenu.setDefaultUnit(kony.flex.DP);
            var flxEngage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxEngage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e1f6f48cb18f4e4199137ff469af20ce,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEngage.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0de93994cc50840 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0de93994cc50840",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_engage.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0e7e07d733b5348 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0e7e07d733b5348",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0bc2190b0e01f43 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0bc2190b0e01f43",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Engage",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxEngage.add(CopyimgArrow0de93994cc50840, CopylblAmount0e7e07d733b5348, CopylblAmountValue0bc2190b0e01f43);
            var flxManageMyAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxManageMyAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxManageMyAccounts.setDefaultUnit(kony.flex.DP);
            var imgArrow = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "imgArrow",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_5.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0gaf25d7cb04747 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0gaf25d7cb04747",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmountValue = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "lblAmountValue",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Manage My Other Accounts",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxManageMyAccounts.add(imgArrow, CopylblAmount0gaf25d7cb04747, lblAmountValue);
            var flxCardMAnagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxCardMAnagement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d44f7efde5ce4e53a1898b6883157157,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCardMAnagement.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0j343c2a352574b = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0j343c2a352574b",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_4.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0ec91843e07f545 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0ec91843e07f545",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0ibca879110714d = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0ibca879110714d",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Card Management",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxCardMAnagement.add(CopyimgArrow0j343c2a352574b, CopylblAmount0ec91843e07f545, CopylblAmountValue0ibca879110714d);
            var flxOpenNewAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxOpenNewAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_hf863925236149158baffc1c70f73afa,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxOpenNewAccount.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0a7387952cb1848 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0a7387952cb1848",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_2.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0f6af9c85c6ed4e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0f6af9c85c6ed4e",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0fe0af7d9763740 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0fe0af7d9763740",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Open a New Account ",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxOpenNewAccount.add(CopyimgArrow0a7387952cb1848, CopylblAmount0f6af9c85c6ed4e, CopylblAmountValue0fe0af7d9763740);
            var flxCheckDeposits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxCheckDeposits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCheckDeposits.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0g1879e3a952a4f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0g1879e3a952a4f",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_3.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0eff82473f5f844 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0eff82473f5f844",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0b06c321f046a48 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0b06c321f046a48",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Check Deposits",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxCheckDeposits.add(CopyimgArrow0g1879e3a952a4f, CopylblAmount0eff82473f5f844, CopylblAmountValue0b06c321f046a48);
            var flxCardlessCash = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxCardlessCash",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCardlessCash.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0i1c67e51e2944e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0i1c67e51e2944e",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_7.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0da0560ab324d46 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0da0560ab324d46",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0hd36f85ec0fd44 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0hd36f85ec0fd44",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Card less Cash",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxCardlessCash.add(CopyimgArrow0i1c67e51e2944e, CopylblAmount0da0560ab324d46, CopylblAmountValue0hd36f85ec0fd44);
            var flxPersonalFinance = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxPersonalFinance",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_df5d5e249edc480fa2af535b5a284f3b,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPersonalFinance.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0d1d05adde9b647 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0d1d05adde9b647",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_9.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0b343a204550f4d = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0b343a204550f4d",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0d92da0befaf24f = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0d92da0befaf24f",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Personal Finance",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxPersonalFinance.add(CopyimgArrow0d1d05adde9b647, CopylblAmount0b343a204550f4d, CopylblAmountValue0d92da0befaf24f);
            var flxMessages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxMessages",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxMessages.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0ad8b654cb4bd49 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0ad8b654cb4bd49",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_12.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0ce997aa6acd643 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0ce997aa6acd643",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0gaff7e4c6d9d43 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0gaff7e4c6d9d43",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Messages",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxMessages.add(CopyimgArrow0ad8b654cb4bd49, CopylblAmount0ce997aa6acd643, CopylblAmountValue0gaff7e4c6d9d43);
            var flxChatbot = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxChatbot",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxChatbot.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0d6cc6b4c8a9c4c = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0d6cc6b4c8a9c4c",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_1.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0f608d034113448 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0f608d034113448",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0bba1f3e978e042 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0bba1f3e978e042",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Chatbot",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxChatbot.add(CopyimgArrow0d6cc6b4c8a9c4c, CopylblAmount0f608d034113448, CopylblAmountValue0bba1f3e978e042);
            var flxSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "9%",
                "id": "flxSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSettings.setDefaultUnit(kony.flex.DP);
            var CopyimgArrow0c8bffe63c33d44 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "25dp",
                "id": "CopyimgArrow0c8bffe63c33d44",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "slImage",
                "src": "icon_16.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblAmount0d7fe98f881554e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblAmount0d7fe98f881554e",
                "isVisible": false,
                "left": "15dp",
                "skin": "CopysknlblBankName0f2b73fbdf78643",
                "text": "Amount",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblAmountValue0ec534edac66844 = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopylblAmountValue0ec534edac66844",
                "isVisible": true,
                "left": "55dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0b9e98f66471f43",
                "text": "Settings",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxSettings.add(CopyimgArrow0c8bffe63c33d44, CopylblAmount0d7fe98f881554e, CopylblAmountValue0ec534edac66844);
            var lblLocateUs = new kony.ui.Label({
                "height": "9%",
                "id": "lblLocateUs",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0g3ccde12dafc48",
                "text": "Locate Us",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFeedback = new kony.ui.Label({
                "height": "9%",
                "id": "lblFeedback",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0g3ccde12dafc48",
                "text": "Feedback",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSupport = new kony.ui.Label({
                "height": "9%",
                "id": "lblSupport",
                "isVisible": true,
                "left": "15dp",
                "right": "55dp",
                "skin": "CopysknlblAccountName0g3ccde12dafc48",
                "text": "Support",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSignout = new kony.ui.Label({
                "bottom": "20dp",
                "height": "9%",
                "id": "lblSignout",
                "isVisible": true,
                "left": "15dp",
                "onTouchEnd": controller.AS_Label_b5943620bd4c4e9ba45fa61c9a2561d6,
                "right": "55dp",
                "skin": "CopysknlblAccountName0g3ccde12dafc48",
                "text": "Sign Out",
                "textStyle": {},
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxMenu.add(flxEngage, flxManageMyAccounts, flxCardMAnagement, flxOpenNewAccount, flxCheckDeposits, flxCardlessCash, flxPersonalFinance, flxMessages, flxChatbot, flxSettings, lblLocateUs, lblFeedback, lblSupport, lblSignout);
            flxHamburger.add(flxTop, flxMenu);
            flxMain.add(flxHeader, flxContent, flxFooter, flxHamburger);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmDashboardAggregated,
            "bounces": false,
            "enableScrolling": false,
            "enabledForIdleTimeout": false,
            "id": "frmDashboardAggregated",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_abc440d05fca42f6af23509995673adb,
            "preShow": function(eventobject) {
                controller.AS_Form_h50aebee09fc41c5a01a08eca3c6e09d(eventobject);
            },
            "skin": "sknFrmBgWhitef8f8f8"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});