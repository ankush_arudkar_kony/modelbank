define("frmTransferFrom", function() {
    return function(controller) {
        function addWidgetsfrmTransferFrom() {
            this.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_ge606460c8d14e289c1699b7909e0338,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_ff17ef94b2d6430da622547313bd63b6,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "From",
                "textStyle": {},
                "top": "0dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel, lblPersonalChecking);
            var flxScrlContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": false,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrlContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlWHiteBG",
                "top": "40dp",
                "verticalScrollIndicator": false,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrlContent.setDefaultUnit(kony.flex.DP);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var CopylblPersonalChecking0j74bf47189c841 = new kony.ui.Label({
                "id": "CopylblPersonalChecking0j74bf47189c841",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBlack95N",
                "text": "Select an account to transfer from",
                "textStyle": {},
                "top": "2dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "search_02.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAccountNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTbxFocus",
                "height": "100%",
                "id": "txtAccountNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "placeholder": "Search by account name or number",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTbxPlaceholder",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtAccountNumber);
            flxWelcome.add(CopylblPersonalChecking0j74bf47189c841, flxSearch);
            var flxPendingTransactions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxPendingTransactions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxPendingTransactions.setDefaultUnit(kony.flex.DP);
            var flxCheckingAccounts = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckingAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "height": "100%",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "origin_24x24.png",
                                    "lblAccountName": "Edison's Checking....x5231",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$7263.98",
                                    "lblBankName": "Temenos Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "acc_detailspic3.png",
                                    "lblAccountName": "John's Checking....x5678",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23,456.77",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Personal Checking....x4567",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$10,456.32",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Sam's Checking....x9156",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$12,345.45",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "310dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCheckingAccounts.segAccRecords.onRowClick = controller.AS_Segment_e5e08f19759a46dc9c902742c1d07924;
            flxPendingTransactions.add(flxCheckingAccounts);
            var flxSavingsAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxSavingsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-20dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSavingsAccounts.setDefaultUnit(kony.flex.DP);
            var flxSavingsAcounts = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSavingsAcounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "height": "100%",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "savings.png",
                                    "lblAccount": "Savings Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "origin_24x24.png",
                                    "lblAccountName": "Robert's Savings....x9812",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$9234.34",
                                    "lblBankName": "Temenos Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Linda's Savings....x7878",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2,346.67",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Personal Savings....x9651",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$7,867.55",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "John's Savings....x6711",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$8,956.44",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "310dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSavingsAcounts.segAccRecords.onRowClick = controller.AS_Segment_cc896831a15c4ebb9fa4ebe074703a2a;
            flxSavingsAccounts.add(flxSavingsAcounts);
            flxScrlContent.add(flxWelcome, flxPendingTransactions, flxSavingsAccounts);
            this.add(FlexContainer0eed5fe5dc1064e, flxScrlContent);
        };
        return [{
            "addWidgets": addWidgetsfrmTransferFrom,
            "enabledForIdleTimeout": false,
            "id": "frmTransferFrom",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "sknFrmBgWhitef8f8f8"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});