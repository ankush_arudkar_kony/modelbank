define("userfrmPPReviewAndSendController", {
    //Type your controller code here 
    preShowFun: function() {
        this.view.lblAmount.text = gblEnteredPPAmt;
        this.view.lblAccountId.text = gblSelectedPPID;
        this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
        this.view.lblAvailableBalance.text = gblSelectedFromAccountRow.lblBalance;
    }
});
define("frmPPReviewAndSendControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_hc0e0db770b84cb783534edf953877ac: function AS_FlexContainer_hc0e0db770b84cb783534edf953877ac(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmSendPPPayments");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_a9942efe15264f53b66dd0dfae6fefa8: function AS_FlexContainer_a9942efe15264f53b66dd0dfae6fefa8(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for bnTransfer **/
    AS_Button_df46860a9b594ae4b5954f1e526ed24f: function AS_Button_df46860a9b594ae4b5954f1e526ed24f(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPPSendSuccess");
        ntf.navigate();
    },
    /** preShow defined for frmPPReviewAndSend **/
    AS_Form_gd2c5d1d0cb44dc584b6c69596269f97: function AS_Form_gd2c5d1d0cb44dc584b6c69596269f97(eventobject) {
        var self = this;
        return self.preShowFun.call(this);
    }
});
define("frmPPReviewAndSendController", ["userfrmPPReviewAndSendController", "frmPPReviewAndSendControllerActions"], function() {
    var controller = require("userfrmPPReviewAndSendController");
    var controllerActions = ["frmPPReviewAndSendControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
