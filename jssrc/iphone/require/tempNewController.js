define("usertempNewController", {
    //Type your controller code here 
    NaVToNxtFrmChat: function(indx) {
        var nav = new kony.mvc.Navigation("frmConvo");
        //var arrayTemp={name:indx.widgetInfo.selectedRowItems[0]["lblName"],profileimg:indx.widgetInfo.selectedRowItems[0]["imgagent"], traveler:indx.widgetInfo.selectedRowItems[0]["lblFav"]};
        var arrayTemp = {
            name: indx.widgetInfo.selectedItems[0]["lblName"],
            profileimg: indx.widgetInfo.selectedItems[0]["imgagent"],
            traveler: indx.widgetInfo.selectedItems[0]["lblFav"]
        };
        nav.navigate(arrayTemp);
    }
});
define("tempNewControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnGetStarted **/
    AS_Button_i49bbbfed1ae4e49a817098c823db614: function AS_Button_i49bbbfed1ae4e49a817098c823db614(eventobject, context) {
        var self = this;
        return self.NaVToNxtFrmChat.call(this, context);
    }
});
define("tempNewController", ["usertempNewController", "tempNewControllerActions"], function() {
    var controller = require("usertempNewController");
    var controllerActions = ["tempNewControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
