define("Form1", function() {
    return function(controller) {
        function addWidgetsForm1() {
            this.setDefaultUnit(kony.flex.DP);
            var donutchart = new com.konymp.donutchart({
                "clipBounds": true,
                "height": "50%",
                "id": "donutchart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0fb230441e9ea4b",
                "top": "0dp",
                "width": "80%",
                "overrides": {
                    "donutchart": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            donutchart.enableLegend = false;
            donutchart.bgColor = "#ffffff";
            donutchart.chartData = {
                "data": [{
                    "colorCode": "#e02020",
                    "label": "Data1",
                    "value": "21"
                }, {
                    "colorCode": "#f7b500",
                    "label": "Data2",
                    "value": "21"
                }, {
                    "colorCode": "#6dd400",
                    "label": "Data3",
                    "value": "15"
                }, {
                    "colorCode": "#000000",
                    "label": "Data4",
                    "value": "25"
                }, {
                    "colorCode": "#ffffff",
                    "label": "Data5",
                    "value": "25"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Value",
                    "columnHeaderType": "text",
                    "columnID": "value",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color Code",
                    "columnHeaderType": "text",
                    "columnID": "colorCode",
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f722303b71a8439eae528d9af9856f30"
                }]
            };
            donutchart.titleFontSize = "12";
            donutchart.enableChartAnimation = true;
            donutchart.chartTitle = "Donut Chart";
            donutchart.legendFontSize = "8";
            donutchart.enableStaticPreview = true;
            donutchart.titleFontColor = "#000000";
            donutchart.legendFontColor = "#000000";
            var Button0f615e3b2baca49 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "Button0f615e3b2baca49",
                "isVisible": true,
                "left": "39dp",
                "onClick": controller.AS_Button_a45a5d791ea44361b188d7a9fc08a8dd,
                "skin": "defBtnNormal",
                "text": "Button",
                "top": "480dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            this.add(donutchart, Button0f615e3b2baca49);
        };
        return [{
            "addWidgets": addWidgetsForm1,
            "enabledForIdleTimeout": false,
            "id": "Form1",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});