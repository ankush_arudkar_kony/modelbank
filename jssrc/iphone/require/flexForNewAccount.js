define("flexForNewAccount", function() {
    return function(controller) {
        var flexForNewAccount = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flexForNewAccount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flexForNewAccount.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label({
            "centerY": "25%",
            "id": "lbl1",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0jd8faf2d94d448",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbl2 = new kony.ui.Label({
            "centerY": "51%",
            "id": "lbl2",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0cc3c834317dc40",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbl3 = new kony.ui.Label({
            "centerY": "75%",
            "id": "lbl3",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0d1b410f085cd40",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var imgselection = new kony.ui.Image2({
            "centerY": "50%",
            "height": "30%",
            "id": "imgselection",
            "isVisible": true,
            "right": 15,
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flexForNewAccount.add(lbl1, lbl2, lbl3, imgselection);
        return flexForNewAccount;
    }
})