define("tempNew", function() {
    return function(controller) {
        var tempNew = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "87%",
            "id": "tempNew",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        tempNew.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "52%",
            "clipBounds": false,
            "height": "86%",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "CopyslFbox0ddaa817f86884d",
            "top": "0dp",
            "width": "95%"
        }, {}, {});
        flxMain.setDefaultUnit(kony.flex.DP);
        var imgProfile = new kony.ui.Image2({
            "centerX": "50%",
            "height": "25%",
            "id": "imgProfile",
            "isVisible": true,
            "src": "imagedrag.png",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblName = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblName",
            "isVisible": true,
            "skin": "sknLblBlackName",
            "text": "Label",
            "textStyle": {},
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblFav = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblFav",
            "isVisible": true,
            "skin": "CopyCopyCopydefLabel",
            "text": "Label",
            "textStyle": {},
            "top": "2%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblArea = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblArea",
            "isVisible": true,
            "skin": "CopyCopyCopydefLabel",
            "text": "Label",
            "textStyle": {},
            "top": "1%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblDes = new kony.ui.Label({
            "height": "25%",
            "id": "lblDes",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLblDesBlack",
            "text": "Label",
            "textStyle": {},
            "top": "14dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblExpertise = new kony.ui.Label({
            "id": "lblExpertise",
            "isVisible": true,
            "left": "5%",
            "skin": "sknlblGrey",
            "text": "Label",
            "textStyle": {},
            "top": "-4%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var flxExpertise = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "24dp",
            "id": "flxExpertise",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxExpertise.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lbl1",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblbgGrey",
            "text": "Label",
            "textStyle": {},
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbl2 = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lbl2",
            "isVisible": true,
            "left": "2%",
            "skin": "sknlblbgGrey",
            "text": "Label",
            "textStyle": {},
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxExpertise.add(lbl1, lbl2);
        var lblHrs = new kony.ui.Label({
            "id": "lblHrs",
            "isVisible": true,
            "left": "5%",
            "skin": "sknlblGrey",
            "text": "Label",
            "textStyle": {},
            "top": "3%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var flxTiming = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxTiming",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "top": "4dp",
            "width": "80%",
            "zIndex": 1
        }, {}, {});
        flxTiming.setDefaultUnit(kony.flex.DP);
        var Copylbl0a1a0b464f64140 = new kony.ui.Label({
            "centerY": "50%",
            "height": "80%",
            "id": "Copylbl0a1a0b464f64140",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLblDesBlack",
            "text": "Label",
            "textStyle": {},
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxTiming.add(Copylbl0a1a0b464f64140);
        var lblNMLS = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblNMLS",
            "isVisible": true,
            "skin": "CopyCopyCopydefLabel",
            "text": "Label",
            "textStyle": {},
            "top": "4%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxMain.add(imgProfile, lblName, lblFav, lblArea, lblDes, lblExpertise, flxExpertise, lblHrs, flxTiming, lblNMLS);
        var btnGetStarted = new kony.ui.Button({
            "bottom": "0%",
            "centerX": "50%",
            "focusSkin": "sknBtnFocus",
            "height": "50dp",
            "id": "btnGetStarted",
            "isVisible": true,
            "onClick": controller.AS_Button_i49bbbfed1ae4e49a817098c823db614,
            "skin": "sknBtnBlueN",
            "text": "Choose Kim",
            "width": "75%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "showProgressIndicator": true
        });
        tempNew.add(flxMain, btnGetStarted);
        return tempNew;
    }
})