define("frmLogin", function() {
    return function(controller) {
        function addWidgetsfrmLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "110%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d0cd28aeed3642",
                "top": "-45dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40%",
                "id": "flxTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTop.setDefaultUnit(kony.flex.DP);
            var imgBackgroud = new kony.ui.Image2({
                "height": "100%",
                "id": "imgBackgroud",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTop.add(imgBackgroud);
            var flxMiddle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "430dp",
                "id": "flxMiddle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxMiddleBackgroud",
                "top": "22%",
                "width": "90%",
                "zIndex": 5
            }, {}, {});
            flxMiddle.setDefaultUnit(kony.flex.DP);
            var imgDBXLogo = new kony.ui.Image2({
                "centerX": "50%",
                "height": "90dp",
                "id": "imgDBXLogo",
                "isVisible": true,
                "skin": "slImage",
                "src": "konylogo_color.png",
                "top": "15dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "130dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxUserName.setDefaultUnit(kony.flex.DP);
            var txtUsername = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "CopydefTextBoxNormal0a030ef5eb35947",
                "height": "40dp",
                "id": "txtUsername",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "onDone": controller.AS_TextField_fe8d91e8deae4a4b83259f9dadb73ec5,
                "onTouchStart": controller.AS_TextField_g11ec17e4bb3465aa3de2b0563476bee,
                "placeholder": "Username",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0a030ef5eb35947",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "onEndEditing": controller.AS_TextField_a17dfda1e4364693b46e70212534b6ed,
                "placeholderSkin": "CopydefTextBoxNormal0cfcdd1cbf8d142",
                "showClearButton": true,
                "showCloseButton": false,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblLIne1 = new kony.ui.Label({
                "bottom": "5dp",
                "height": "1dp",
                "id": "lblLIne1",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLblLine",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxLineBg",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxUserName.add(txtUsername, lblLIne1, flxLine);
            var flxPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "190dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxPassword.setDefaultUnit(kony.flex.DP);
            var txtPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "CopydefTextBoxNormal0a030ef5eb35947",
                "height": "40dp",
                "id": "txtPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "onDone": controller.AS_TextField_deb7d49ed2674c89a6d83f40fe6a976e,
                "onTouchStart": controller.AS_TextField_i7e0f7d569664948877d7913924f3ebe,
                "placeholder": "Password",
                "secureTextEntry": true,
                "skin": "CopydefTextBoxNormal0a030ef5eb35947",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "onEndEditing": controller.AS_TextField_d302524abdb041c2b3b1b94d47ede40b,
                "placeholderSkin": "CopydefTextBoxNormal0cfcdd1cbf8d142",
                "showClearButton": false,
                "showCloseButton": false,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblLine2 = new kony.ui.Label({
                "bottom": "5dp",
                "height": "1dp",
                "id": "lblLine2",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLblLine",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgHide = new kony.ui.Image2({
                "centerY": "50%",
                "height": "22dp",
                "id": "imgHide",
                "isVisible": true,
                "right": "3dp",
                "skin": "slImage",
                "src": "password_eyeoff.png",
                "top": "10dp",
                "width": "22dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLine2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxLineBg",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine2.setDefaultUnit(kony.flex.DP);
            flxLine2.add();
            flxPassword.add(txtPassword, lblLine2, imgHide, flxLine2);
            var flxRemenberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRemenberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "245dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxRemenberMe.setDefaultUnit(kony.flex.DP);
            var lblRememberMe = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRememberMe",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknRememberMe21",
                "text": "Remember Me",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var imgRememberMe = new kony.ui.Image2({
                "centerY": "50%",
                "height": "70%",
                "id": "imgRememberMe",
                "isVisible": true,
                "onTouchEnd": controller.AS_Image_d43618599d5946afbc25f1856b09782c,
                "right": "2%",
                "skin": "slImage",
                "src": "switchon.png",
                "width": "30%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRemenberMe.add(lblRememberMe, imgRememberMe);
            var flxForgotPassword = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxForgotPassword",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "4%",
                "skin": "slFbox",
                "top": "245dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxForgotPassword.setDefaultUnit(kony.flex.DP);
            var lblForgot = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblForgot",
                "isVisible": true,
                "right": "0dp",
                "skin": "CopysknlblPurple0c4f70b10675f4a",
                "text": "Forgot Password?",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxForgotPassword.add(lblForgot);
            var btnSignIn = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "CopysknBtnFocus0f3a85ed09d4e46",
                "height": "50dp",
                "id": "btnSignIn",
                "isVisible": true,
                "onClick": controller.AS_Button_d1ad2f6bd8aa4bfe807fa9853ddd7161,
                "skin": "CopysknBtnGradPurple0a923eb5238c043",
                "text": "Sign In",
                "top": "305dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            var flxSignUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSignUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "370dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSignUp.setDefaultUnit(kony.flex.DP);
            var rtxSignUp = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "height": "80%",
                "id": "rtxSignUp",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0f34789ef723248",
                "text": "Don't have an account? <font color=\"#4176a4\">Sign Up</font>",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxSignUp.add(rtxSignUp);
            flxMiddle.add(imgDBXLogo, flxUserName, flxPassword, flxRemenberMe, flxForgotPassword, btnSignIn, flxSignUp);
            var flxBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60%",
                "id": "flxBottom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFlxGradiantPurple0fa30ef4230b349",
                "top": "40%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottom.setDefaultUnit(kony.flex.DP);
            var lblLine3 = new kony.ui.Label({
                "bottom": "65dp",
                "centerX": "50%",
                "height": "1dp",
                "id": "lblLine3",
                "isVisible": true,
                "skin": "sknLblLineWhite",
                "textStyle": {},
                "top": "335dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxFooterMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxFooterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "340dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFooterMenu.setDefaultUnit(kony.flex.DP);
            var flxLocate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "flxLocate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxLocate.setDefaultUnit(kony.flex.DP);
            var lblLocate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocate",
                "isVisible": true,
                "left": "45dp",
                "skin": "sknlblWhite120",
                "text": "Locate",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblLine4 = new kony.ui.Label({
                "centerY": "50%",
                "height": "50%",
                "id": "lblLine4",
                "isVisible": true,
                "right": "2%",
                "skin": "sknLblLineWhite",
                "textStyle": {},
                "width": "2dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxLocate.add(lblLocate, lblLine4);
            var flxEnroll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "flxEnroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxEnroll.setDefaultUnit(kony.flex.DP);
            var lblEnroll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEnroll",
                "isVisible": true,
                "left": "45dp",
                "skin": "sknlblWhite120",
                "text": "Enroll",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblLine5 = new kony.ui.Label({
                "centerY": "50%",
                "height": "50%",
                "id": "lblLine5",
                "isVisible": true,
                "right": "2%",
                "skin": "sknLblLineWhite",
                "textStyle": {},
                "width": "2dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxEnroll.add(lblEnroll, lblLine5);
            var flxSupport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "flxSupport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxSupport.setDefaultUnit(kony.flex.DP);
            var lblSupport = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSupport",
                "isVisible": true,
                "left": "45dp",
                "skin": "sknlblWhite120",
                "text": "Support",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblLine0a4b2d81f09e141 = new kony.ui.Label({
                "centerY": "50%",
                "height": "50%",
                "id": "CopylblLine0a4b2d81f09e141",
                "isVisible": false,
                "right": "2%",
                "skin": "sknLblLineWhite",
                "textStyle": {},
                "width": "1dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxSupport.add(lblSupport, CopylblLine0a4b2d81f09e141);
            flxFooterMenu.add(flxLocate, flxEnroll, flxSupport);
            flxBottom.add(lblLine3, flxFooterMenu);
            flxMain.add(flxTop, flxMiddle, flxBottom);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmLogin,
            "bounces": false,
            "enableScrolling": false,
            "enabledForIdleTimeout": false,
            "id": "frmLogin",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e6e608fa2e004c85b0be369cc686bc28(eventobject);
            },
            "skin": "CopysknFrmBgPurple0g9d6bd9dde034d"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": true,
            "configureExtendTop": true,
            "configureStatusBarStyle": true,
            "extendBottom": true,
            "extendTop": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_NONE,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_LIGHT_CONTENT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});