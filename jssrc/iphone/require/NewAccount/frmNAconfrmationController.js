define("NewAccount/userfrmNAconfrmationController", {
    //Type your controller code here 
});
define("NewAccount/frmNAconfrmationControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for CopyFlexContainer0bffc8c7b19f249 **/
    AS_FlexContainer_bee77100a3ef4e5ea7a1047811eb7879: function AS_FlexContainer_bee77100a3ef4e5ea7a1047811eb7879(eventobject) {
        var self = this;
        this.view.flxPopUp.setVisibility(true);
    },
    /** onTouchEnd defined for Button0a00a22369f244a **/
    AS_Button_bf6f3b4e7e034a8fba7908b614da8827: function AS_Button_bf6f3b4e7e034a8fba7908b614da8827(eventobject, x, y) {
        var self = this;
        this.view.flxPopUp.setVisibility(false);
    },
    /** onClick defined for bnMyAccounts **/
    AS_Button_f9472268afdd4407ab382ac6815963cb: function AS_Button_f9472268afdd4407ab382ac6815963cb(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    }
});
define("NewAccount/frmNAconfrmationController", ["NewAccount/userfrmNAconfrmationController", "NewAccount/frmNAconfrmationControllerActions"], function() {
    var controller = require("NewAccount/userfrmNAconfrmationController");
    var controllerActions = ["NewAccount/frmNAconfrmationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
