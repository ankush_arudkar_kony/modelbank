define("NewAccount/frmNAconfrmation", function() {
    return function(controller) {
        function addWidgetsfrmNAconfrmation() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c846579cfa2b44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "FlexContainer0c846579cfa2b44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c846579cfa2b44.setDefaultUnit(kony.flex.DP);
            var Image0f7270bbd061d44 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "75dp",
                "id": "Image0f7270bbd061d44",
                "isVisible": true,
                "left": "133dp",
                "skin": "slImage",
                "src": "ok_03_1.png",
                "top": "20dp",
                "width": "75dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0c846579cfa2b44.add(Image0f7270bbd061d44);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopyCopysknlblAccountName3",
                "text": "We successfully opened your accounts",
                "textStyle": {},
                "top": "25dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblPersonalChecking0gc56cf1eb0f54f = new kony.ui.Label({
                "centerX": "50%",
                "id": "CopylblPersonalChecking0gc56cf1eb0f54f",
                "isVisible": true,
                "skin": "CopysknlblAccountName0e8ae817c6be141",
                "text": "The following products are now active:",
                "textStyle": {},
                "top": "25dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var FlexContainer0g35ce9015ce94f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "60dp",
                "id": "FlexContainer0g35ce9015ce94f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h171a4fe888144",
                "top": "25dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0g35ce9015ce94f.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "centerY": "33%",
                "id": "lbl1",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0eecc773268cf4c",
                "text": "Youth Checking",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbl3 = new kony.ui.Label({
                "centerY": "66%",
                "id": "lbl3",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0c886e07a247345",
                "text": "Our most popular products offering, with high intrest rates...",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0g35ce9015ce94f.add(lbl1, lbl3);
            var CopyFlexContainer0bffc8c7b19f249 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "60dp",
                "id": "CopyFlexContainer0bffc8c7b19f249",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_bee77100a3ef4e5ea7a1047811eb7879,
                "skin": "CopyslFbox0h171a4fe888144",
                "top": "20dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            CopyFlexContainer0bffc8c7b19f249.setDefaultUnit(kony.flex.DP);
            var Copylbl0c615e41c4cfe49 = new kony.ui.Label({
                "centerY": "33%",
                "id": "Copylbl0c615e41c4cfe49",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0i6ed921081d244",
                "text": "Pro Checking",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Copylbl0da1127da790a4d = new kony.ui.Label({
                "centerY": "66%",
                "id": "Copylbl0da1127da790a4d",
                "isVisible": true,
                "left": "15dp",
                "skin": "CopydefLabel0c886e07a247345",
                "text": "Our most popular products offering, with high intrest rates...",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyFlexContainer0bffc8c7b19f249.add(Copylbl0c615e41c4cfe49, Copylbl0da1127da790a4d);
            flxMain.add(FlexContainer0c846579cfa2b44, lblPersonalChecking, CopylblPersonalChecking0gc56cf1eb0f54f, FlexContainer0g35ce9015ce94f, CopyFlexContainer0bffc8c7b19f249);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b8117917abf547",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var FlexContainer0ee0cb99210fd42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "54%",
                "clipBounds": true,
                "height": "95%",
                "id": "FlexContainer0ee0cb99210fd42",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopyslFbox0a5ea5a4135ff4f",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0ee0cb99210fd42.setDefaultUnit(kony.flex.DP);
            var FlexGroup0e651f43f0cef47 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexGroup0e651f43f0cef47",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "100%"
            }, {}, {});
            FlexGroup0e651f43f0cef47.setDefaultUnit(kony.flex.DP);
            var Label0b8a5779c2eb643 = new kony.ui.Label({
                "id": "Label0b8a5779c2eb643",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ac45fc5b17eb4d",
                "text": "Pro Checking",
                "textStyle": {},
                "top": "0dp",
                "width": "64.05%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbllIne = new kony.ui.Label({
                "bottom": 1,
                "height": "1dp",
                "id": "lbllIne",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopydefLabel2",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Button0a00a22369f244a = new kony.ui.Button({
                "focusSkin": "CopydefBtnNormal0c0bf23b1e0f04f",
                "height": "16dp",
                "id": "Button0a00a22369f244a",
                "isVisible": true,
                "onTouchEnd": controller.AS_Button_bf6f3b4e7e034a8fba7908b614da8827,
                "right": "10dp",
                "skin": "CopydefBtnNormal0c0bf23b1e0f04f",
                "top": "10dp",
                "width": "16dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            FlexGroup0e651f43f0cef47.add(Label0b8a5779c2eb643, lbllIne, Button0a00a22369f244a);
            var Labelhead = new kony.ui.Label({
                "id": "Labelhead",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0jb84199f851a4b",
                "text": "Description",
                "textStyle": {},
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblcontent = new kony.ui.Label({
                "id": "lblcontent",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ed2efa60d5d444",
                "text": "All of our checking accounts come with access to line banking, both on your desktop and your smartphone. Access your accounts anytime, anywhere Features",
                "textStyle": {},
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyLabelhead0b9018370660d4c = new kony.ui.Label({
                "id": "CopyLabelhead0b9018370660d4c",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0jb84199f851a4b",
                "text": "Features",
                "textStyle": {},
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Copylblcontent0f0828887e68c46 = new kony.ui.Label({
                "id": "Copylblcontent0f0828887e68c46",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ed2efa60d5d444",
                "text": "• No minimum balance\n • Earns monthly interest\n • Direct deposit \n• No unexpected or hidden service fees Overdraft protection services \n• Visa debit card \n• Access to over 55,000 surcharge-free ATMs \n• Access to Online/ Mobile banking \n•Access to E-Document service for paperless statements and notices \n• Access to our Mobile Check Deposit Service ",
                "textStyle": {},
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyLabelhead0de856fcb3d1e44 = new kony.ui.Label({
                "id": "CopyLabelhead0de856fcb3d1e44",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0jb84199f851a4b",
                "text": "Charges and Fees",
                "textStyle": {},
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Copylblcontent0gc0c4426ccd244 = new kony.ui.Label({
                "id": "Copylblcontent0gc0c4426ccd244",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ed2efa60d5d444",
                "text": "Our free personal checking account earns interest and has no minimum balance requirements. All you need is a $25 opening balance to gain access to over 55,000 ATMs and our Visa debit card.",
                "textStyle": {},
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0ee0cb99210fd42.add(FlexGroup0e651f43f0cef47, Labelhead, lblcontent, CopyLabelhead0b9018370660d4c, Copylblcontent0f0828887e68c46, CopyLabelhead0de856fcb3d1e44, Copylblcontent0gc0c4426ccd244);
            flxPopUp.add(FlexContainer0ee0cb99210fd42);
            var bnMyAccounts = new kony.ui.Button({
                "bottom": "30dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnMyAccounts",
                "isVisible": true,
                "onClick": controller.AS_Button_f9472268afdd4407ab382ac6815963cb,
                "skin": "CopysknMyAccountsN",
                "text": "My Accounts",
                "width": "80%",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            this.add(flxMain, flxPopUp, bnMyAccounts);
        };
        return [{
            "addWidgets": addWidgetsfrmNAconfrmation,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmNAconfrmation",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopysknFromBGWhite0f6fa4324c3aa45"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});