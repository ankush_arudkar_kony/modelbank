define("userfrmGetStarted2Controller", {
    //Type your controller code here 
});
define("frmGetStarted2ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnGetStarted **/
    AS_Button_c46027f31c784d0dad7021c2f4e60264: function AS_Button_c46027f31c784d0dad7021c2f4e60264(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmChooseAgent");
        ntf.navigate();
    },
    /** onClick defined for btnSwitch **/
    AS_Button_b1c34031ba684c82a18fd3a1a8ae218a: function AS_Button_b1c34031ba684c82a18fd3a1a8ae218a(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmGetstarted");
        ntf.navigate();
    },
    /** onClick defined for btnBack **/
    AS_Button_h91674ca8bfd4bdea96f21a742e828a5: function AS_Button_h91674ca8bfd4bdea96f21a742e828a5(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** preShow defined for frmGetStarted2 **/
    AS_Form_ded0fd613abd46ac9782da7db5b023ff: function AS_Form_ded0fd613abd46ac9782da7db5b023ff(eventobject) {
        var self = this;
        this.view.segIntro.pageSkin = "sknSegffffff";
    }
});
define("frmGetStarted2Controller", ["userfrmGetStarted2Controller", "frmGetStarted2ControllerActions"], function() {
    var controller = require("userfrmGetStarted2Controller");
    var controllerActions = ["frmGetStarted2ControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
