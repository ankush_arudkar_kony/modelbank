define("flxHdrImageText", function() {
    return function(controller) {
        var flxHdrImageText = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxHdrImageText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0fdf7ff8ae46944",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxHdrImageText.setDefaultUnit(kony.flex.DP);
        var imgAccountType = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgAccountType",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "money_icon.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "left": "45dp",
            "skin": "CopysknlblAccountName0ea14f8d8dd994b",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbllIne = new kony.ui.Label({
            "bottom": 3,
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": false,
            "left": "0dp",
            "skin": "CopydefLabel0c027844cadc043",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxHdrImageText.add(imgAccountType, lblAccount, lbllIne);
        return flxHdrImageText;
    }
})