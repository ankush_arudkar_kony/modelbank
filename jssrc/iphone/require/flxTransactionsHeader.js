define("flxTransactionsHeader", function() {
    return function(controller) {
        var flxTransactionsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxTransactionsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0b50e604257b44c",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTransactionsHeader.setDefaultUnit(kony.flex.DP);
        var lblAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopysknlblAccountName0ea14f8d8dd994b",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbllIne = new kony.ui.Label({
            "bottom": 3,
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": false,
            "left": "0dp",
            "skin": "CopydefLabel0c027844cadc043",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxTransactionsHeader.add(lblAccount, lbllIne);
        return flxTransactionsHeader;
    }
})