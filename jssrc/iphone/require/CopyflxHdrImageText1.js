define("CopyflxHdrImageText1", function() {
    return function(controller) {
        var CopyflxHdrImageText1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "CopyflxHdrImageText1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox3",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        CopyflxHdrImageText1.setDefaultUnit(kony.flex.DP);
        var imgAccountType = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgAccountType",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "money_icon_2.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "left": "45dp",
            "skin": "CopyCopysknlblAccountName2",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbllIne = new kony.ui.Label({
            "bottom": 3,
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": false,
            "left": "0dp",
            "skin": "CopyCopydefLabel1",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        CopyflxHdrImageText1.add(imgAccountType, lblAccount, lbllIne);
        return CopyflxHdrImageText1;
    }
})