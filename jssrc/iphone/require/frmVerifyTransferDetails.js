define("frmVerifyTransferDetails", function() {
    return function(controller) {
        function addWidgetsfrmVerifyTransferDetails() {
            this.setDefaultUnit(kony.flex.DP);
            var flxTransferDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": false,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxTransferDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxWhiteBG0g6d8ddff830a44",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransferDetails.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i357d8219c1b45c5bad4e53339f4e413,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f6402ac3892045fab902e3df4042daf7,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel);
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "Verify Transfer Details",
                "textStyle": {},
                "top": "50dp",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxFrom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxFrom.setDefaultUnit(kony.flex.DP);
            var FlexContainer0b8d6266a3bf24e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "FlexContainer0b8d6266a3bf24e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "88%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0b8d6266a3bf24e.setDefaultUnit(kony.flex.DP);
            var Label0c180603619a54f = new kony.ui.Label({
                "height": "100%",
                "id": "Label0c180603619a54f",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "From",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSelectedFromAccount = new kony.ui.Label({
                "height": "100%",
                "id": "lblSelectedFromAccount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl110SemiBold222222",
                "text": "Personal Checking....x4567",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0b8d6266a3bf24e.add(Label0c180603619a54f, lblSelectedFromAccount);
            var lblAvailableBalanceFrom = new kony.ui.Label({
                "bottom": "0dp",
                "height": "50%",
                "id": "lblAvailableBalanceFrom",
                "isVisible": true,
                "right": "12%",
                "skin": "sknlblBankName",
                "text": "Available Balance: $10,456.32",
                "textStyle": {},
                "width": "88%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var Image0bc3225b2ebb148 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "Image0bc3225b2ebb148",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFrom.add(FlexContainer0b8d6266a3bf24e, lblAvailableBalanceFrom, Image0bc3225b2ebb148);
            var flxTo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "160dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTo.setDefaultUnit(kony.flex.DP);
            var CopyFlexContainer0fbf9932c483542 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50%",
                "id": "CopyFlexContainer0fbf9932c483542",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "88%",
                "zIndex": 1
            }, {}, {});
            CopyFlexContainer0fbf9932c483542.setDefaultUnit(kony.flex.DP);
            var CopyLabel0h318efecf7c14a = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0h318efecf7c14a",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "To",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblSelectedToAccount = new kony.ui.Label({
                "height": "100%",
                "id": "lblSelectedToAccount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl110SemiBold222222",
                "text": "Linda's Savings....x7878",
                "textStyle": {},
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            CopyFlexContainer0fbf9932c483542.add(CopyLabel0h318efecf7c14a, lblSelectedToAccount);
            var lblAvailableBalanceTo = new kony.ui.Label({
                "bottom": "0dp",
                "height": "50%",
                "id": "lblAvailableBalanceTo",
                "isVisible": true,
                "right": "12%",
                "skin": "sknlblBankName",
                "text": "Available Balance: $2,345.67",
                "textStyle": {},
                "width": "88%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0f3d93fa0ce9c46 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0f3d93fa0ce9c46",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTo.add(CopyFlexContainer0fbf9932c483542, lblAvailableBalanceTo, CopyImage0f3d93fa0ce9c46);
            var flxFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "220dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxFrequency.setDefaultUnit(kony.flex.DP);
            var CopyLabel0g369a6beee8c49 = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0g369a6beee8c49",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Frequency",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblFrequency = new kony.ui.Label({
                "height": "100%",
                "id": "lblFrequency",
                "isVisible": true,
                "right": "12%",
                "skin": "sknLbl110SemiBold222222",
                "text": "Once",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0h819f8b02ebf4e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0h819f8b02ebf4e",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFrequency.add(CopyLabel0g369a6beee8c49, lblFrequency, CopyImage0h819f8b02ebf4e);
            var flxTransferDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTransferDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "280dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTransferDate.setDefaultUnit(kony.flex.DP);
            var CopyLabel0cfb3ebbf1e614e = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0cfb3ebbf1e614e",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Transfer Date",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblDate = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate",
                "isVisible": true,
                "right": "12%",
                "skin": "sknLbl110SemiBold222222",
                "text": "09/12/19",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0jbbfb8942bf54c = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0jbbfb8942bf54c",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransferDate.add(CopyLabel0cfb3ebbf1e614e, lblDate, CopyImage0jbbfb8942bf54c);
            var flxEnteredAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEnteredAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "340dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxEnteredAmount.setDefaultUnit(kony.flex.DP);
            var CopyLabel0j5d56d735cf44b = new kony.ui.Label({
                "height": "100%",
                "id": "CopyLabel0j5d56d735cf44b",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey100Regular",
                "text": "Amount",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAmount = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount",
                "isVisible": true,
                "right": "12%",
                "skin": "sknLbl110SemiBold222222",
                "text": "09/12/19",
                "textStyle": {},
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopyImage0j4d03996667848 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyImage0j4d03996667848",
                "isVisible": true,
                "right": "0%",
                "skin": "slImage",
                "src": "right_arrow.png",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEnteredAmount.add(CopyLabel0j5d56d735cf44b, lblAmount, CopyImage0j4d03996667848);
            var flxNotes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxNotes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "400dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxNotes.setDefaultUnit(kony.flex.DP);
            var txtNotes = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtBxTrans",
                "height": "88%",
                "id": "txtNotes",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Note (Optional)",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTxtBxTrans",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var CopyflxLine0b0f8085572564d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0b0f8085572564d",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLineDarkGrey",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0b0f8085572564d.setDefaultUnit(kony.flex.DP);
            CopyflxLine0b0f8085572564d.add();
            flxNotes.add(txtNotes, CopyflxLine0b0f8085572564d);
            var bnTransfer = new kony.ui.Button({
                "bottom": "30dp",
                "centerX": "50%",
                "focusSkin": "sknBtnFocus",
                "height": "50dp",
                "id": "bnTransfer",
                "isVisible": true,
                "onClick": controller.AS_Button_a596153f46424611a6dd138bb1b3db8c,
                "skin": "sknBtnBlueN",
                "text": "Transfer",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxTransferDetails.add(FlexContainer0eed5fe5dc1064e, lblPersonalChecking, flxFrom, flxTo, flxFrequency, flxTransferDate, flxEnteredAmount, flxNotes, bnTransfer);
            this.add(flxTransferDetails);
        };
        return [{
            "addWidgets": addWidgetsfrmVerifyTransferDetails,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmVerifyTransferDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_hf6eef420dd9483093ebc642ffc90d8b(eventobject);
            },
            "skin": "CopysknFromBGWhite0f6fa4324c3aa45",
            "verticalScrollIndicator": false
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "bouncesZoom": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": false,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});