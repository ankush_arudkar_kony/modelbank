define("userfrmPPSendSuccessController", {
    //Type your controller code here 
});
define("frmPPSendSuccessControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for bnTransfer **/
    AS_Button_bda9546064cb4846aee025968d709575: function AS_Button_bda9546064cb4846aee025968d709575(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** preShow defined for frmPPSendSuccess **/
    AS_Form_b0ab4ac6d80a4ff5a2db97be645c1bbd: function AS_Form_b0ab4ac6d80a4ff5a2db97be645c1bbd(eventobject) {
        var self = this;
        this.view.lblTransferredAmt.text = "You transferred " + gblEnteredPPAmt;
    }
});
define("frmPPSendSuccessController", ["userfrmPPSendSuccessController", "frmPPSendSuccessControllerActions"], function() {
    var controller = require("userfrmPPSendSuccessController");
    var controllerActions = ["frmPPSendSuccessControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
