define("tempSwipe", function() {
    return function(controller) {
        var tempSwipe = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "96%",
            "id": "tempSwipe",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknflxWhitebg3",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        tempSwipe.setDefaultUnit(kony.flex.DP);
        var flxBG = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55%",
            "id": "flxBG",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBG.setDefaultUnit(kony.flex.DP);
        var imgBG = new kony.ui.Image2({
            "height": "100%",
            "id": "imgBG",
            "isVisible": true,
            "left": "0dp",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxBG.add(imgBG);
        var flxUpparImg = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "30%",
            "id": "flxUpparImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "top": "38%",
            "width": "65%",
            "zIndex": 1
        }, {}, {});
        flxUpparImg.setDefaultUnit(kony.flex.DP);
        var upparImg = new kony.ui.Image2({
            "height": "100%",
            "id": "upparImg",
            "isVisible": true,
            "left": "0dp",
            "src": "imagedrag.png",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUpparImg.add(upparImg);
        var lbl1 = new kony.ui.Label({
            "centerX": "50%",
            "id": "lbl1",
            "isVisible": true,
            "skin": "CopydefLabel0b98e490f033b4b",
            "text": "Label",
            "textStyle": {},
            "top": "69%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbl2 = new kony.ui.Label({
            "centerX": "50%",
            "id": "lbl2",
            "isVisible": true,
            "skin": "CopyCopydefLabel4",
            "text": "Label",
            "textStyle": {},
            "top": "80%",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblSwipe = new kony.ui.Label({
            "bottom": "2%",
            "centerX": "50%",
            "id": "lblSwipe",
            "isVisible": true,
            "skin": "CopydefLabel0jfc33d29f3c747",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        tempSwipe.add(flxBG, flxUpparImg, lbl1, lbl2, lblSwipe);
        return tempSwipe;
    }
})