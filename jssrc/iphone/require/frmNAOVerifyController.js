define("userfrmNAOVerifyController", {
    //Type your controller code here 
});
define("frmNAOVerifyControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_f1554a789da044009c66c22cb8dfc1e2: function AS_FlexContainer_f1554a789da044009c66c22cb8dfc1e2(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("NewAccount/frmApplyNewAccount");
        ntf.navigate();
    },
    /** onClick defined for btnMaualCapture **/
    AS_Button_c634db93227548dcaf5838a8cea80406: function AS_Button_c634db93227548dcaf5838a8cea80406(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("NewAccount/frmNAconfrmation");
        ntf.navigate();
    }
});
define("frmNAOVerifyController", ["userfrmNAOVerifyController", "frmNAOVerifyControllerActions"], function() {
    var controller = require("userfrmNAOVerifyController");
    var controllerActions = ["frmNAOVerifyControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
