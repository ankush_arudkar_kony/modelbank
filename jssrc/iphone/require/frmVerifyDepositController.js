define("userfrmVerifyDepositController", {
    frm_preshow: function() {
            this.view.lblBalance.text = gblSelectedFromAccountRow1.lblAccountName;
            this.view.lblAmountValue.text = amount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
            //alert(gblSelectedAmount);
        }
        //Type your controller code here 
});
define("frmVerifyDepositControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_ed1507f813204867b4b7b791c66632ce: function AS_Image_ed1507f813204867b4b7b791c66632ce(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCapture");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_g5001d67aeb442b5876cc3c6a04f4672: function AS_Label_g5001d67aeb442b5876cc3c6a04f4672(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for btnMaualCapture **/
    AS_Button_b7286b258cce436f92619b8a9d490ace: function AS_Button_b7286b258cce436f92619b8a9d490ace(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDepositConformation");
        ntf.navigate();
    },
    /** preShow defined for frmVerifyDeposit **/
    AS_Form_h96baf29ee4440f18492448e04886023: function AS_Form_h96baf29ee4440f18492448e04886023(eventobject) {
        var self = this;
        return self.frm_preshow.call(this);
    }
});
define("frmVerifyDepositController", ["userfrmVerifyDepositController", "frmVerifyDepositControllerActions"], function() {
    var controller = require("userfrmVerifyDepositController");
    var controllerActions = ["frmVerifyDepositControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
