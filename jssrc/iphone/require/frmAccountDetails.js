define("frmAccountDetails", function() {
    return function(controller) {
        function addWidgetsfrmAccountDetails() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b8b5d5e86bed44",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var imgBg = new kony.ui.Image2({
                "height": "100%",
                "id": "imgBg",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "headerbg.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(imgBg);
            var CopyflxBtns0new = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "CopyflxBtns0new",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fdb4b003115642",
                "top": "15dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            CopyflxBtns0new.setDefaultUnit(kony.flex.DP);
            var yimgBacknew = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30dp",
                "id": "yimgBacknew",
                "isVisible": true,
                "left": "10dp",
                "onTouchEnd": controller.AS_Image_g4f49d62e5384d15b6e6840dd058f0af,
                "skin": "slImage",
                "src": "back.png",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyimgSearcchnew = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30dp",
                "id": "CopyimgSearcchnew",
                "isVisible": true,
                "right": "10dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxBtns0new.add(yimgBacknew, CopyimgSearcchnew);
            var flxWelcomenew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxWelcomenew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d5368b3edf0143",
                "top": "65dp",
                "width": "80%",
                "zIndex": 3
            }, {}, {});
            flxWelcomenew.setDefaultUnit(kony.flex.DP);
            var lblPersonalCheckingnew = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblPersonalCheckingnew",
                "isVisible": true,
                "left": "20dp",
                "skin": "skintwohun",
                "text": "Personal Checking",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxWelcomenew.add(lblPersonalCheckingnew);
            var flxContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": false,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "98%",
                "horizontalScrollIndicator": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "onScrolling": controller.AS_FlexScrollContainer_f15b02b2235442398caa3bb8ab56f859,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContentBg0ha8092d5921a4c",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxBtns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxBtns",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBtns.setDefaultUnit(kony.flex.DP);
            var imgBsck = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30dp",
                "id": "imgBsck",
                "isVisible": true,
                "left": "10dp",
                "onTouchEnd": controller.AS_Image_g4f49d62e5384d15b6e6840dd058f0af,
                "skin": "slImage",
                "src": "back.png",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSearch = new kony.ui.Image2({
                "centerY": "60%",
                "height": "30dp",
                "id": "imgSearch",
                "isVisible": true,
                "right": "10dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBtns.add(imgBsck, imgSearch);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxWelcome",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var lblPersonalChecking = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "20dp",
                "skin": "skinhundred",
                "text": "Personal Checking",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxWelcome.add(lblPersonalChecking);
            var flxPendingTransactions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxPendingTransactions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "320dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPendingTransactions.setDefaultUnit(kony.flex.DP);
            var flxSegPending = new com.demo.dbx.flxSegPending({
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxSegPending",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "95%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPendingTransactions.add(flxSegPending);
            var flxPostedTransactions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "660dp",
                "id": "flxPostedTransactions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "630dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPostedTransactions.setDefaultUnit(kony.flex.DP);
            var flxSegPosted = new com.demo.dbx.flxSegPosted.flxSegPosted({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxSegPosted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxShadow",
                "top": "0dp",
                "width": "88%",
                "overrides": {
                    "flxSegPosted": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "width": "88%"
                    },
                    "segPostedTransactions": {
                        "height": "570dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPostedTransactions.add(flxSegPosted);
            var flxAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%"
            }, {}, {});
            flxAccount.setDefaultUnit(kony.flex.DP);
            var flxAccountDetails = new kony.ui.FlexContainer({
                "centerX": "50%",
                "clipBounds": true,
                "height": "220dp",
                "id": "flxAccountDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d3915994b4bb4b",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAccountDetails.setDefaultUnit(kony.flex.DP);
            var flxAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "90%"
            }, {}, {});
            flxAccounts.setDefaultUnit(kony.flex.DP);
            var imgBankLogo = new kony.ui.Image2({
                "height": "18dp",
                "id": "imgBankLogo",
                "isVisible": true,
                "left": "120dp",
                "skin": "slImage",
                "src": "info_blue.png",
                "top": 15,
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccountName = new kony.ui.Label({
                "id": "lblAccountName",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopysknlblAccountName0d40ebfabde5e4c",
                "text": "Current Balance",
                "textStyle": {},
                "top": "15dp",
                "width": "115dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBalance = new kony.ui.Label({
                "id": "lblBalance",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblAccountName0ef2051f784ca42",
                "text": "$65,034.56",
                "textStyle": {},
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblBankName = new kony.ui.Label({
                "bottom": "15dp",
                "id": "lblBankName",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopysknlblBankName0a89f5deb474946",
                "text": "Available Balance",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lblAvailableBalance = new kony.ui.Label({
                "bottom": "15dp",
                "id": "lblAvailableBalance",
                "isVisible": true,
                "right": "15dp",
                "skin": "CopysknlblBankName0a352e920b0d64c",
                "text": "$55,860.66",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var lbllIne = new kony.ui.Label({
                "bottom": 3,
                "height": "1dp",
                "id": "lbllIne",
                "isVisible": false,
                "left": "0dp",
                "skin": "CopydefLabel0i1af17ef0c8348",
                "textStyle": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxAccounts.add(imgBankLogo, lblAccountName, lblBalance, lblBankName, lblAvailableBalance, lbllIne);
            var flxLinks = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "90dp",
                "id": "flxLinks",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFooterBg0ie06cae4889f49",
                "top": "90dp",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxLinks.setDefaultUnit(kony.flex.DP);
            var flxTransferFunds = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransferFunds",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTransferFunds.setDefaultUnit(kony.flex.DP);
            var imgTransferFunds = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgTransferFunds",
                "isVisible": true,
                "skin": "slImage",
                "src": "transfer_fund.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTransferFunds = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblTransferFunds",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Transfer Funds",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTransferFunds.add(imgTransferFunds, lblTransferFunds);
            var flxRequestMoney = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRequestMoney",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxRequestMoney.setDefaultUnit(kony.flex.DP);
            var imgRequestMoney = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgRequestMoney",
                "isVisible": true,
                "skin": "slImage",
                "src": "request_money.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRequestMoney = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblRequestMoney",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Request Money",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxRequestMoney.add(imgRequestMoney, lblRequestMoney);
            var flxDepositCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDepositCheck",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxDepositCheck.setDefaultUnit(kony.flex.DP);
            var imgDepositCheck = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgDepositCheck",
                "isVisible": true,
                "skin": "slImage",
                "src": "deposit_check.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDepositCheck = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblDepositCheck",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Deposit Check",
                "textStyle": {},
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxDepositCheck.add(imgDepositCheck, lblDepositCheck);
            var flxTravel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTravel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxTravel.setDefaultUnit(kony.flex.DP);
            var imgTravel = new kony.ui.Image2({
                "centerX": "50%",
                "height": "55dp",
                "id": "imgTravel",
                "isVisible": true,
                "skin": "slImage",
                "src": "travel_notification.png",
                "top": "5dp",
                "width": "55dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTravel = new kony.ui.Label({
                "bottom": "7dp",
                "centerX": "50%",
                "id": "lblTravel",
                "isVisible": true,
                "skin": "CopydefLabel0f3bc2b7ff2bb42",
                "text": "Travel Notification",
                "textStyle": {},
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxTravel.add(imgTravel, lblTravel);
            flxLinks.add(flxTransferFunds, flxRequestMoney, flxDepositCheck, flxTravel);
            flxAccountDetails.add(flxAccounts, flxLinks);
            var flxLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "165dp",
                "id": "flxLeft",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0edc0a2d042a944",
                "width": "10dp",
                "zIndex": 1
            }, {}, {});
            flxLeft.setDefaultUnit(kony.flex.DP);
            flxLeft.add();
            var flxRight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "165dp",
                "id": "flxRight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "-2dp",
                "skin": "CopyslFbox0edc0a2d042a944",
                "width": "10dp",
                "zIndex": 1
            }, {}, {});
            flxRight.setDefaultUnit(kony.flex.DP);
            flxRight.add();
            flxAccount.add(flxAccountDetails, flxLeft, flxRight);
            flxContent.add(flxBtns, flxWelcome, flxPendingTransactions, flxPostedTransactions, flxAccount);
            flxMain.add(flxHeader, CopyflxBtns0new, flxWelcomenew, flxContent);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmAccountDetails,
            "bounces": false,
            "enableScrolling": false,
            "enabledForIdleTimeout": false,
            "id": "frmAccountDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_e81921d0671040bd888eecc0d2f4e0d0,
            "preShow": function(eventobject) {
                controller.AS_Form_a3ece9b58dc848b688af710cfb6e868e(eventobject);
            },
            "skin": "sknFrmBgWhitef8f8f8",
            "statusBarHidden": false
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": true,
            "configureExtendTop": true,
            "configureStatusBarStyle": true,
            "extendBottom": true,
            "extendTop": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});