define("FBox0db73473fcf104a", function() {
    return function(controller) {
        FBox0db73473fcf104a = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "65dp",
            "id": "FBox0db73473fcf104a",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0db73473fcf104a.setDefaultUnit(kony.flex.DP);
        var FlexContainer0ee985f2fddf845 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "65dp",
            "id": "FlexContainer0ee985f2fddf845",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerWeight": 100
        }, {});
        FlexContainer0ee985f2fddf845.setDefaultUnit(kony.flex.DP);
        var lblAccountName = new kony.ui.Label({
            "id": "lblAccountName",
            "isVisible": true,
            "left": "13dp",
            "skin": "sknlblAccountName",
            "text": "Label",
            "textStyle": {},
            "top": "15dp",
            "width": "50%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "hExpand": true,
            "margin": [1, 1, 1, 1],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lblBankName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblBankName",
            "isVisible": true,
            "left": "13dp",
            "skin": "sknlblBankName",
            "text": "Label",
            "textStyle": {},
            "width": "80%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "hExpand": true,
            "margin": [1, 1, 1, 1],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var imgPayPal = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgPayPal",
            "isVisible": true,
            "right": "10%",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var lbllIne = new kony.ui.Label({
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0d54e0e18a61a44",
            "textStyle": {},
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "hExpand": true,
            "margin": [1, 1, 1, 1],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        FlexContainer0ee985f2fddf845.add(lblAccountName, lblBankName, imgPayPal, lbllIne);
        FBox0db73473fcf104a.add(FlexContainer0ee985f2fddf845);
        return FBox0db73473fcf104a;
    }
})