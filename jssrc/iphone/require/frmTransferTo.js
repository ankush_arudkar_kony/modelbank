define("frmTransferTo", function() {
    return function(controller) {
        function addWidgetsfrmTransferTo() {
            this.setDefaultUnit(kony.flex.DP);
            var FlexContainer0eed5fe5dc1064e = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "FlexContainer0eed5fe5dc1064e",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0eed5fe5dc1064e.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d8e47a200c14475dbc3df1139ff4625f,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            flxBack.add();
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e4a46b45c72546fd8d32da85ee26aa67,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            flxCancel.add();
            var lblPersonalChecking = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknlblAccountName0i9a59b07964843",
                "text": "To",
                "textStyle": {},
                "top": "0dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            FlexContainer0eed5fe5dc1064e.add(Image0fe32af4553f34e, Label0dd464373550946, flxBack, flxCancel, lblPersonalChecking);
            var flxScrlContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": false,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrlContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlWHiteBG",
                "top": "40dp",
                "verticalScrollIndicator": false,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrlContent.setDefaultUnit(kony.flex.DP);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var CopylblPersonalChecking0j74bf47189c841 = new kony.ui.Label({
                "id": "CopylblPersonalChecking0j74bf47189c841",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBlack95N",
                "text": "Select a recipient to transfer to",
                "textStyle": {},
                "top": "2dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "search_02.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAccountNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTxtBxTrans",
                "height": "100%",
                "id": "txtAccountNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "placeholder": "Search by account, IBAN or recepient",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTbxPlaceholder",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtAccountNumber);
            flxWelcome.add(CopylblPersonalChecking0j74bf47189c841, flxSearch);
            var flxChecking = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "320dp",
                "id": "flxChecking",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxChecking.setDefaultUnit(kony.flex.DP);
            var flxCheckingAccounts = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90%",
                "id": "flxCheckingAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "height": "90%",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "acc_detailspic3.png",
                                    "lblAccountName": "John's Checking....x5678",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23,456.77",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Sam's Checking....x9156",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$12,345.45",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "origin_24x24.png",
                                    "lblAccountName": "Jame's Checking....x7162",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23262.76",
                                    "lblBankName": "Temenos Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "245dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCheckingAccounts.segAccRecords.onRowClick = controller.AS_Segment_i0e3768eaf14456089711015dc38d905;
            flxChecking.add(flxCheckingAccounts);
            var flxSavings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxSavings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-46dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSavings.setDefaultUnit(kony.flex.DP);
            var flxSavingsAcounts = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSavingsAcounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "height": "100%",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "savings.png",
                                    "lblAccount": "Savings Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Linda's Savings....x7878",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2,346.67",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Personal Savings....x9651",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$7,867.55",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "John's Savings....x6711",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$8,956.44",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "origin_24x24.png",
                                    "lblAccountName": "Edward's Savings....x7614",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2233.45",
                                    "lblBankName": "Origin Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "310dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSavingsAcounts.segAccRecords.onRowClick = controller.AS_Segment_g48b2e5f71c2427ea85934097131de96;
            flxSavings.add(flxSavingsAcounts);
            var flxExternalRecipients = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxExternalRecipients",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxExternalRecipients.setDefaultUnit(kony.flex.DP);
            var EnternalRecipients = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "EnternalRecipients",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "-10dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "height": "100%",
                        "top": "-10dp",
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "icon_external.png",
                                    "lblAccount": "External Recipients",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Ellen Murray....x8407",
                                    "lblAvailableBalance": "Checking Account",
                                    "lblBalance": "",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Siri Jakobson....x9875",
                                    "lblAvailableBalance": "Checking Account",
                                    "lblBalance": "",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Steven Howard....x7834",
                                    "lblAvailableBalance": "Checking Account",
                                    "lblBalance": "",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "origin_24x24.png",
                                    "lblAccountName": "Richard Edison....x8723",
                                    "lblAvailableBalance": "Checking Account",
                                    "lblBalance": "",
                                    "lblBankName": "Temenos Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ],
                        "height": "310dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            EnternalRecipients.segAccRecords.onRowClick = controller.AS_Segment_adb3459b02f649d1a14d53842fd38b58;
            flxExternalRecipients.add(EnternalRecipients);
            var flxPerson = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxPerson",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "-28dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxPerson.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('FBox0db73473fcf104a', 'FBox0db73473fcf104a', 'FBox0db73473fcf104aController');
            var segPersonToPerson = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "data": [
                    [{
                            "imgLogo": "icon_p2p.png",
                            "lblAccount": "Person-to-Person",
                            "lbllIne": ""
                        },
                        [{
                            "imgPayPal": "paypal_icon.png",
                            "lblAccountName": "Andries Grootoonk",
                            "lblBankName": "(123) 456-7890",
                            "lbllIne": ""
                        }, {
                            "imgPayPal": "venmo.png",
                            "lblAccountName": "Paromita Haque",
                            "lblBankName": "paro.haque@gmail.com",
                            "lbllIne": ""
                        }, {
                            "imgPayPal": "zelle.png",
                            "lblAccountName": "Steve Richardson",
                            "lblBankName": "(443) 223-2725",
                            "lbllIne": ""
                        }, {
                            "imgPayPal": "paypal_icon.png",
                            "lblAccountName": "James Robert",
                            "lblBankName": "james.robert@gmail.com",
                            "lbllIne": ""
                        }]
                    ]
                ],
                "groupCells": false,
                "height": "310dp",
                "id": "segPersonToPerson",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_fd51a3b23f1842f8a5d52be76645c3e1,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "FBox0db73473fcf104a",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "CopyflxSegHeader0c856c8bbd46448",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "6dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CopyflxSegHeader0c856c8bbd46448": "CopyflxSegHeader0c856c8bbd46448",
                    "FlexContainer0ee985f2fddf845": "FlexContainer0ee985f2fddf845",
                    "imgLogo": "imgLogo",
                    "imgPayPal": "imgPayPal",
                    "lblAccount": "lblAccount",
                    "lblAccountName": "lblAccountName",
                    "lblBankName": "lblBankName",
                    "lbllIne": "lbllIne"
                },
                "width": "90%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "bounces": true,
                "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
                "enableDictionary": false,
                "indicator": constants.SEGUI_NONE,
                "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
                "showProgressIndicator": true
            });
            flxPerson.add(segPersonToPerson);
            flxScrlContent.add(flxWelcome, flxChecking, flxSavings, flxExternalRecipients, flxPerson);
            var flxBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxBottom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBG",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottom.setDefaultUnit(kony.flex.DP);
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f911bf8872584f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            var bnTransferToNewRecipient = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "50%",
                "focusSkin": "CopysknBtnFocus0b1315c23fdcf46",
                "height": "60%",
                "id": "bnTransferToNewRecipient",
                "isVisible": true,
                "skin": "sknBtnBlueN",
                "text": "Transfer to New Recipient",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "showProgressIndicator": true
            });
            flxBottom.add(flxLine, bnTransferToNewRecipient);
            this.add(FlexContainer0eed5fe5dc1064e, flxScrlContent, flxBottom);
        };
        return [{
            "addWidgets": addWidgetsfrmTransferTo,
            "enabledForIdleTimeout": false,
            "id": "frmTransferTo",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "sknFrmBgWhitef8f8f8"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});