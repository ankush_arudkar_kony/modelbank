define("flxSegHeader", function() {
    return function(controller) {
        var flxSegHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0b50e604257b44c",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegHeader.setDefaultUnit(kony.flex.DP);
        var imgLogo = new kony.ui.Image2({
            "centerY": "50%",
            "height": "38dp",
            "id": "imgLogo",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "30dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccount = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "left": "53dp",
            "skin": "CopysknlblAccountName0ea14f8d8dd994b",
            "text": "Label",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        var lbllIne = new kony.ui.Label({
            "bottom": 3,
            "height": "1dp",
            "id": "lbllIne",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0d54e0e18a61a44",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        });
        flxSegHeader.add(imgLogo, lblAccount, lbllIne);
        return flxSegHeader;
    }
})