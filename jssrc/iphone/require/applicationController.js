define({
    appInit: function(params) {
        skinsInit();
        kony.application.setCheckBoxSelectionImageAlignment(constants.CHECKBOX_SELECTION_IMAGE_ALIGNMENT_RIGHT);
        kony.application.setDefaultTextboxPadding(false);
        kony.application.setRespectImageSizeForImageWidgetAlignment(true);
        kony.mvc.registry.add("com.comp.segComp", "segComp", "segCompController");
        kony.application.registerMaster({
            "namespace": "com.comp",
            "classname": "segComp",
            "name": "com.comp.segComp"
        });
        kony.mvc.registry.add("com.demo.dbx.flxSegPending", "flxSegPending", "flxSegPendingController");
        kony.application.registerMaster({
            "namespace": "com.demo.dbx",
            "classname": "flxSegPending",
            "name": "com.demo.dbx.flxSegPending"
        });
        kony.mvc.registry.add("com.demo.dbx.flxSegPosted.flxSegPosted", "flxSegPosted", "flxSegPostedController");
        kony.application.registerMaster({
            "namespace": "com.demo.dbx.flxSegPosted",
            "classname": "flxSegPosted",
            "name": "com.demo.dbx.flxSegPosted.flxSegPosted"
        });
        kony.mvc.registry.add("com.demo.originbank.compSegfornewAccount", "compSegfornewAccount", "compSegfornewAccountController");
        kony.application.registerMaster({
            "namespace": "com.demo.originbank",
            "classname": "compSegfornewAccount",
            "name": "com.demo.originbank.compSegfornewAccount"
        });
        kony.mvc.registry.add("com.demo.originbank.compSegHdrWithImgText", "compSegHdrWithImgText", "compSegHdrWithImgTextController");
        kony.application.registerMaster({
            "namespace": "com.demo.originbank",
            "classname": "compSegHdrWithImgText",
            "name": "com.demo.originbank.compSegHdrWithImgText"
        });
        kony.mvc.registry.add("com.demo.originbank.compSegHdrWithoutImgText", "compSegHdrWithoutImgText", "compSegHdrWithoutImgTextController");
        kony.application.registerMaster({
            "namespace": "com.demo.originbank",
            "classname": "compSegHdrWithoutImgText",
            "name": "com.demo.originbank.compSegHdrWithoutImgText"
        });
        kony.mvc.registry.add("com.konymp.AccountDetails.AccountDetails", "AccountDetails", "AccountDetailsController");
        kony.application.registerMaster({
            "namespace": "com.konymp.AccountDetails",
            "classname": "AccountDetails",
            "name": "com.konymp.AccountDetails.AccountDetails"
        });
        kony.mvc.registry.add("com.konymp.donutchart", "donutchart", "donutchartController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchart",
            "name": "com.konymp.donutchart"
        });
        kony.mvc.registry.add("com.konymp.donutchart3", "donutchart3", "donutchart3Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchart3",
            "name": "com.konymp.donutchart3"
        });
        kony.mvc.registry.add("com.konymp.donutchart4", "donutchart4", "donutchart4Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchart4",
            "name": "com.konymp.donutchart4"
        });
        kony.mvc.registry.add("com.konymp.donutchart5", "donutchart5", "donutchart5Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchart5",
            "name": "com.konymp.donutchart5"
        });
        kony.mvc.registry.add("com.konymp.donutchartCopy", "donutchartCopy", "donutchartCopyController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchartCopy",
            "name": "com.konymp.donutchartCopy"
        });
        kony.mvc.registry.add("com.konymp.segTransactions.segTransactions", "segTransactions", "segTransactionsController");
        kony.application.registerMaster({
            "namespace": "com.konymp.segTransactions",
            "classname": "segTransactions",
            "name": "com.konymp.segTransactions.segTransactions"
        });
        kony.mvc.registry.add("com.konymp.segTransactionsPending.segTransactions", "segTransactions", "segTransactionsController");
        kony.application.registerMaster({
            "namespace": "com.konymp.segTransactionsPending",
            "classname": "segTransactions",
            "name": "com.konymp.segTransactionsPending.segTransactions"
        });
        kony.mvc.registry.add("com.konymp.verticalbar", "verticalbar", "verticalbarController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "verticalbar",
            "name": "com.konymp.verticalbar"
        });
        kony.mvc.registry.add("CopyflxAccounts0ed79c7a8aaca4f", "CopyflxAccounts0ed79c7a8aaca4f", "CopyflxAccounts0ed79c7a8aaca4fController");
        kony.mvc.registry.add("CopyflxAccounts0b622ef8af97e41", "CopyflxAccounts0b622ef8af97e41", "CopyflxAccounts0b622ef8af97e41Controller");
        kony.mvc.registry.add("CopyflxHdrImageText", "CopyflxHdrImageText", "CopyflxHdrImageTextController");
        kony.mvc.registry.add("CopyflxHdrImageText1", "CopyflxHdrImageText1", "CopyflxHdrImageText1Controller");
        kony.mvc.registry.add("flxSampleRowTemplate", "flxSampleRowTemplate", "flxSampleRowTemplateController");
        kony.mvc.registry.add("flxSectionHeaderTemplate", "flxSectionHeaderTemplate", "flxSectionHeaderTemplateController");
        kony.mvc.registry.add("flxAccounts", "flxAccounts", "flxAccountsController");
        kony.mvc.registry.add("flxSegHeader", "flxSegHeader", "flxSegHeaderController");
        kony.mvc.registry.add("CopyflxSegHeader0c856c8bbd46448", "CopyflxSegHeader0c856c8bbd46448", "CopyflxSegHeader0c856c8bbd46448Controller");
        kony.mvc.registry.add("flxAccountswithImg", "flxAccountswithImg", "flxAccountswithImgController");
        kony.mvc.registry.add("flxHdrImageText", "flxHdrImageText", "flxHdrImageTextController");
        kony.mvc.registry.add("flxHeaderTran", "flxHeaderTran", "flxHeaderTranController");
        kony.mvc.registry.add("flexForNewAccount", "flexForNewAccount", "flexForNewAccountController");
        kony.mvc.registry.add("flxTransactions", "flxTransactions", "flxTransactionsController");
        kony.mvc.registry.add("flxTransactionsHeader", "flxTransactionsHeader", "flxTransactionsHeaderController");
        kony.mvc.registry.add("tempSwipe", "tempSwipe", "tempSwipeController");
        kony.mvc.registry.add("flxMain", "flxMain", "flxMainController");
        kony.mvc.registry.add("tempNew", "tempNew", "tempNewController");
        kony.mvc.registry.add("Form1", "Form1", "Form1Controller");
        kony.mvc.registry.add("frmAccountDetails", "frmAccountDetails", "frmAccountDetailsController");
        kony.mvc.registry.add("frmCapture", "frmCapture", "frmCaptureController");
        kony.mvc.registry.add("frmCaptureBack", "frmCaptureBack", "frmCaptureBackController");
        kony.mvc.registry.add("frmCardManagement", "frmCardManagement", "frmCardManagementController");
        kony.mvc.registry.add("frmCheck1", "frmCheck1", "frmCheck1Controller");
        kony.mvc.registry.add("frmCheck2", "frmCheck2", "frmCheck2Controller");
        kony.mvc.registry.add("frmCheckDepositEnterAmount", "frmCheckDepositEnterAmount", "frmCheckDepositEnterAmountController");
        kony.mvc.registry.add("frmCheckDepositFrom", "frmCheckDepositFrom", "frmCheckDepositFromController");
        kony.mvc.registry.add("frmChooseAgent", "frmChooseAgent", "frmChooseAgentController");
        kony.mvc.registry.add("frmConvo", "frmConvo", "frmConvoController");
        kony.mvc.registry.add("frmDashboardAggregated", "frmDashboardAggregated", "frmDashboardAggregatedController");
        kony.mvc.registry.add("frmDepositConformation", "frmDepositConformation", "frmDepositConformationController");
        kony.mvc.registry.add("frmGetstarted", "frmGetstarted", "frmGetstartedController");
        kony.mvc.registry.add("frmGetStarted2", "frmGetStarted2", "frmGetStarted2Controller");
        kony.mvc.registry.add("frmInterstitial", "frmInterstitial", "frmInterstitialController");
        kony.mvc.registry.add("frmLogin", "frmLogin", "frmLoginController");
        kony.mvc.registry.add("frmNAOVerify", "frmNAOVerify", "frmNAOVerifyController");
        kony.mvc.registry.add("frmPFM", "frmPFM", "frmPFMController");
        kony.mvc.registry.add("frmPPReviewAndSend", "frmPPReviewAndSend", "frmPPReviewAndSendController");
        kony.mvc.registry.add("frmPPSendSuccess", "frmPPSendSuccess", "frmPPSendSuccessController");
        kony.mvc.registry.add("frmScaning", "frmScaning", "frmScaningController");
        kony.mvc.registry.add("frmSendPPPayments", "frmSendPPPayments", "frmSendPPPaymentsController");
        kony.mvc.registry.add("frmSplash", "frmSplash", "frmSplashController");
        kony.mvc.registry.add("frmTransferAmount", "frmTransferAmount", "frmTransferAmountController");
        kony.mvc.registry.add("frmTransferFrom", "frmTransferFrom", "frmTransferFromController");
        kony.mvc.registry.add("frmTransferSuccess", "frmTransferSuccess", "frmTransferSuccessController");
        kony.mvc.registry.add("frmTransferTo", "frmTransferTo", "frmTransferToController");
        kony.mvc.registry.add("frmVerifyDeposit", "frmVerifyDeposit", "frmVerifyDepositController");
        kony.mvc.registry.add("frmVerifyTransferDetails", "frmVerifyTransferDetails", "frmVerifyTransferDetailsController");
        kony.mvc.registry.add("NewAccount/frmApplyNewAccount", "NewAccount/frmApplyNewAccount", "NewAccount/frmApplyNewAccountController");
        kony.mvc.registry.add("NewAccount/frmNAconfrmation", "NewAccount/frmNAconfrmation", "NewAccount/frmNAconfrmationController");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        new kony.mvc.Navigation("frmSplash").navigate();
    }
});