define("userfrmVerifyTransferDetailsController", {
    //Type your controller code here 
    setTransferValues: function() {
        var currDate = new Date();
        this.view.lblDate.text = ((currDate.getMonth() + 1) < 10 ? "0" + (currDate.getMonth() + 1) : (currDate.getMonth() + 1)) + "/" + (currDate.getDate() < 10 ? "0" + currDate.getDate() : currDate.getDate()) + "/" + currDate.getFullYear();
        this.view.lblAmount.text = gblEnteredTransferAmount;
        this.view.txtNotes.text = "";
        this.preShowFun();
        this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
        this.view.lblAvailableBalanceFrom.text = "Available Balance:" + gblSelectedFromAccountRow.lblBalance;
        this.view.lblSelectedToAccount.text = gblSelectedToAccountRow.lblAccountName;
        this.view.lblAvailableBalanceTo.text = "Available Balance:" + gblSelectedToAccountRow.lblBalance;
    },
    preShowFun: function() {
        this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
        this.view.lblAvailableBalanceFrom.text = gblSelectedFromAccountRow.lblBalance;
        this.view.lblSelectedToAccount.text = gblSelectedToAccountRow.lblAccountName;
        this.view.lblAvailableBalanceTo.text = gblSelectedToAccountRow.lblBalance;
    }
});
define("frmVerifyTransferDetailsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_i357d8219c1b45c5bad4e53339f4e413: function AS_FlexContainer_i357d8219c1b45c5bad4e53339f4e413(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_f6402ac3892045fab902e3df4042daf7: function AS_FlexContainer_f6402ac3892045fab902e3df4042daf7(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for bnTransfer **/
    AS_Button_a596153f46424611a6dd138bb1b3db8c: function AS_Button_a596153f46424611a6dd138bb1b3db8c(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferSuccess");
        ntf.navigate();
    },
    /** preShow defined for frmVerifyTransferDetails **/
    AS_Form_hf6eef420dd9483093ebc642ffc90d8b: function AS_Form_hf6eef420dd9483093ebc642ffc90d8b(eventobject) {
        var self = this;
        return self.setTransferValues.call(this);
    }
});
define("frmVerifyTransferDetailsController", ["userfrmVerifyTransferDetailsController", "frmVerifyTransferDetailsControllerActions"], function() {
    var controller = require("userfrmVerifyTransferDetailsController");
    var controllerActions = ["frmVerifyTransferDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
