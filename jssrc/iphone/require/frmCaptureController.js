define("userfrmCaptureController", {
    //Type your controller code here 
});
define("frmCaptureControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_d72570b7b47841ce8cc64ff3c44d71ed: function AS_Image_d72570b7b47841ce8cc64ff3c44d71ed(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositEnterAmount");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_d123d4adb2754026b6f0f04f7188b793: function AS_Label_d123d4adb2754026b6f0f04f7188b793(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for btnAutoCapture **/
    AS_Button_cdc5e444de5c421592359eab33402151: function AS_Button_cdc5e444de5c421592359eab33402151(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmVerifyDeposit");
        ntf.navigate();
    },
    /** onCapture defined for CamManual **/
    AS_Camera_j656ce0f116646a7a1c4c3b151099e85: function AS_Camera_j656ce0f116646a7a1c4c3b151099e85(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheck1");
        ntf.navigate();
    }
});
define("frmCaptureController", ["userfrmCaptureController", "frmCaptureControllerActions"], function() {
    var controller = require("userfrmCaptureController");
    var controllerActions = ["frmCaptureControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
