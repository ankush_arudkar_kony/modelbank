define("frmCheckDepositFrom", function() {
    return function(controller) {
        function addWidgetsfrmCheckDepositFrom() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxScrlWHiteBG",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var Image0fe32af4553f34e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "Image0fe32af4553f34e",
                "isVisible": true,
                "left": "15dp",
                "onTouchEnd": controller.AS_Image_i60f461d19084d3fbf9ca32147ce5370,
                "skin": "slImage",
                "src": "back.png",
                "width": "17dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0dd464373550946 = new kony.ui.Label({
                "centerY": "50%",
                "id": "Label0dd464373550946",
                "isVisible": true,
                "onTouchEnd": controller.AS_Label_aac15a892b324686a236d7c21014295c,
                "right": "15dp",
                "skin": "sknLblBlackSemiBold110",
                "text": "Cancel",
                "textStyle": {},
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            flxHeader.add(Image0fe32af4553f34e, Label0dd464373550946);
            var flxWelcome = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxWelcome",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWelcome.setDefaultUnit(kony.flex.DP);
            var lblPersonalChecking = new kony.ui.Label({
                "id": "lblPersonalChecking",
                "isVisible": true,
                "left": "15dp",
                "skin": "skinhundred",
                "text": "Deposit Account",
                "textStyle": {},
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var CopylblPersonalChecking0j74bf47189c841 = new kony.ui.Label({
                "id": "CopylblPersonalChecking0j74bf47189c841",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLblBlack95N",
                "text": "Select where you would like to deposit your check",
                "textStyle": {},
                "top": "2dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false,
                "wrapping": constants.WIDGET_TEXT_WORD_WRAP
            });
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "sknFlxLightGreyBg",
                "top": "5dp",
                "width": "91%",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var Image0jb49fbf2cd9d47 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "Image0jb49fbf2cd9d47",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "search_02.png",
                "width": "18dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAccountNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknTbxFocus",
                "height": "100%",
                "id": "txtAccountNumber",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10%",
                "placeholder": "Search by account name or number",
                "secureTextEntry": false,
                "skin": "sknTxtBxTrans",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
                "placeholderSkin": "sknTbxPlaceholder",
                "showClearButton": true,
                "showCloseButton": true,
                "showProgressIndicator": true,
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            flxSearch.add(Image0jb49fbf2cd9d47, txtAccountNumber);
            flxWelcome.add(lblPersonalChecking, CopylblPersonalChecking0j74bf47189c841, flxSearch);
            var flxPendingTransactions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxPendingTransactions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPendingTransactions.setDefaultUnit(kony.flex.DP);
            var flxCheckingAccounts = new com.demo.originbank.compSegHdrWithImgText({
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxCheckingAccounts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "isVisible": false,
                        "width": "100%"
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "acc_detailspic3.png",
                                    "lblAccountName": "John's Checking....x5678",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23,456.77",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Personal Checking....x4567",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$10,456.32",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Sam's Checking....x9156",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$12,345.45",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCheckingAccounts.segAccRecords.onRowClick = controller.AS_Segment_e5e08f19759a46dc9c902742c1d07924;
            var compSegHdrWithImgText = new com.demo.originbank.compSegHdrWithImgText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "compSegHdrWithImgText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "95%",
                "overrides": {
                    "compSegHdrWithImgText": {
                        "isVisible": true
                    },
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "money_icon.png",
                                    "lblAccount": "Checking Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "acc_detailspic3.png",
                                    "lblAccountName": "John's Checking....X5678",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$23,456.77",
                                    "lblBankName": "Bank of America",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Personal Checking....X4567",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$10,456.32",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Sam's Checking....X9156",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$12,345.45",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            compSegHdrWithImgText.segAccRecords.onRowClick = controller.AS_Segment_ae0e0b08f8d54894beb18bbc5c90e587;
            flxPendingTransactions.add(flxCheckingAccounts, compSegHdrWithImgText);
            var flxSavingsAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxSavingsAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-11dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSavingsAccounts.setDefaultUnit(kony.flex.DP);
            var flxSavingsAcounts = new com.demo.originbank.compSegHdrWithImgText({
                "centerX": "50%",
                "clipBounds": true,
                "height": "300dp",
                "id": "flxSavingsAcounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0d9c472a3e7a64a",
                "top": "0dp",
                "width": "95%",
                "overrides": {
                    "segAccRecords": {
                        "data": [
                            [{
                                    "imgAccountType": "savings.png",
                                    "lblAccount": "Savings Accounts",
                                    "lbllIne": ""
                                },
                                [{
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "Linda's Savings....x7878",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$2,346.67",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "citi.png",
                                    "lblAccountName": "Personal Savings....x9651",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$7,867.55",
                                    "lblBankName": "Citi Bank",
                                    "lbllIne": ""
                                }, {
                                    "imgBankLogo": "chase.png",
                                    "lblAccountName": "John's Savings....x6711",
                                    "lblAvailableBalance": "Available Balance",
                                    "lblBalance": "$8,956.44",
                                    "lblBankName": "Chase Bank",
                                    "lbllIne": ""
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSavingsAcounts.segAccRecords.onRowClick = controller.AS_Segment_jbd5ea44d90843fabf2b10467803eb74;
            flxSavingsAccounts.add(flxSavingsAcounts);
            flxMain.add(flxHeader, flxWelcome, flxPendingTransactions, flxSavingsAccounts);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCheckDepositFrom,
            "bounces": false,
            "enabledForIdleTimeout": false,
            "id": "frmCheckDepositFrom",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "sknFromBGWhite",
            "statusBarHidden": false
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "bounces": false,
            "configureExtendBottom": false,
            "configureExtendTop": false,
            "configureStatusBarStyle": true,
            "footerOverlap": false,
            "formTransparencyDuringPostShow": "100",
            "headerOverlap": false,
            "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
            "needsIndicatorDuringPostShow": false,
            "retainScrollPosition": false,
            "statusBarStyle": constants.STATUS_BAR_STYLE_DEFAULT,
            "titleBar": false,
            "titleBarSkin": "slTitleBar"
        }]
    }
});