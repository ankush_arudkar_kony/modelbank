define("userfrmScaningController", {
    showSplash: function() {
        kony.timer.schedule("splashTimer", this.navToWelcome, 2, false);
    },
    navToWelcome: function() {
            navToRespForm("frmVerifyDeposit");
        }
        //Type your controller code here 
});
define("frmScaningControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmScaning **/
    AS_Form_gc05465ba7df451c915e28863482ccbd: function AS_Form_gc05465ba7df451c915e28863482ccbd(eventobject) {
        var self = this;
        return self.showSplash.call(this);
    }
});
define("frmScaningController", ["userfrmScaningController", "frmScaningControllerActions"], function() {
    var controller = require("userfrmScaningController");
    var controllerActions = ["frmScaningControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
