define("com/demo/originbank/compSegHdrWithoutImgText/usercompSegHdrWithoutImgTextController", function() {
    return {};
});
define("com/demo/originbank/compSegHdrWithoutImgText/compSegHdrWithoutImgTextControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/demo/originbank/compSegHdrWithoutImgText/compSegHdrWithoutImgTextController", ["com/demo/originbank/compSegHdrWithoutImgText/usercompSegHdrWithoutImgTextController", "com/demo/originbank/compSegHdrWithoutImgText/compSegHdrWithoutImgTextControllerActions"], function() {
    var controller = require("com/demo/originbank/compSegHdrWithoutImgText/usercompSegHdrWithoutImgTextController");
    var actions = require("com/demo/originbank/compSegHdrWithoutImgText/compSegHdrWithoutImgTextControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
