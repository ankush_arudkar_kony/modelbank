define(function() {
    return function(controller) {
        var compSegHdrWithoutImgText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "300dp",
            "id": "compSegHdrWithoutImgText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0d9c472a3e7a64a",
            "top": "0dp",
            "width": "95%"
        }, controller.args[0], "compSegHdrWithoutImgText"), extendConfig({}, controller.args[1], "compSegHdrWithoutImgText"), extendConfig({}, controller.args[2], "compSegHdrWithoutImgText"));
        compSegHdrWithoutImgText.setDefaultUnit(kony.flex.DP);
        var segAccRecords = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "data": [
                [{
                        "imgAccountType": "money_icon.png",
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ],
                [{
                        "imgAccountType": "money_icon.png",
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ]
            ],
            "groupCells": false,
            "height": "85%",
            "id": "segAccRecords",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "CopyflxAccounts0ed79c7a8aaca4f",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxHdrImageText",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyflxAccounts0ed79c7a8aaca4f": "CopyflxAccounts0ed79c7a8aaca4f",
                "flxHdrImageText": "flxHdrImageText",
                "imgAccountType": "imgAccountType",
                "imgBankLogo": "imgBankLogo",
                "lblAccount": "lblAccount",
                "lblAccountName": "lblAccountName",
                "lblAvailableBalance": "lblAvailableBalance",
                "lblBalance": "lblBalance",
                "lblBankName": "lblBankName",
                "lbllIne": "lbllIne"
            },
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "segAccRecords"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccRecords"), extendConfig({
            "bounces": true,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segAccRecords"));
        compSegHdrWithoutImgText.add(segAccRecords);
        return compSegHdrWithoutImgText;
    }
})