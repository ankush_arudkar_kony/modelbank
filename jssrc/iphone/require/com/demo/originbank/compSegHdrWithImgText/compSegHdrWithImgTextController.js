define("com/demo/originbank/compSegHdrWithImgText/usercompSegHdrWithImgTextController", function() {
    return {};
});
define("com/demo/originbank/compSegHdrWithImgText/compSegHdrWithImgTextControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/demo/originbank/compSegHdrWithImgText/compSegHdrWithImgTextController", ["com/demo/originbank/compSegHdrWithImgText/usercompSegHdrWithImgTextController", "com/demo/originbank/compSegHdrWithImgText/compSegHdrWithImgTextControllerActions"], function() {
    var controller = require("com/demo/originbank/compSegHdrWithImgText/usercompSegHdrWithImgTextController");
    var actions = require("com/demo/originbank/compSegHdrWithImgText/compSegHdrWithImgTextControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
