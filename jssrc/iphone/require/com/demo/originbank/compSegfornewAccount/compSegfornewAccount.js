define(function() {
    return function(controller) {
        var compSegfornewAccount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "300dp",
            "id": "compSegfornewAccount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox2",
            "top": "0dp",
            "width": "95%"
        }, controller.args[0], "compSegfornewAccount"), extendConfig({}, controller.args[1], "compSegfornewAccount"), extendConfig({}, controller.args[2], "compSegfornewAccount"));
        compSegfornewAccount.setDefaultUnit(kony.flex.DP);
        var segAccRecords = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "data": [
                [{
                        "imgAccountType": "money_icon_2.png",
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgselection": "imagedrag.png",
                        "lbl1": "Pro Checking",
                        "lbl2": "Our most popular product offering,with...",
                        "lbl3": "View Details"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }]
                ],
                [{
                        "imgAccountType": "money_icon_2.png",
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }, {
                        "imgselection": "imagedrag.png",
                        "lbl1": "Label",
                        "lbl2": "Label",
                        "lbl3": "Label"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "85%",
            "id": "segAccRecords",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flexForNewAccount",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "CopyflxHdrImageText1",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgselection",
                "selectedStateImage": "radiobtn_active.png",
                "unselectedStateImage": "dotn_1.png"
            },
            "separatorColor": "aaaaaa4c",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyflxHdrImageText1": "CopyflxHdrImageText1",
                "flexForNewAccount": "flexForNewAccount",
                "imgAccountType": "imgAccountType",
                "imgselection": "imgselection",
                "lbl1": "lbl1",
                "lbl2": "lbl2",
                "lbl3": "lbl3",
                "lblAccount": "lblAccount",
                "lbllIne": "lbllIne"
            },
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "segAccRecords"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccRecords"), extendConfig({
            "bounces": false,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segAccRecords"));
        compSegfornewAccount.add(segAccRecords);
        return compSegfornewAccount;
    }
})