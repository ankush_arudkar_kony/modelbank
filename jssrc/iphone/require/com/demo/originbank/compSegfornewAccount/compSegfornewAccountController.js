define("com/demo/originbank/compSegfornewAccount/usercompSegfornewAccountController", function() {
    return {};
});
define("com/demo/originbank/compSegfornewAccount/compSegfornewAccountControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/demo/originbank/compSegfornewAccount/compSegfornewAccountController", ["com/demo/originbank/compSegfornewAccount/usercompSegfornewAccountController", "com/demo/originbank/compSegfornewAccount/compSegfornewAccountControllerActions"], function() {
    var controller = require("com/demo/originbank/compSegfornewAccount/usercompSegfornewAccountController");
    var actions = require("com/demo/originbank/compSegfornewAccount/compSegfornewAccountControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
