define("com/demo/dbx/flxSegPending/userflxSegPendingController", function() {
    return {};
});
define("com/demo/dbx/flxSegPending/flxSegPendingControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/demo/dbx/flxSegPending/flxSegPendingController", ["com/demo/dbx/flxSegPending/userflxSegPendingController", "com/demo/dbx/flxSegPending/flxSegPendingControllerActions"], function() {
    var controller = require("com/demo/dbx/flxSegPending/userflxSegPendingController");
    var actions = require("com/demo/dbx/flxSegPending/flxSegPendingControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
