define(function() {
    return function(controller) {
        var flxSegPending = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "300dp",
            "id": "flxSegPending",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0d9c472a3e7a64a",
            "top": "0dp",
            "width": "95%"
        }, controller.args[0], "flxSegPending"), extendConfig({}, controller.args[1], "flxSegPending"), extendConfig({}, controller.args[2], "flxSegPending"));
        flxSegPending.setDefaultUnit(kony.flex.DP);
        var segPendingTransactions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "data": [
                [{
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ],
                [{
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ]
            ],
            "groupCells": false,
            "height": "85%",
            "id": "segPendingTransactions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxAccountswithImg",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxHeaderTran",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccountswithImg": "flxAccountswithImg",
                "flxHeaderTran": "flxHeaderTran",
                "imgBankLogo": "imgBankLogo",
                "lblAccount": "lblAccount",
                "lblAccountName": "lblAccountName",
                "lblAvailableBalance": "lblAvailableBalance",
                "lblBalance": "lblBalance",
                "lblBankName": "lblBankName",
                "lbllIne": "lbllIne"
            },
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "segPendingTransactions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segPendingTransactions"), extendConfig({
            "bounces": true,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segPendingTransactions"));
        flxSegPending.add(segPendingTransactions);
        return flxSegPending;
    }
})