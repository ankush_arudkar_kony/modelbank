define(function() {
    return function(controller) {
        var flxSegPosted = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "isMaster": true,
            "id": "flxSegPosted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxShadow",
            "top": "0dp",
            "width": "90%"
        }, controller.args[0], "flxSegPosted"), extendConfig({}, controller.args[1], "flxSegPosted"), extendConfig({}, controller.args[2], "flxSegPosted"));
        flxSegPosted.setDefaultUnit(kony.flex.DP);
        var segPostedTransactions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "data": [
                [{
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ],
                [{
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ]
            ],
            "groupCells": false,
            "id": "segPostedTransactions",
            "isVisible": true,
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "Copyseg0a082efd2f93440",
            "rowTemplate": "flxAccountswithImg",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxHeaderTran",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccountswithImg": "flxAccountswithImg",
                "flxHeaderTran": "flxHeaderTran",
                "imgBankLogo": "imgBankLogo",
                "lblAccount": "lblAccount",
                "lblAccountName": "lblAccountName",
                "lblAvailableBalance": "lblAvailableBalance",
                "lblBalance": "lblBalance",
                "lblBankName": "lblBankName",
                "lbllIne": "lbllIne"
            },
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "segPostedTransactions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segPostedTransactions"), extendConfig({
            "bounces": true,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segPostedTransactions"));
        flxSegPosted.add(segPostedTransactions);
        return flxSegPosted;
    }
})