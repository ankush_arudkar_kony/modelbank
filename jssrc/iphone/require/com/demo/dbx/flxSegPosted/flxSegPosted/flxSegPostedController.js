define("com/demo/dbx/flxSegPosted/flxSegPosted/userflxSegPostedController", function() {
    return {};
});
define("com/demo/dbx/flxSegPosted/flxSegPosted/flxSegPostedControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/demo/dbx/flxSegPosted/flxSegPosted/flxSegPostedController", ["com/demo/dbx/flxSegPosted/flxSegPosted/userflxSegPostedController", "com/demo/dbx/flxSegPosted/flxSegPosted/flxSegPostedControllerActions"], function() {
    var controller = require("com/demo/dbx/flxSegPosted/flxSegPosted/userflxSegPostedController");
    var actions = require("com/demo/dbx/flxSegPosted/flxSegPosted/flxSegPostedControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
