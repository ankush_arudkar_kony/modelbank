define(function() {
    return function(controller) {
        var segComp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "248dp",
            "id": "segComp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "segComp"), extendConfig({}, controller.args[1], "segComp"), extendConfig({}, controller.args[2], "segComp"));
        segComp.setDefaultUnit(kony.flex.DP);
        var segAccounts = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "data": [
                [{
                        "imgLogo": "",
                        "lblAccount": "",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "",
                        "lblAccountName": "",
                        "lblAvailableBalance": "",
                        "lblBalance": "",
                        "lblBankName": "",
                        "lbllIne": ""
                    }]
                ]
            ],
            "groupCells": true,
            "height": "97.50%",
            "id": "segAccounts",
            "isVisible": true,
            "left": "20dp",
            "needPageIndicator": true,
            "onRowClick": controller.AS_onRowClick_d3064266a937431faf7d0e3a55cbdf4d,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxAccounts",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxSegHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "00000064",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccounts": "flxAccounts",
                "flxSegHeader": "flxSegHeader",
                "imgBankLogo": "imgBankLogo",
                "imgLogo": "imgLogo",
                "lblAccount": "lblAccount",
                "lblAccountName": "lblAccountName",
                "lblAvailableBalance": "lblAvailableBalance",
                "lblBalance": "lblBalance",
                "lblBankName": "lblBankName",
                "lbllIne": "lbllIne"
            },
            "width": "94%",
            "zIndex": 1
        }, controller.args[0], "segAccounts"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccounts"), extendConfig({
            "bounces": false,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segAccounts"));
        segComp.add(segAccounts);
        return segComp;
    }
})