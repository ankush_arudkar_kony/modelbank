define("com/comp/segComp/usersegCompController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {}
    };
});
define("com/comp/segComp/segCompControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/comp/segComp/segCompController", ["com/comp/segComp/usersegCompController", "com/comp/segComp/segCompControllerActions"], function() {
    var controller = require("com/comp/segComp/usersegCompController");
    var actions = require("com/comp/segComp/segCompControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "data", function(val) {
            this.view.segAccounts.data = val;
        });
        defineGetter(this, "data", function() {
            return this.view.segAccounts.data;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.addAll = function() {
        var wModel = this.view.segAccounts;
        return wModel.addAll.apply(wModel, arguments);
    };
    controller.setData = function() {
        var wModel = this.view.segAccounts;
        return wModel.setData.apply(wModel, arguments);
    };
    controller.setAnimations = function() {
        var wModel = this.view.segAccounts;
        return wModel.setAnimations.apply(wModel, arguments);
    };
    controller.removeAll = function() {
        var wModel = this.view.segAccounts;
        return wModel.removeAll.apply(wModel, arguments);
    };
    controller.AS_onRowClick_d3064266a937431faf7d0e3a55cbdf4d = function() {
        if (this.onRowClick) {
            this.onRowClick.apply(this, arguments);
        }
    }
    return controller;
});
