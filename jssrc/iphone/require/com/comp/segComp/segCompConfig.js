define(function() {
    return {
        "properties": [{
            "name": "data",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["addAll", "setData", "setAnimations", "removeAll"],
        "events": ["onRowClick"]
    }
});