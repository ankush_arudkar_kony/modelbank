define(function() {
    return function(controller) {
        var donutchart5 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "donutchart5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0fb230441e9ea4b",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "donutchart5"), extendConfig({}, controller.args[1], "donutchart5"), extendConfig({}, controller.args[2], "donutchart5"));
        donutchart5.setDefaultUnit(kony.flex.DP);
        var doughnutBrowser = new kony.ui.Browser(extendConfig({
            "detectTelNumber": false,
            "enableZoom": false,
            "height": "100%",
            "id": "doughnutBrowser",
            "isVisible": true,
            "left": "0dp",
            "requestURLConfig": {
                "URL": "CopyCopyChart_donut3/donut.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "doughnutBrowser"), extendConfig({}, controller.args[1], "doughnutBrowser"), extendConfig({}, controller.args[2], "doughnutBrowser"));
        donutchart5.add(doughnutBrowser);
        return donutchart5;
    }
})