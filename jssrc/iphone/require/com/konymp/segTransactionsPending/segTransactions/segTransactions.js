define(function() {
    return function(controller) {
        var segTransactions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "248dp",
            "id": "segTransactions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "CopyslFbox0h24c1377fff641",
            "top": "20dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "segTransactions"), extendConfig({}, controller.args[1], "segTransactions"), extendConfig({}, controller.args[2], "segTransactions"));
        segTransactions.setDefaultUnit(kony.flex.DP);
        var segPending = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "data": [
                [{
                        "lblAccount": "Label",
                        "lbllIne": ""
                    },
                    [{
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }, {
                        "imgBankLogo": "imagedrag.png",
                        "lblAccountName": "Label",
                        "lblAvailableBalance": "Label",
                        "lblBalance": "Label",
                        "lblBankName": "Label",
                        "lbllIne": ""
                    }]
                ]
            ],
            "groupCells": false,
            "height": "88%",
            "id": "segPending",
            "isVisible": true,
            "left": "20dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknSegPendingBg",
            "rowTemplate": "flxAccounts",
            "scrollingEvents": {},
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxTransactionsHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "ededed00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccounts": "flxAccounts",
                "flxTransactionsHeader": "flxTransactionsHeader",
                "imgBankLogo": "imgBankLogo",
                "lblAccount": "lblAccount",
                "lblAccountName": "lblAccountName",
                "lblAvailableBalance": "lblAvailableBalance",
                "lblBalance": "lblBalance",
                "lblBankName": "lblBankName",
                "lbllIne": "lbllIne"
            },
            "zIndex": 1
        }, controller.args[0], "segPending"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segPending"), extendConfig({
            "bounces": true,
            "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
            "enableDictionary": false,
            "indicator": constants.SEGUI_NONE,
            "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
            "showProgressIndicator": true
        }, controller.args[2], "segPending"));
        segTransactions.add(segPending);
        return segTransactions;
    }
})