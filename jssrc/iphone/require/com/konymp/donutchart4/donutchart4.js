define(function() {
    return function(controller) {
        var donutchart4 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "donutchart4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0fb230441e9ea4b",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "donutchart4"), extendConfig({}, controller.args[1], "donutchart4"), extendConfig({}, controller.args[2], "donutchart4"));
        donutchart4.setDefaultUnit(kony.flex.DP);
        var doughnutBrowser = new kony.ui.Browser(extendConfig({
            "detectTelNumber": false,
            "enableZoom": false,
            "height": "100%",
            "id": "doughnutBrowser",
            "isVisible": true,
            "left": "0dp",
            "requestURLConfig": {
                "URL": "CopyCopyChart_donut2/donut.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "doughnutBrowser"), extendConfig({}, controller.args[1], "doughnutBrowser"), extendConfig({}, controller.args[2], "doughnutBrowser"));
        donutchart4.add(doughnutBrowser);
        return donutchart4;
    }
})