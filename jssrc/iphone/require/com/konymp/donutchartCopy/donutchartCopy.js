define(function() {
    return function(controller) {
        var donutchartCopy = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "donutchartCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0fb230441e9ea4b",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "donutchartCopy"), extendConfig({}, controller.args[1], "donutchartCopy"), extendConfig({}, controller.args[2], "donutchartCopy"));
        donutchartCopy.setDefaultUnit(kony.flex.DP);
        var doughnutBrowser = new kony.ui.Browser(extendConfig({
            "detectTelNumber": false,
            "enableZoom": false,
            "height": "100%",
            "id": "doughnutBrowser",
            "isVisible": true,
            "left": "0dp",
            "requestURLConfig": {
                "URL": "CopyChart_donut2/donut.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "doughnutBrowser"), extendConfig({}, controller.args[1], "doughnutBrowser"), extendConfig({}, controller.args[2], "doughnutBrowser"));
        donutchartCopy.add(doughnutBrowser);
        return donutchartCopy;
    }
})