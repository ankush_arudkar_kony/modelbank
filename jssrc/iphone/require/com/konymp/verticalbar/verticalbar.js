define(function() {
    return function(controller) {
        var verticalbar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "verticalbar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "verticalbar"), extendConfig({}, controller.args[1], "verticalbar"), extendConfig({}, controller.args[2], "verticalbar"));
        verticalbar.setDefaultUnit(kony.flex.DP);
        var verticalBarBrowser = new kony.ui.Browser(extendConfig({
            "detectTelNumber": true,
            "enableNativeCommunication": true,
            "enableZoom": false,
            "height": "100%",
            "id": "verticalBarBrowser",
            "isVisible": true,
            "left": "0dp",
            "requestURLConfig": {
                "URL": "CopyChart_vertical/verticalBarChartist.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "verticalBarBrowser"), extendConfig({}, controller.args[1], "verticalBarBrowser"), extendConfig({}, controller.args[2], "verticalBarBrowser"));
        verticalbar.add(verticalBarBrowser);
        return verticalbar;
    }
})