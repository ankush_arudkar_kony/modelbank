define("com/konymp/segTransactions/segTransactions/usersegTransactionsController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {}
    };
});
define("com/konymp/segTransactions/segTransactions/segTransactionsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/segTransactions/segTransactions/segTransactionsController", ["com/konymp/segTransactions/segTransactions/usersegTransactionsController", "com/konymp/segTransactions/segTransactions/segTransactionsControllerActions"], function() {
    var controller = require("com/konymp/segTransactions/segTransactions/usersegTransactionsController");
    var actions = require("com/konymp/segTransactions/segTransactions/segTransactionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "data", function(val) {
            this.view.segPending.data = val;
        });
        defineGetter(this, "data", function() {
            return this.view.segPending.data;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.setData = function() {
        var wModel = this.view.segPending;
        return wModel.setData.apply(wModel, arguments);
    };
    controller.setAnimations = function() {
        var wModel = this.view.segPending;
        return wModel.setAnimations.apply(wModel, arguments);
    };
    controller.removeAll = function() {
        var wModel = this.view.segPending;
        return wModel.removeAll.apply(wModel, arguments);
    };
    controller.addAll = function() {
        var wModel = this.view.segPending;
        return wModel.addAll.apply(wModel, arguments);
    };
    return controller;
});
