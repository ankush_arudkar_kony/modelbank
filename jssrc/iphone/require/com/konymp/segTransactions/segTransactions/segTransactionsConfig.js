define(function() {
    return {
        "properties": [{
            "name": "data",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["setData", "setAnimations", "removeAll", "addAll"],
        "events": []
    }
});