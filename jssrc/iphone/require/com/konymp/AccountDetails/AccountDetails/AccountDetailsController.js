define("com/konymp/AccountDetails/AccountDetails/userAccountDetailsController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {}
    };
});
define("com/konymp/AccountDetails/AccountDetails/AccountDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/AccountDetails/AccountDetails/AccountDetailsController", ["com/konymp/AccountDetails/AccountDetails/userAccountDetailsController", "com/konymp/AccountDetails/AccountDetails/AccountDetailsControllerActions"], function() {
    var controller = require("com/konymp/AccountDetails/AccountDetails/userAccountDetailsController");
    var actions = require("com/konymp/AccountDetails/AccountDetails/AccountDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
