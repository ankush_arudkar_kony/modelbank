define(function() {
    return function(controller) {
        var AccountDetails = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "220dp",
            "id": "AccountDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "100dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "AccountDetails"), extendConfig({}, controller.args[1], "AccountDetails"), extendConfig({}, controller.args[2], "AccountDetails"));
        AccountDetails.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "165dp",
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-2dp",
            "isModalContainer": false,
            "skin": "sknGreyFlxLeft",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        flxLeft.add();
        var flxDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "220dp",
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0gafe8cc308c94b",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "flxDetails"), extendConfig({}, controller.args[1], "flxDetails"), extendConfig({}, controller.args[2], "flxDetails"));
        flxDetails.setDefaultUnit(kony.flex.DP);
        var flxBalance = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "65dp",
            "id": "flxBalance",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "90%"
        }, controller.args[0], "flxBalance"), extendConfig({}, controller.args[1], "flxBalance"), extendConfig({}, controller.args[2], "flxBalance"));
        flxBalance.setDefaultUnit(kony.flex.DP);
        var lblCurrentBalance = new kony.ui.Label(extendConfig({
            "id": "lblCurrentBalance",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopysknlblAccountName0b53dc9ad166e46",
            "text": "Current Balance",
            "textStyle": {},
            "top": "15dp",
            "width": "115dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrentBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrentBalance"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblCurrentBalance"));
        var imgDetails = new kony.ui.Image2(extendConfig({
            "height": "18dp",
            "id": "imgDetails",
            "isVisible": true,
            "left": "120dp",
            "skin": "slImage",
            "src": "info_blue.png",
            "top": "15dp",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "imgDetails"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDetails"), extendConfig({}, controller.args[2], "imgDetails"));
        var lblBalance = new kony.ui.Label(extendConfig({
            "id": "lblBalance",
            "isVisible": true,
            "right": "15dp",
            "skin": "CopysknlblAccountName0b53dc9ad166e46",
            "text": "$65,034.56",
            "textStyle": {},
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBalance"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblBalance"));
        var lblAvailable = new kony.ui.Label(extendConfig({
            "bottom": "15dp",
            "id": "lblAvailable",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopysknlblBankName0e1656cb1610e48",
            "text": "Available Balance",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailable"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailable"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblAvailable"));
        var lblAvailableBalance = new kony.ui.Label(extendConfig({
            "bottom": "15dp",
            "id": "lblAvailableBalance",
            "isVisible": true,
            "right": "15dp",
            "skin": "CopysknlblBankName0e1656cb1610e48",
            "text": "$55,860.66",
            "textStyle": {},
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableBalance"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblAvailableBalance"));
        flxBalance.add(lblCurrentBalance, imgDetails, lblBalance, lblAvailable, lblAvailableBalance);
        var flxLinks = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "centerX": "50%",
            "clipBounds": false,
            "height": "90dp",
            "id": "flxLinks",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknFooterBg0ie06cae4889f49",
            "top": "90dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "flxLinks"), extendConfig({}, controller.args[1], "flxLinks"), extendConfig({}, controller.args[2], "flxLinks"));
        flxLinks.setDefaultUnit(kony.flex.DP);
        var flxTransferFunds = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTransferFunds",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxTransferFunds"), extendConfig({}, controller.args[1], "flxTransferFunds"), extendConfig({}, controller.args[2], "flxTransferFunds"));
        flxTransferFunds.setDefaultUnit(kony.flex.DP);
        var imgTransferFunds = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "55dp",
            "id": "imgTransferFunds",
            "isVisible": true,
            "skin": "slImage",
            "src": "transfer_funds.png",
            "top": "5dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "imgTransferFunds"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgTransferFunds"), extendConfig({}, controller.args[2], "imgTransferFunds"));
        var lblTransferFunds = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "centerX": "50%",
            "id": "lblTransferFunds",
            "isVisible": true,
            "skin": "CopydefLabel0f3bc2b7ff2bb42",
            "text": "Transfer Funds",
            "textStyle": {},
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "lblTransferFunds"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTransferFunds"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblTransferFunds"));
        flxTransferFunds.add(imgTransferFunds, lblTransferFunds);
        var flxDepositCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDepositCheck",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxDepositCheck"), extendConfig({}, controller.args[1], "flxDepositCheck"), extendConfig({}, controller.args[2], "flxDepositCheck"));
        flxDepositCheck.setDefaultUnit(kony.flex.DP);
        var imgDeposit = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "55dp",
            "id": "imgDeposit",
            "isVisible": true,
            "skin": "slImage",
            "src": "deposit_check.png",
            "top": "5dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "imgDeposit"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDeposit"), extendConfig({}, controller.args[2], "imgDeposit"));
        var lblDeposit = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "centerX": "50%",
            "id": "lblDeposit",
            "isVisible": true,
            "skin": "CopydefLabel0f3bc2b7ff2bb42",
            "text": "Deposit Check",
            "textStyle": {},
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDeposit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeposit"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblDeposit"));
        flxDepositCheck.add(imgDeposit, lblDeposit);
        var flxCashWithdrawal = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCashWithdrawal",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxCashWithdrawal"), extendConfig({}, controller.args[1], "flxCashWithdrawal"), extendConfig({}, controller.args[2], "flxCashWithdrawal"));
        flxCashWithdrawal.setDefaultUnit(kony.flex.DP);
        var imgCashWithdrawal = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "55dp",
            "id": "imgCashWithdrawal",
            "isVisible": true,
            "skin": "slImage",
            "src": "cash_withdrawal.png",
            "top": "5dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "imgCashWithdrawal"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCashWithdrawal"), extendConfig({}, controller.args[2], "imgCashWithdrawal"));
        var lblCashWithdrawal = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "centerX": "50%",
            "id": "lblCashWithdrawal",
            "isVisible": true,
            "skin": "CopydefLabel0f3bc2b7ff2bb42",
            "text": "   Cash    Withdrawal",
            "textStyle": {},
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblCashWithdrawal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCashWithdrawal"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblCashWithdrawal"));
        flxCashWithdrawal.add(imgCashWithdrawal, lblCashWithdrawal);
        var flxViewStatements = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxViewStatements",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxViewStatements"), extendConfig({}, controller.args[1], "flxViewStatements"), extendConfig({}, controller.args[2], "flxViewStatements"));
        flxViewStatements.setDefaultUnit(kony.flex.DP);
        var imgViewStatements = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "55dp",
            "id": "imgViewStatements",
            "isVisible": true,
            "skin": "slImage",
            "src": "view_statements.png",
            "top": "5dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "imgViewStatements"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgViewStatements"), extendConfig({}, controller.args[2], "imgViewStatements"));
        var lblViewStatements = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "centerX": "50%",
            "id": "lblViewStatements",
            "isVisible": true,
            "skin": "CopydefLabel0f3bc2b7ff2bb42",
            "text": "View Statements",
            "textStyle": {},
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblViewStatements"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewStatements"), extendConfig({
            "textCopyable": false,
            "wrapping": constants.WIDGET_TEXT_WORD_WRAP
        }, controller.args[2], "lblViewStatements"));
        flxViewStatements.add(imgViewStatements, lblViewStatements);
        flxLinks.add(flxTransferFunds, flxDepositCheck, flxCashWithdrawal, flxViewStatements);
        flxDetails.add(flxBalance, flxLinks);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "165dp",
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "-2dp",
            "skin": "sknGreyFlxLeft",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        flxRight.add();
        AccountDetails.add(flxLeft, flxDetails, flxRight);
        return AccountDetails;
    }
})