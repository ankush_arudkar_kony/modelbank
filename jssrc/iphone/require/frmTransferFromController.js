define("userfrmTransferFromController", {
    //Type your controller code here 
});
define("frmTransferFromControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segAccRecords **/
    AS_Segment_e5e08f19759a46dc9c902742c1d07924: function AS_Segment_e5e08f19759a46dc9c902742c1d07924(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedFromAccountRow = this.view.flxCheckingAccounts.segAccRecords.selectedRowItems[0];
        navToRespForm("frmTransferTo");
    },
    /** onClick defined for flxBack **/
    AS_FlexContainer_ge606460c8d14e289c1699b7909e0338: function AS_FlexContainer_ge606460c8d14e289c1699b7909e0338(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_ff17ef94b2d6430da622547313bd63b6: function AS_FlexContainer_ff17ef94b2d6430da622547313bd63b6(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_cc896831a15c4ebb9fa4ebe074703a2a: function AS_Segment_cc896831a15c4ebb9fa4ebe074703a2a(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedFromAccountRow = this.view.flxSavingsAcounts.segAccRecords.selectedRowItems[0];
        var ntf = new kony.mvc.Navigation("frmTransferTo");
        ntf.navigate();
    }
});
define("frmTransferFromController", ["userfrmTransferFromController", "frmTransferFromControllerActions"], function() {
    var controller = require("userfrmTransferFromController");
    var controllerActions = ["frmTransferFromControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
