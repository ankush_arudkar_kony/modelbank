define("userfrmConvoController", {
    //Type your controller code here 
    onNavigate: function(tmpAgentId) {
        if (rtrn_flg) {
            if (prev_agent == tmpAgentId.name) {} else {
                //tmpNxtAgent =  tmpAgentId.name;
                this.setTimerNewAgentChat();
                //this.view.flxJeanChat.isVisible = true; 
                this.view.lblAgentLeft.text = prev_agent + " left the conversation";
                /*this.view.imgagent.src = tmpAgentId.profileimg;
    			this.view.lblName.text = tmpAgentId.name;
    			this.view.lblFav.text = tmpAgentId.traveler;
    			this.view.agentImg1.src = tmpAgentId.profileimg;
    			this.view.imgAgent2.src = tmpAgentId.profileimg;
    			this.view.lblKim.text = tmpAgentId.name;
		    	this.view.lblAgent2.text = tmpAgentId.name;
        		this.view.imgChat.src = tmpAgentId.profileimg;
    			this.view.lblJoinedConvo.text = tmpAgentId.name +" joined the conversation";
        		this.view.lblChoosename.text= tmpAgentId.name;*/
                this.view.lblAgentJoined.text = tmpAgentId.name + " joined the conversation";
                this.view.lblAgentName.text = tmpAgentId.name;
                this.view.lblAgentFav.text = tmpAgentId.traveler;
                this.view.imgJoinednew.src = tmpAgentId.profileimg;
                this.view.imgJoinedChat.src = tmpAgentId.profileimg;
                this.view.lblChoosename.text = tmpAgentId.name;
                this.view.imgNxtProfile.src = tmpAgentId.profileimg;
            }
        } else {
            prev_agent = tmpAgentId.name;
            //tmpNxtAgent = tmpAgentId.name;
            this.view.flxJeanChat.isVisible = false;
            this.view.imgagent.src = tmpAgentId.profileimg;
            this.view.lblName.text = tmpAgentId.name;
            this.view.lblFav.text = tmpAgentId.traveler;
            this.view.agentImg1.src = tmpAgentId.profileimg;
            this.view.imgAgent2.src = tmpAgentId.profileimg;
            this.view.lblKim.text = tmpAgentId.name;
            this.view.lblAgent2.text = tmpAgentId.name;
            this.view.imgChat.src = tmpAgentId.profileimg;
            this.view.lblJoinedConvo.text = tmpAgentId.name + " joined the conversation";
            this.view.lblChoosename.text = tmpAgentId.name;
            this.view.imgJoinednew.src = tmpAgentId.profileimg;
            this.view.imgNxtProfile.src = tmpAgentId.profileimg;
        }
    },
    setTimerAgentChat: function() {
        kony.timer.schedule("Agent1", this.agentChat1, 2, false);
        kony.timer.schedule("Agent2", this.agentChat2, 4, false);
        kony.timer.schedule("Agent3", this.agentChat3, 6, false);
    },
    setTimerNewAgentChat: function() {
        kony.timer.schedule("Agent4", this.agentChat4, 2, false);
    },
    agentChat1: function() {
        this.view.flxAgentChat1.isVisible = true;
    },
    agentChat2: function() {
        this.view.flxCustChat1.isVisible = true;
    },
    agentChat3: function() {
        this.view.flxAgentChat2.isVisible = true;
    },
    agentChat4: function() {
        this.view.flxScroll.scrollToEnd();
        this.view.flxJeanChat.isVisible = true;
    },
    timerFun: function() {
        var d1 = new Date();
        if ((d1.getMonth() + 1) > 9) this.view.lblTime.text = (d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear() + ", " + d1.getHours() + ":" + d1.getMinutes();
        else this.view.lblTime.text = ("0" + (d1.getMonth() + 1)) + "/" + d1.getDate() + "/" + d1.getFullYear() + ", " + d1.getHours() + ":" + d1.getMinutes();
    }
});
define("frmConvoControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for imgsearch **/
    AS_Image_f3127fc8a1454829994528d1ae5fbd53: function AS_Image_f3127fc8a1454829994528d1ae5fbd53(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____a8120f62fa6643bebe3196cac433d79f_Callback() {}
        this.view.flxGrey.isVisible = true;
        self.view.flxMatter.animate(kony.ui.createAnimation({
            "100": {
                "bottom": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "duration": 0.2,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": MOVE_ACTION____a8120f62fa6643bebe3196cac433d79f_Callback
        });
    },
    /** onTouchEnd defined for imgBack **/
    AS_Image_c1673b6f386846d5b2d2f5d9890078db: function AS_Image_c1673b6f386846d5b2d2f5d9890078db(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onTouchStart defined for Image0f1bd344cca184f **/
    AS_Image_g27593d7408f4d9da68c54a9f74ac475: function AS_Image_g27593d7408f4d9da68c54a9f74ac475(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____h12122f3f3f649a4b5e0cfcbdcaa6e38_Callback() {}
        this.view.flxGrey.isVisible = false;
        self.view.Image0f1bd344cca184f.animate(kony.ui.createAnimation({
            "100": {
                "bottom": "-30%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "duration": 0.25,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": MOVE_ACTION____h12122f3f3f649a4b5e0cfcbdcaa6e38_Callback
        });
    },
    /** onClick defined for flxChoosePrimary **/
    AS_FlexContainer_baf6aa140c084d69b6b4fb095a8bfa13: function AS_FlexContainer_baf6aa140c084d69b6b4fb095a8bfa13(eventobject) {
        var self = this;
        rtrn_flg = true;
        this.view.flxGrey.isVisible = false;
        this.view.flxMatter.bottom = "-28%";
        var n1 = new kony.mvc.Navigation("frmChooseAgent");
        n1.navigate();
    },
    /** preShow defined for frmConvo **/
    AS_Form_c287733b202b48f3a4d56ce0d9892f11: function AS_Form_c287733b202b48f3a4d56ce0d9892f11(eventobject) {
        var self = this;
        this.timerFun();
    },
    /** postShow defined for frmConvo **/
    AS_Form_c7213f103bc14a38b4eef305d86f0dab: function AS_Form_c7213f103bc14a38b4eef305d86f0dab(eventobject) {
        var self = this;
        this.setTimerAgentChat();
    }
});
define("frmConvoController", ["userfrmConvoController", "frmConvoControllerActions"], function() {
    var controller = require("userfrmConvoController");
    var controllerActions = ["frmConvoControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
