define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onDone defined for txtUsername **/
    AS_TextField_fe8d91e8deae4a4b83259f9dadb73ec5: function AS_TextField_fe8d91e8deae4a4b83259f9dadb73ec5(eventobject, changedtext) {
        var self = this;
        this.view.flxLine.height = "1dp";
        this.view.flxLine.skin = "sknFlxLineBg";
    },
    /** onTouchStart defined for txtUsername **/
    AS_TextField_g11ec17e4bb3465aa3de2b0563476bee: function AS_TextField_g11ec17e4bb3465aa3de2b0563476bee(eventobject, x, y) {
        var self = this;
        this.view.flxLine.height = "2dp";
        this.view.flxLine.skin = "sknFlxLineGrad";
        this.view.flxLine2.height = "1dp";
        this.view.flxLine2.skin = "sknFlxLineBg";
    },
    /** onEndEditing defined for txtUsername **/
    AS_TextField_a17dfda1e4364693b46e70212534b6ed: function AS_TextField_a17dfda1e4364693b46e70212534b6ed(eventobject, changedtext) {
        var self = this;
        this.view.flxLine.height = "1dp";
        this.view.flxLine.skin = "sknFlxLineBg";
    },
    /** onEndEditing defined for txtUsername **/
    AS_TextField_g7f95e5cdb8741aead8d8274aabb2f03: function AS_TextField_g7f95e5cdb8741aead8d8274aabb2f03(eventobject, changedtext) {
        var self = this;
        this.view.flxLine.height = "1dp";
        this.view.flxLine.skin = "sknFlxLineBg";
    },
    /** onDone defined for txtPassword **/
    AS_TextField_deb7d49ed2674c89a6d83f40fe6a976e: function AS_TextField_deb7d49ed2674c89a6d83f40fe6a976e(eventobject, changedtext) {
        var self = this;
        this.view.flxLine2.height = "1dp";
        this.view.flxLine2.skin = "sknFlxLineBg";
    },
    /** onTouchStart defined for txtPassword **/
    AS_TextField_i7e0f7d569664948877d7913924f3ebe: function AS_TextField_i7e0f7d569664948877d7913924f3ebe(eventobject, x, y) {
        var self = this;
        this.view.flxLine2.height = "2dp";
        this.view.flxLine2.skin = "sknFlxLineGrad";
        this.view.flxLine.height = "1dp";
        this.view.flxLine.skin = "sknFlxLineBg";
    },
    /** onEndEditing defined for txtPassword **/
    AS_TextField_d302524abdb041c2b3b1b94d47ede40b: function AS_TextField_d302524abdb041c2b3b1b94d47ede40b(eventobject, changedtext) {
        var self = this;
        this.view.flxLine2.height = "1dp";
        this.view.flxLine2.skin = "sknFlxLineBg";
    },
    /** onEndEditing defined for txtPassword **/
    AS_TextField_cab194a051674fa1b190bbc107801991: function AS_TextField_cab194a051674fa1b190bbc107801991(eventobject, changedtext) {
        var self = this;
        this.view.flxLine2.height = "1dp";
        this.view.flxLine2.skin = "sknFlxLineBg";
    },
    /** onTouchEnd defined for imgRememberMe **/
    AS_Image_d43618599d5946afbc25f1856b09782c: function AS_Image_d43618599d5946afbc25f1856b09782c(eventobject, x, y) {
        var self = this;
        if (this.view.imgRememberMe.src == "switchon.png") {
            this.view.imgRememberMe.src = "switchoff.png";
        } else {
            this.view.imgRememberMe.src = "switchon.png";
        }
    },
    /** onClick defined for btnSignIn **/
    AS_Button_d1ad2f6bd8aa4bfe807fa9853ddd7161: function AS_Button_d1ad2f6bd8aa4bfe807fa9853ddd7161(eventobject) {
        var self = this;
        navToRespForm("frmInterstitial");
    },
    /** preShow defined for frmLogin **/
    AS_Form_e6e608fa2e004c85b0be369cc686bc28: function AS_Form_e6e608fa2e004c85b0be369cc686bc28(eventobject) {
        var self = this;
        return changebgperTime.call(this);
    }
});