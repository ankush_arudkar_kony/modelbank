define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_a69c2137f78c4f34876d08c313fbb2f2: function AS_FlexContainer_a69c2137f78c4f34876d08c313fbb2f2(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_e8fcde6c659e47409bdbe20d054d3fd3: function AS_FlexContainer_e8fcde6c659e47409bdbe20d054d3fd3(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onTouchEnd defined for imgRememberMe **/
    AS_Image_jf27a24de72f41818eb308b82ecab205: function AS_Image_jf27a24de72f41818eb308b82ecab205(eventobject, x, y) {
        var self = this;
        if (this.view.imgRememberMe.src == "switchoff.png") {
            this.view.imgRememberMe.src = "switchon.png";
            this.view.flxCardActions.isVisible = false;
        } else {
            this.view.imgRememberMe.src = "switchoff.png";
            this.view.flxCardActions.isVisible = true;
        }
    },
    /** preShow defined for frmCardManagement **/
    AS_Form_i6d4a3dbe8fb4007be5ef377b789337c: function AS_Form_i6d4a3dbe8fb4007be5ef377b789337c(eventobject) {
        var self = this;
        this.preshow();
    }
});