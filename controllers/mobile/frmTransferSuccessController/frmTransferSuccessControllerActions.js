define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for bnNewTransfer **/
    AS_Button_dbe02e714f9747ee8837527466a14169: function AS_Button_dbe02e714f9747ee8837527466a14169(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** onClick defined for bnMyAccounts **/
    AS_Button_f133a38ddb784adc8dc8f18da726afcb: function AS_Button_f133a38ddb784adc8dc8f18da726afcb(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** preShow defined for frmTransferSuccess **/
    AS_Form_a258a14efc794c00a6feddab2c8863fb: function AS_Form_a258a14efc794c00a6feddab2c8863fb(eventobject) {
        var self = this;
        return self.preShowFun.call(this);
    }
});