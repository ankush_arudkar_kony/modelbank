define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_i357d8219c1b45c5bad4e53339f4e413: function AS_FlexContainer_i357d8219c1b45c5bad4e53339f4e413(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_f6402ac3892045fab902e3df4042daf7: function AS_FlexContainer_f6402ac3892045fab902e3df4042daf7(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onClick defined for bnTransfer **/
    AS_Button_a596153f46424611a6dd138bb1b3db8c: function AS_Button_a596153f46424611a6dd138bb1b3db8c(eventobject) {
        var self = this;
        if (this.view.txtNotes.text) {
            TransNote = this.view.txtNotes.text;
            this.view.txtNotes.text = " ";
        } else {
            TransNote = " ";
        }
        var ntf = new kony.mvc.Navigation("frmTransferSuccess");
        ntf.navigate();
    },
    /** preShow defined for frmVerifyTransferDetails **/
    AS_Form_hf6eef420dd9483093ebc642ffc90d8b: function AS_Form_hf6eef420dd9483093ebc642ffc90d8b(eventobject) {
        var self = this;
        return self.setTransferValues.call(this);
    }
});