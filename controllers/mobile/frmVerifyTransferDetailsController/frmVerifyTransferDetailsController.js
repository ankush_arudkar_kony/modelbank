define({ 

 //Type your controller code here 
setTransferValues:function(){
  var currDate = new Date();
  this.view.lblDate.text = ((currDate.getMonth()+1) < 10 ? "0"+(currDate.getMonth()+1): (currDate.getMonth()+1) )+"/"+(currDate.getDate() < 10 ? "0"+currDate.getDate(): currDate.getDate())+"/"+currDate.getFullYear();
  this.view.lblAmount.text = gblEnteredTransferAmount;
  this.view.txtNotes.text = "";
  this.preShowFun();
   this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
    this.view.lblAvailableBalanceFrom.text = "Available Balance:"+gblSelectedFromAccountRow.lblBalance;
    this.view.lblSelectedToAccount.text = gblSelectedToAccountRow.lblAccountName;
    this.view.lblAvailableBalanceTo.text = "Available Balance:"+gblSelectedToAccountRow.lblBalance;
},
	preShowFun:function(){
		this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
        this.view.lblAvailableBalanceFrom.text = gblSelectedFromAccountRow.lblBalance;
        this.view.lblSelectedToAccount.text = gblSelectedToAccountRow.lblAccountName;
        this.view.lblAvailableBalanceTo.text = gblSelectedToAccountRow.lblBalance;
	}
 });