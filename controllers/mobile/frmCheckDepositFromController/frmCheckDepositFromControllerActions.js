define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0fe32af4553f34e **/
    AS_Image_i60f461d19084d3fbf9ca32147ce5370: function AS_Image_i60f461d19084d3fbf9ca32147ce5370(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_aac15a892b324686a236d7c21014295c: function AS_Label_aac15a892b324686a236d7c21014295c(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_e5e08f19759a46dc9c902742c1d07924: function AS_Segment_e5e08f19759a46dc9c902742c1d07924(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedFromAccountRow = this.view.flxCheckingAccounts.segAccRecords.selectedRowItems[0];
        navToRespForm("frmTransferTo");
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_ae0e0b08f8d54894beb18bbc5c90e587: function AS_Segment_ae0e0b08f8d54894beb18bbc5c90e587(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedFromAccountRow1 = this.view.compSegHdrWithImgText.segAccRecords.selectedRowItems[0];
        navToRespForm("frmCheckDepositEnterAmount");
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_jbd5ea44d90843fabf2b10467803eb74: function AS_Segment_jbd5ea44d90843fabf2b10467803eb74(eventobject, sectionNumber, rowNumber) {
        var self = this;
        gblSelectedFromAccountRow1 = this.view.flxSavingsAcounts.segAccRecords.selectedRowItems[0];
        var ntf = new kony.mvc.Navigation("frmCheckDepositEnterAmount");
        ntf.navigate();
    }
});