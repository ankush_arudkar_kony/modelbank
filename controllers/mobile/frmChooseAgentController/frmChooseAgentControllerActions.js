define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBack **/
    AS_Button_d31f114a762f4cd6af54a94822b192f3: function AS_Button_d31f114a762f4cd6af54a94822b192f3(eventobject) {
        var self = this;
        var prevFrm = kony.application.getPreviousForm().id;
        if (prevFrm == "frmMyAccounts2") {
            var ntf = new kony.mvc.Navigation("frmMyAccounts2");
            ntf.navigate({
                "selectedVersionNum": "9"
            });
        } else {
            var ntf = new kony.mvc.Navigation("frmMyAccounts");
            ntf.navigate({
                "selectedVersionNum": "1"
            });
        }
    },
    /** onClick defined for flxBack **/
    AS_FlexContainer_f011c709e617469c97ca5bf91705b02e: function AS_FlexContainer_f011c709e617469c97ca5bf91705b02e(eventobject) {
        var self = this;
    },
    /** onClick defined for btnGetStarted **/
    AS_Button_b71c119c77834e9580feb35fe640f8dc: function AS_Button_b71c119c77834e9580feb35fe640f8dc(eventobject) {
        var self = this;
        this.btnGetStartNxtFrm();
    },
    /** onClick defined for btnChoose **/
    AS_Button_aec71ab6d0ad494f8a6897aac8d853dc: function AS_Button_aec71ab6d0ad494f8a6897aac8d853dc(eventobject) {
        var self = this;
        tmpFlxClose = 1;
        this.view.flxGrey.isVisible = false;
    },
    /** preShow defined for frmChooseAgent **/
    AS_Form_h5c4a4a580844bb3afc685da4fc40842: function AS_Form_h5c4a4a580844bb3afc685da4fc40842(eventobject) {
        var self = this;
        return self.frmPreShow.call(this);
    },
    /** postShow defined for frmChooseAgent **/
    AS_Form_d8f912337c6743e0a287984878900207: function AS_Form_d8f912337c6743e0a287984878900207(eventobject) {
        var self = this;
        //this.view.flxGrey.isVisible = true;
    }
});