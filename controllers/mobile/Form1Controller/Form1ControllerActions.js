define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for Button0f615e3b2baca49 **/
    AS_Button_a45a5d791ea44361b188d7a9fc08a8dd: function AS_Button_a45a5d791ea44361b188d7a9fc08a8dd(eventobject) {
        var self = this;
        this.view.donutchart.createChart([{
            "colorCode": "#e02020",
            "label": "Data1",
            "value": "21"
        }, {
            "colorCode": "#f7b500",
            "label": "Data2",
            "value": "21"
        }, {
            "colorCode": "#6dd400",
            "label": "Data3",
            "value": "15"
        }, {
            "colorCode": "#000000",
            "label": "Data4",
            "value": "25"
        }, {
            "colorCode": "#ffffff",
            "label": "Data5",
            "value": "25"
        }])
    }
});