define({ 

 //Type your controller code here 
onClickKey:function(selectedValue){
  var tempVal = null;
  tempVal = this.view.txtAmount.text;
  if(selectedValue == -1){
    if(tempVal != "" && tempVal != null && tempVal != undefined){
      if(tempVal == "$"){
        this.view.txtAmount.text = "";
        this.view.bnContinue.skin = "sknBtnDisabled";
        this.view.bnContinue.focusSkin = "sknBtnDisabled";
        return;
      }
        tempVal = tempVal.substring(0,tempVal.length-1);
        this.view.txtAmount.text = tempVal.replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return;
  }
  else if(selectedValue != -1){
    if(tempVal != "" && tempVal != null && tempVal != undefined){
       this.view.txtAmount.text = (tempVal+selectedValue).replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    else {
      this.view.txtAmount.text = "$"+selectedValue;
    }
    this.view.bnContinue.skin = "sknBtnBlueN";
    this.view.bnContinue.focusSkin = "sknBtnFocus";
  }
},
  onClickContinue:function(){
    if(this.view.bnContinue.skin == "sknBtnBlueN"){
      gblEnteredTransferAmount = this.view.txtAmount.text;
      navToRespForm("frmVerifyTransferDetails");
    }
  },
  preShowFun:function(){
    this.view.txtAmount.text = "";
    this.view.bnContinue.skin="sknBtnDisabled";
    this.view.bnContinue.focusSkin = "sknBtnDisabled";
//     this.view.lblSelectedFromAccount.text = gblSelectedFromAccountRow.lblAccountName;
//     this.view.lblAvailableBalanceFrom.text = gblSelectedFromAccountRow.lblBalance;
//     this.view.lblSelectedToAccount.text = gblSelectedToAccountRow.lblAccountName;
//     this.view.lblAvailableBalanceTo.text =gblSelectedToAccountRow.lblBalance;
  }
 });