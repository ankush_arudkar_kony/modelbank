define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for lbl1 **/
    AS_Label_b194076a19694b4a81a689473d3fb881: function AS_Label_b194076a19694b4a81a689473d3fb881(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 1);
    },
    /** onTouchStart defined for lbl2 **/
    AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c: function AS_Label_efa2a6d95cb94eefb5dad3f314c3f34c(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 2);
    },
    /** onTouchStart defined for lbl3 **/
    AS_Label_cd4bf4991d604d5c9235a4f2a0073419: function AS_Label_cd4bf4991d604d5c9235a4f2a0073419(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 3);
    },
    /** onTouchStart defined for lbl4 **/
    AS_Label_a8313943b7994b55bb104e23d0bea023: function AS_Label_a8313943b7994b55bb104e23d0bea023(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 4);
    },
    /** onTouchStart defined for lbl5 **/
    AS_Label_c1f47c8e66d242fc893d55c068c44a3e: function AS_Label_c1f47c8e66d242fc893d55c068c44a3e(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 5);
    },
    /** onTouchStart defined for lbl6 **/
    AS_Label_e7abd80904f64838a05de2d67f2692e8: function AS_Label_e7abd80904f64838a05de2d67f2692e8(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 6);
    },
    /** onTouchStart defined for lbl7 **/
    AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b: function AS_Label_ea6cf6b4d8ee408a968b26acf9a13e4b(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 7);
    },
    /** onTouchStart defined for lbl8 **/
    AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6: function AS_Label_h8d30ef6f3d84bd7a7699a11b75d0bc6(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 8);
    },
    /** onTouchStart defined for lbl9 **/
    AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29: function AS_Label_ca0ce042aac64c96a7c5e2af30aa1a29(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 9);
    },
    /** onTouchStart defined for lbl0 **/
    AS_Label_ef5e880ef423498293b0652c5cb97192: function AS_Label_ef5e880ef423498293b0652c5cb97192(eventobject, x, y) {
        var self = this;
        return self.onClickKey.call(this, 0);
    },
    /** onClick defined for flxBack **/
    AS_FlexContainer_h93e632e57334359a02f3b5d45772317: function AS_FlexContainer_h93e632e57334359a02f3b5d45772317(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferTo");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_a917ad95cfe44947a8790746fc71b6de: function AS_FlexContainer_a917ad95cfe44947a8790746fc71b6de(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onClick defined for flxAmount **/
    AS_FlexContainer_dbfa671ae9844accb8b5f5ab9b53c344: function AS_FlexContainer_dbfa671ae9844accb8b5f5ab9b53c344(eventobject) {
        var self = this;
        ///
    },
    /** onClick defined for bnContinue **/
    AS_Button_b9c4c65d4b044824b35a898ba05af385: function AS_Button_b9c4c65d4b044824b35a898ba05af385(eventobject) {
        var self = this;
        return self.onClickContinue.call(this);
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_b71a4c89e4564d968eb1ec1c684b82b3: function AS_FlexContainer_b71a4c89e4564d968eb1ec1c684b82b3(eventobject) {
        var self = this;
        return self.onClickKey.call(this, -1);
    },
    /** preShow defined for frmTransferAmount **/
    AS_Form_f80190b30a424e28aa3cb4adf3add0cc: function AS_Form_f80190b30a424e28aa3cb4adf3add0cc(eventobject) {
        var self = this;
        return self.preShowFun.call(this);
    }
});