define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Label0dd464373550946 **/
    AS_Label_aac15a892b324686a236d7c21014295c: function AS_Label_aac15a892b324686a236d7c21014295c(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onTouchEnd defined for CopyImage0c7d42c5de9504c **/
    AS_Image_ic37779fba0b40c2a3f0daaece6ddfd9: function AS_Image_ic37779fba0b40c2a3f0daaece6ddfd9(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onTouchEnd defined for imgBack **/
    AS_Image_he776d3827264b45b603ab9d0438c618: function AS_Image_he776d3827264b45b603ab9d0438c618(eventobject, x, y) {
        var self = this;
        return self.BackNavigation.call(this);
    },
    /** onClick defined for FlexGroup0d14a00d53c364f **/
    AS_FlexContainer_e3746eec21724540b52bcd43c9eaeccf: function AS_FlexContainer_e3746eec21724540b52bcd43c9eaeccf(eventobject) {
        var self = this;
        return self.BackNavigation.call(this);
    },
    /** onClick defined for flxAmount **/
    AS_FlexContainer_hd33cc0bb0274e6983364a1cc6c71890: function AS_FlexContainer_hd33cc0bb0274e6983364a1cc6c71890(eventobject) {
        var self = this;
        return self.show_foodData.call(this);
    },
    /** preShow defined for frmPFM **/
    AS_Form_gbe35e2bd762414caccef49871abb8c7: function AS_Form_gbe35e2bd762414caccef49871abb8c7(eventobject) {
        var self = this;
        return self.frm_preshow.call(this);
    }
});