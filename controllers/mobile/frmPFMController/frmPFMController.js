define({ 
frm_preshow:function()
  {
    this.view.lblPersonalChecking.text="Personal Finance";
    this.view.donutchart.setVisibility(true);
    this.view.flxCatogories.setVisibility(true);
    this.view.donutFood.setVisibility(false);
    this.view.flxFoodandDining.setVisibility(false);
    this.view.lblTotalSpending.text="$1,829.79";
    this.view.lblSpendingText.text="Total Spending in all categories";
    
  },
  
  show_foodData:function()
  {  this.view.lblPersonalChecking.text="Food & Dining";
     this.view.donutchart.setVisibility(false);
    this.view.flxCatogories.setVisibility(false);
    this.view.donutFood.setVisibility(true);
    this.view.flxFoodandDining.setVisibility(true);
    this.view.lblTotalSpending.text="$762.12";
    this.view.lblSpendingText.text="Total Spending in food & Dining";
  },
  BackNavigation:function()
  {
    if((this.view.donutFood.isVisible == true)&&(this.view.flxFoodandDining.isVisible == true))
      {
        this.frm_preshow();
      }
    else{
      var ntf=new kony.mvc.Navigation("frmDashboardAggregated");
      ntf.navigate();
    }
  }
 //Type your controller code here 

 });