define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for yimgBacknew **/
    AS_Image_g4f49d62e5384d15b6e6840dd058f0af: function AS_Image_g4f49d62e5384d15b6e6840dd058f0af(eventobject, x, y) {
        var self = this;
        navToRespForm("frmDashboardAggregated");
    },
    /** onScrolling defined for flxContent **/
    AS_FlexScrollContainer_f15b02b2235442398caa3bb8ab56f859: function AS_FlexScrollContainer_f15b02b2235442398caa3bb8ab56f859(eventobject) {
        var self = this;
        return self.onScrolling.call(this);
    },
    /** preShow defined for frmAccountDetails **/
    AS_Form_a3ece9b58dc848b688af710cfb6e868e: function AS_Form_a3ece9b58dc848b688af710cfb6e868e(eventobject) {
        var self = this;
        this.view.flxBtns.top = "0dp";
        this.view.flxWelcome.top = "50dp";
        this.view.flxPendingTransactions.top = "1240dp";
        this.view.flxPostedTransactions.top = "1240dp";
        this.view.flxAccount.top = "1240dp";
        this.centrXOfflxWelcomenew = this.view.flxWelcomenew.centerX;
        this.view.lblPersonalCheckingnew.text = Account.lblAccountName;
        self.preShowFun.call(this);
    },
    /** postShow defined for frmAccountDetails **/
    AS_Form_e81921d0671040bd888eecc0d2f4e0d0: function AS_Form_e81921d0671040bd888eecc0d2f4e0d0(eventobject) {
        var self = this;
        return self.AnimationFun.call(this);
    }
});