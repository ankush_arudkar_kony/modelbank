define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmInterstitial **/
    AS_Form_gb170da185254d56ba4a98b5a0bd09dd: function AS_Form_gb170da185254d56ba4a98b5a0bd09dd(eventobject) {
        var self = this;
        this.view.Image0ff39e13fece840.height = "0dp";
        this.view.Image0ff39e13fece840.width = "0dp";
    },
    /** postShow defined for frmInterstitial **/
    AS_Form_ee6caa78a0bf455ebc9d2be814bc3f5e: function AS_Form_ee6caa78a0bf455ebc9d2be814bc3f5e(eventobject) {
        var self = this;

        function SCALE_ACTION____dcc76bf1bc4d49458cff2b7217bf64b8_Callback() {}
        function SCALE_ACTION____cce31a847de3492bad1aed598ebd1447_Callback() {
            var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
            ntf.navigate();
            self.view.Image0ff39e13fece840.animate(
            kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "width": "0px",
                    "height": "0px"
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {
                "animationEnd": SCALE_ACTION____dcc76bf1bc4d49458cff2b7217bf64b8_Callback
            });
        }
        self.view.Image0ff39e13fece840.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "width": "85%",
                "height": "25%"
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 1.25
        }, {
            "animationEnd": SCALE_ACTION____cce31a847de3492bad1aed598ebd1447_Callback
        });
    }
});