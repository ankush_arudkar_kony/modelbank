define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_bde89e0b3a934b079a7f2e7add86e8d1: function AS_FlexContainer_bde89e0b3a934b079a7f2e7add86e8d1(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferAmount");
        ntf.navigate();
    },
    /** onClick defined for flxCancel **/
    AS_FlexContainer_f412185b66574e6daf25d8317894b17c: function AS_FlexContainer_f412185b66574e6daf25d8317894b17c(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboardAggregated");
        ntf.navigate();
    },
    /** onClick defined for btnAutoCapture **/
    AS_Button_gb770fc34c1c414390aced3e5be0e084: function AS_Button_gb770fc34c1c414390aced3e5be0e084(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmNAOVerify");
        ntf.navigate();
    },
    /** preShow defined for frmApplyNewAccount **/
    AS_Form_dbaa66fe483746f685aca2e4bea2970a: function AS_Form_dbaa66fe483746f685aca2e4bea2970a(eventobject) {
        var self = this;
        return self.preshow.call(this);
    }
});