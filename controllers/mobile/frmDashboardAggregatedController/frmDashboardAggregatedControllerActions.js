define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for imgFilter **/
    AS_Image_c7d2c6b132a844ed818508646177a73c: function AS_Image_c7d2c6b132a844ed818508646177a73c(eventobject, x, y) {
        var self = this;
        navToRespForm("frmLogin");
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_i56f7d264e1e46c08792d563b0d65350: function AS_Segment_i56f7d264e1e46c08792d563b0d65350(eventobject, sectionNumber, rowNumber) {
        var self = this;
        Account = this.view.compSegHdrWithoutImgText.segAccRecords.selectedRowItems[0];
        navToRespForm("frmAccountDetails");
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_ba10c8a686f84e22a66cbc00e5765fb5: function AS_Segment_ba10c8a686f84e22a66cbc00e5765fb5(eventobject, sectionNumber, rowNumber) {
        var self = this;
        Account = this.view.compSegHdrWithImgText.segAccRecords.selectedRowItems[0];
        navToRespForm("frmAccountDetails");
    },
    /** onClick defined for flxTransferFunds **/
    AS_FlexContainer_g0dac3250aac4f4b83053239c82841f5: function AS_FlexContainer_g0dac3250aac4f4b83053239c82841f5(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** onClick defined for flxDepositCheck **/
    AS_FlexContainer_fffba09644924215bb1249fe73edff62: function AS_FlexContainer_fffba09644924215bb1249fe73edff62(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCheckDepositFrom");
        ntf.navigate();
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_aabbdb42f9524c2e8efb4adffb2981e1: function AS_Segment_aabbdb42f9524c2e8efb4adffb2981e1(eventobject, sectionNumber, rowNumber) {
        var self = this;
        Account = this.view.compSegHdrWithImgText1.segAccRecords.selectedRowItems[0];
        navToRespForm("frmAccountDetails");
    },
    /** onRowClick defined for segAccRecords **/
    AS_Segment_fa6c63eed15b4f34900644f0b2860c3b: function AS_Segment_fa6c63eed15b4f34900644f0b2860c3b(eventobject, sectionNumber, rowNumber) {
        var self = this;
        Account = this.view.compSegHdrWithImgText2.segAccRecords.selectedRowItems[0];
        navToRespForm("frmAccountDetails");
    },
    /** onScrollStart defined for flxContent **/
    AS_FlexScrollContainer_a14ca97a1a8c4f88971a8b24fe38f085: function AS_FlexScrollContainer_a14ca97a1a8c4f88971a8b24fe38f085(eventobject) {
        var self = this;
        this.view.donutchart.setVisibility(true);
        this.view.donutchartCopy.setVisibility(true);
        this.view.donutchart3.setVisibility(true);
        this.view.donutchart4.setVisibility(true);
        this.view.donutchart5.setVisibility(true);
    },
    /** onScrollEnd defined for flxContent **/
    AS_FlexScrollContainer_e249c48cb7a84dd48812ad58ae9386cd: function AS_FlexScrollContainer_e249c48cb7a84dd48812ad58ae9386cd(eventobject) {
        var self = this;
        self.chartsAnim.call(this);
        self.chartsAnim1.call(this);
    },
    /** onClick defined for flxTransfer **/
    AS_FlexContainer_b0c87070a89245b28db29d679ab5d35f: function AS_FlexContainer_b0c87070a89245b28db29d679ab5d35f(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmTransferFrom");
        ntf.navigate();
    },
    /** onClick defined for flxMore **/
    AS_FlexContainer_e10828c230cd4e0f98901d966cf2b52d: function AS_FlexContainer_e10828c230cd4e0f98901d966cf2b52d(eventobject) {
        var self = this;
        return self.animationFun.call(this);
    },
    /** onTouchEnd defined for imgClose **/
    AS_Image_b208e4fcce68443bae917d4797623900: function AS_Image_b208e4fcce68443bae917d4797623900(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____c95a07afe27c46028d521044af7e6493_Callback() {}
        self.view.flxHamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {
            "animationEnd": MOVE_ACTION____c95a07afe27c46028d521044af7e6493_Callback
        });
    },
    /** onClick defined for flxEngage **/
    AS_FlexContainer_e1f6f48cb18f4e4199137ff469af20ce: function AS_FlexContainer_e1f6f48cb18f4e4199137ff469af20ce(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmGetstarted");
        ntf.navigate();
    },
    /** onClick defined for flxCardMAnagement **/
    AS_FlexContainer_d44f7efde5ce4e53a1898b6883157157: function AS_FlexContainer_d44f7efde5ce4e53a1898b6883157157(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCardManagement");
        ntf.navigate();
    },
    /** onClick defined for flxOpenNewAccount **/
    AS_FlexContainer_hf863925236149158baffc1c70f73afa: function AS_FlexContainer_hf863925236149158baffc1c70f73afa(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("NewAccount/frmApplyNewAccount");
        ntf.navigate();
    },
    /** onClick defined for flxPersonalFinance **/
    AS_FlexContainer_df5d5e249edc480fa2af535b5a284f3b: function AS_FlexContainer_df5d5e249edc480fa2af535b5a284f3b(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPFM");
        ntf.navigate();
    },
    /** onTouchEnd defined for lblSignout **/
    AS_Label_b5943620bd4c4e9ba45fa61c9a2561d6: function AS_Label_b5943620bd4c4e9ba45fa61c9a2561d6(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmLogin");
        ntf.navigate();
    },
    /** preShow defined for frmDashboardAggregated **/
    AS_Form_h50aebee09fc41c5a01a08eca3c6e09d: function AS_Form_h50aebee09fc41c5a01a08eca3c6e09d(eventobject) {
        var self = this;
        this.view.imgAccount.src = "icon_01.png";
        this.frm_preshow();
        this.view.flxHamburger.left = "-100%";
        this.view.lblAccount.skin = "CopydefLabel0f19088f695b640";
    },
    /** postShow defined for frmDashboardAggregated **/
    AS_Form_abc440d05fca42f6af23509995673adb: function AS_Form_abc440d05fca42f6af23509995673adb(eventobject) {
        var self = this;
        self.reFreshWithRowAnim.call(this);
        self.frm_Postshow.call(this);
    }
});