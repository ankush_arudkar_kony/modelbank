define({ 
  
  chartsAnim:function(){
    var k= this.view.flxContent.contentOffsetMeasured.y;
   //alert(k);
    if(k>420 || k<0){
           // this.view.donutchart.setVisibility(true);

      this.view.donutchart.createChart([
                {
                    "colorCode": "#e02020",
                    "label": "Data1",
                    "value": "21"
                },
                {
                    "colorCode": "#f7b500",
                    "label": "Data2",
                    "value": "21"
                },
                {
                    "colorCode": "#6dd400",
                    "label": "Data3",
                    "value": "15"
                },
                {
                    "colorCode": "#e7e7e7",
                    "label": "Data4",
                    "value": "25"
                },
                {
                    "colorCode": "#ffffff",
                    "label": "Data5",
                    "value": "25"
                }
            ])
    }
  },
  chartsAnim1:function(){
    var k= this.view.flxContent.contentOffsetMeasured.y;
    //alert(k);
    if(k>640 || k<0){
     //this.view.donutchartCopy.setVisibility(true);
       this.view.donutchartCopy.createChart([
                 {
                                "colorCode": "#f7b500",
                                "label": "Data1",
                                "value": "45"
                            },
    
     
                            {
                                "colorCode":"#e7e7e7",
                                "label": "data2",
                                "value": "55"
                            }
      
            ])
     // this.view.donutchart3.setVisibility(true);
       this.view.donutchart3.createChart([
                 {
                                "colorCode": "#fa6400",
                                "label": "Data1",
                                "value": "65"
                            },
     
                            {
                                "colorCode":"#e7e7e7",
                                "label": "data2",
                                "value": "35"
                            }
            ])
      // this.view.donutchart4.setVisibility(true);
       this.view.donutchart4.createChart([
                   {
                                "colorCode": "#f7b500",
                                "label": "Data1",
                                "value": "53"
                            },
     
                            {
                                "colorCode":"#e7e7e7",
                                "label": "data2",
                                "value": "47"
                            }
            ])
      // this.view.donutchart5.setVisibility(true);
       this.view.donutchart5.createChart([
                  {
                                "colorCode": "#6dd400",
                                "label": "Data1",
                                "value": "25"
                            },
     
                            {
                                "colorCode":"#e7e7e7",
                                "label": "data2",
                                "value": "75"
                            }
            ])
    }
  },

  data_CheckingAccounts : [
        [
            {
                "imgLogo": {
                    "@class": "com.viz.ide.model.segment.ImageSegmentData",
                    "src": "money_icon.png",
                    // "isVisible":"false"
                },
                "lblAccount": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": "Checking Accounts"
                },
                "lbllIne": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": ""
                }
            },
            [
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "dbx.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "John's Checking"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$10,456.32"//"$23,456.77"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Acct #5678" //"Bank of America #5678"
                    },
                    "lbllIne": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "-" //"Bank of America #5678"
                    }
                },
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "boa.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Personal Checking"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$23,456.77"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Bank of America #5678"
                    },
                    "lbllIne": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "-" //"Bank of America #5678"
                    }
                },
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "citi.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Sam’s Checking"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$12,345.45"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Citi Bank #7765"
                    },
//                     "lbllIne": {
//                         "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
//                         "text": "-" //"Bank of America #5678"
//                     }
                }
            ]
        ]
    ],
  data_SavingsAccounts : [
        [
            {
                "imgLogo": {
                    "@class": "com.viz.ide.model.segment.ImageSegmentData",
                    "src": "savings.png"
                },
                "lblAccount": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": "Savings Accounts"
                },
                "lbllIne": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": ""
                }
            },
            [
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "dbx.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Personal Savings"//"Linda's Savings"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$7,867.55"//"$2,346.67"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "DBX Bank #1234"//"Chase Bank #7878"
                    },
                    "lbllIne": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "-" //"Bank of America #5678"
                    }
                },
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "chase.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Linda's Savings"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$2,346.67"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Chase Bank #7878"
                    },
                    "lbllIne": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "-" //"Bank of America #5678"
                    }
                },
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "chase.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "John's Checking"
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$8,956.44"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Chase Bank #4567"
                    },
//                     "lbllIne": {
//                         "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
//                         "text": "-" //"Bank of America #5678"
//                     }
                }
            ]
        ]
    ],
  data_CreditCards: [
        [
            {
                "imgLogo": {
                    "@class": "com.viz.ide.model.segment.ImageSegmentData",
                    "src": "credit_card.png"
                },
                "lblAccount": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": "Credit Cards"
                },
                "lbllIne": {
                    "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                    "text": ""
                }
            },
            [
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "chase.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Chase Airline Rewards..."
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$5,025.54"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Chase Bank #7788"
                    },
                    "lbllIne": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "-" //"Bank of America #5678"
                    }
                },
                {
                    "imgBankLogo": {
                        "@class": "com.viz.ide.model.segment.ImageSegmentData",
                        "src": "boa.png",
                      "isVisible":false
                    },
                    "lblAccountName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Bank of America Credit..."
                    },
                    "lblAvailableBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Available Balance"
                    },
                    "lblBalance": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "$4,543.00"
                    },
                    "lblBankName": {
                        "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
                        "text": "Bank of America #3344"
                    },
//                     "lbllIne": {
//                         "@class": "com.viz.ide.model.segment.SimpleTextSegmentData",
//                         "text": "-" //"Bank of America #5678"
//                     }
                }
            ]
        ]
    ],
  
 //Type your controller code here 
  OnSegAccountsRowClick:function(selIndex){
   //alert(JSON.stringify(this.view.segAccounts.selectedRowIndex[1]));
//     if(selIndex=="0" || selIndex==0)
//     {
//        navToRespForm("frmAccountDetails");
//     }
    
    
    
  },
  
  onNavigate:function()
  {
    //this.view.segCheckingAccounts.removeAll();
    this.view.segSavingsAccountComp.removeAll();
    this.view.segCreditCardsComp.removeAll();
    
   
//     this.reFreshWithRowAnim();
  },
  
reFreshWithRowAnim:function(){
//           	var data = this.view.segAccounts.data;
  			var data = this.data_CheckingAccounts;
  			var data1 = this.data_SavingsAccounts;
  			var data2 = this.data_CreditCards;
			var addRowAnim=this.getAddRowAnim();
          	//this.view.segCheckingAccounts.addAll(data,addRowAnim);
this.view.segSavingsAccountComp.addAll(data1,addRowAnim);
this.view.segCreditCardsComp.addAll(data2,addRowAnim);
          	this.onRowDisplayFunction();
},

getAddRowAnim:function(){
              var transformProp1 = kony.ui.makeAffineTransform();
             transformProp1.scale(0.0,0.0); 
             var transformProp2 = kony.ui.makeAffineTransform();
             transformProp2.scale(0.5,0.5);
             var transformProp3 = kony.ui.makeAffineTransform();
             transformProp3.scale(1,1);
             var animDefinitionOne = {0  : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp1},
                                       // 50 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp2},
                         100 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp3}
                        } ;
              var animObj=kony.ui.createAnimation(animDefinitionOne);
              var animConf={delay:0,fillMode:kony.anim.FILL_MODE_FORWARDS,duration:0.7};
              var addRowAnimtion = { definition : animObj, config : animConf, callbacks : null };
             return addRowAnimtion;
            },
      		
onRowDisplayFunction: function(){
//                 var animConfig = {"duration":0.3,"iterationCount":1,"delay":0,"fillMode":kony.anim.FORWARDS	};
//                   //scale
//                   var transformProp1 = kony.ui.makeAffineTransform();
//                   transformProp1.scale(0.0,0.0); 
//                   var transformProp3 = kony.ui.makeAffineTransform();
//                   transformProp3.scale(1,1);
//                   var animDefinitionOne = {0  : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp1},
//                                           100 : {"anchorPoint":{"x":0.5,"y":0.5},"transform":transformProp3}
//                                           } ;
//                   var animDefinition = kony.ui.createAnimation(animDefinitionOne);
//                   var finalAnimation = {definition: animDefinition, config: animConfig};
              		
//                   this.view.segCheckingAccounts.setAnimations({visible: finalAnimation});
//   	this.view.segSavingsAccountComp.setAnimations({visible: finalAnimation});
//   this.view.segCreditCardsComp.setAnimations({visible: finalAnimation});
            },
//   preshow:function()
//   {
//       this.view.flxWelcome.animate(
//     kony.ui.createAnimation({
//         "100": {
//             "top": "0dp",
//             "stepConfig": {
//                 "timingFunction": kony.anim.EASE
//             }
//         }
//     }), {
//         "delay": 0,
//         "iterationCount": 1,
//         "fillMode": kony.anim.FILL_MODE_FORWARDS,
//         "duration": 0.5
//     }, {
//        // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
//     });
//    // this.view.donutchart.setVisibility(false);
//     //this.view.donutchart5.setVisibility(false);
//   //  var Content_width=this.view.flxContent.contentOffset;
//     //alert("response"+JSON.stringyfy(Content_width));
//   //  if(Content_width)
//   },
//   move_flxUser:function()
//   {
//     this.view.flxUser.animate(
//     kony.ui.createAnimation({
//         "100": {
//             "top": "50dp",
//             "stepConfig": {
//                 "timingFunction": kony.anim.EASE
//             }
//         }
//     }), {
//         "delay": 0.085,
//         "iterationCount": 1,
//         "fillMode": kony.anim.FILL_MODE_FORWARDS,
//         "duration": 0.5
//     }, {
//        // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
//     });
//   },
  move_CheckingAccounts:function()
  {
    this.view.flxCheckingAccounts.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "95dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.060,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
   move_flxQuickLinks:function()
  {
    this.view.flxQuickLinks.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "395dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.190,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
  frm_Postshow:function()
  {
    this.move_flxQuickLinks();
    this. move_CheckingAccounts();
   // this. move_flxUser();
   // this. preshow();
    this. move_SavingsAccounts();
    this. move_CreditCards();
    this. move_flxMonthlyBudget();
    this. move_Chart();
  },
    move_SavingsAccounts:function()
  {
    this.view.flxSavingsAccounts.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "505dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.260,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
   move_CreditCards:function()
  {
    this.view.flxCreditCards.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "785dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.330,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
   
  move_flxMonthlyBudget:function()
  {
    this.view.flxMonthlyBudget.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "1175dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.400,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
  move_Chart:function()
  {
    this.view.flxChart.animate(
    kony.ui.createAnimation({
        "100": {
            "top": "1000dp",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0.470,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
       // "animationEnd": MOVE_ACTION____i58709492486453fb00f424e123d20bf_Callback
    });
  },
frm_preshow:function()
  {
 this.view.imgMore.src="more_inactive.png";
 this.view.flxWelcome.top="0dp";
this.view.flxUser.top="40dp";
this.view.flxCheckingAccounts.top="1340dp";
this.view.flxQuickLinks.top="1340dp";
this.view.flxSavingsAccounts.top="1340dp";
this.view.flxCreditCards.top="1340dp";
this.view.flxChart.top="1340dp";
this.view.flxMonthlyBudget.top="1340dp";
  },
  animationFun:function()
  {
    var left=this.view.flxHamburger.left;
    if(left=="-100%")
      {
        // var self = this;
        this.view.lblAccount.skin="CopydefLabel0b667d2cb3a924b";
     this.view.imgMore.src="icon_11.png";
        this.view.imgAccount.src="accounts_inactive.png";
   // function MOVE_ACTION____f2e5d61e1a0b4469a6bcb558a9b600e6_Callback() {}
    this.view.flxHamburger.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
      //  "animationEnd": MOVE_ACTION____f2e5d61e1a0b4469a6bcb558a9b600e6_Callback
    });
      }
    else{
      {
        // var self = this;
         this.view.lblAccount.skin="CopydefLabel0f19088f695b640";
this.view.imgMore.src="more_inactive.png";
        this.view.imgAccount.src="icon_01.png";
   // function MOVE_ACTION____f2e5d61e1a0b4469a6bcb558a9b600e6_Callback() {}
    this.view.flxHamburger.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "-100%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    }, {
      //  "animationEnd": MOVE_ACTION____f2e5d61e1a0b4469a6bcb558a9b600e6_Callback
    });
      }
    }
  }


 });